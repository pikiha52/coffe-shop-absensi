-- Adminer 4.8.1 MySQL 5.5.5-10.4.20-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `absen`;
CREATE TABLE `absen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `check_in` datetime NOT NULL,
  `check_out` datetime DEFAULT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `absen` (`id`, `id_user`, `check_in`, `check_out`, `lat`, `lng`) VALUES
(1,	2,	'2021-07-09 01:43:09',	'2021-07-09 01:43:17',	-6.1747,	106.827),
(2,	3,	'2021-07-01 02:36:11',	'2021-07-01 02:36:16',	-7.090910999999999,	107.668887),
(3,	3,	'2021-07-02 02:36:11',	'2021-07-02 02:36:16',	-7.090910999999999,	107.668887),
(4,	3,	'2021-07-03 02:36:11',	'2021-06-03 02:36:16',	-7.090910999999999,	107.668887),
(5,	3,	'2021-06-01 02:36:11',	'2021-06-01 02:36:16',	-7.090910999999999,	107.668887),
(6,	3,	'2021-06-02 02:36:11',	'2021-06-02 02:36:16',	-7.090910999999999,	107.668887),
(7,	3,	'2021-06-03 02:36:11',	'2021-06-03 02:36:16',	-7.090910999999999,	107.668887),
(8,	3,	'2021-06-04 02:36:11',	'2021-06-04 02:36:16',	-7.090910999999999,	107.668887),
(9,	3,	'2021-06-05 02:36:11',	'2021-06-05 02:36:16',	-7.090910999999999,	107.668887),
(10,	3,	'2021-06-06 02:36:11',	'2021-06-06 02:36:16',	-7.090910999999999,	107.668887),
(11,	3,	'2021-06-07 02:36:11',	'2021-06-07 02:36:16',	-7.090910999999999,	107.668887),
(12,	3,	'2021-06-08 02:36:11',	'2021-06-08 02:36:16',	-7.090910999999999,	107.668887),
(13,	3,	'2021-06-09 02:36:11',	'2021-06-09 02:36:16',	-7.090910999999999,	107.668887),
(14,	3,	'2021-06-10 02:36:11',	'2021-06-10 02:36:16',	-7.090910999999999,	107.668887),
(15,	3,	'2021-06-11 02:36:11',	'2021-06-11 02:36:16',	-7.090910999999999,	107.668887),
(16,	3,	'2021-06-12 02:36:11',	'2021-06-12 02:36:16',	-7.090910999999999,	107.668887),
(17,	3,	'2021-06-13 02:36:11',	'2021-06-13 02:36:16',	-7.090910999999999,	107.668887),
(18,	3,	'2021-06-14 02:36:11',	'2021-06-14 02:36:16',	-7.090910999999999,	107.668887),
(19,	3,	'2021-06-15 02:36:11',	'2021-06-15 02:36:16',	-7.090910999999999,	107.668887),
(20,	3,	'2021-06-16 02:36:11',	'2021-06-16 02:36:16',	-7.090910999999999,	107.668887),
(21,	3,	'2021-06-17 02:36:11',	'2021-06-17 02:36:16',	-7.090910999999999,	107.668887),
(22,	3,	'2021-06-18 02:36:11',	'2021-06-18 02:36:16',	-7.090910999999999,	107.668887),
(23,	3,	'2021-06-19 02:36:11',	'2021-06-19 02:36:16',	-7.090910999999999,	107.668887),
(24,	3,	'2021-06-20 02:36:11',	'2021-06-20 02:36:16',	-7.090910999999999,	107.668887),
(25,	3,	'2021-06-21 02:36:11',	'2021-06-21 02:36:16',	-7.090910999999999,	107.668887),
(26,	3,	'2021-06-22 02:36:11',	'2021-06-22 02:36:16',	-7.090910999999999,	107.668887),
(27,	3,	'2021-06-23 02:36:11',	'2021-06-23 02:36:16',	-7.090910999999999,	107.668887),
(28,	3,	'2021-06-24 02:36:11',	'2021-06-24 02:36:16',	-7.090910999999999,	107.668887),
(29,	3,	'2021-06-25 02:36:11',	'2021-06-25 02:36:16',	-7.090910999999999,	107.668887),
(30,	3,	'2021-06-26 02:36:11',	'2021-06-26 02:36:16',	-7.090910999999999,	107.668887),
(31,	3,	'2021-07-04 02:36:11',	'2021-07-04 02:36:16',	-7.090910999999999,	107.668887),
(32,	3,	'2021-07-05 02:36:11',	'2021-07-05 02:36:16',	-7.090910999999999,	107.668887),
(33,	3,	'2021-07-06 02:36:11',	'2021-07-06 02:36:16',	-7.090910999999999,	107.668887),
(34,	3,	'2021-07-07 02:36:11',	'2021-07-07 02:36:16',	-7.090910999999999,	107.668887),
(35,	3,	'2021-06-08 02:36:11',	'2021-06-08 02:36:16',	-7.090910999999999,	107.668887),
(36,	3,	'2021-07-09 02:36:11',	'2021-07-09 02:36:16',	-7.090910999999999,	107.668887),
(37,	3,	'2021-07-10 02:31:42',	NULL,	-7.090910999999999,	107.668887),
(42,	3,	'2021-08-01 02:36:11',	'2021-08-01 02:36:16',	-7.090910999999999,	107.668887),
(43,	3,	'2021-08-02 02:36:11',	'2021-08-02 02:36:16',	-7.090910999999999,	107.668887),
(44,	3,	'2021-08-03 02:36:11',	'2021-08-03 02:36:16',	-7.090910999999999,	107.668887),
(45,	3,	'2021-08-04 02:36:11',	'2021-08-04 02:36:16',	-7.090910999999999,	107.668887),
(46,	3,	'2021-08-05 02:36:11',	'2021-08-05 02:36:16',	-7.090910999999999,	107.668887),
(47,	3,	'2021-08-06 02:36:11',	'2021-08-06 02:36:16',	-7.090910999999999,	107.668887),
(48,	3,	'2021-08-07 02:36:11',	'2021-08-07 02:36:16',	-7.090910999999999,	107.668887),
(49,	3,	'2021-08-08 02:36:11',	'2021-08-08 02:36:16',	-7.090910999999999,	107.668887),
(50,	3,	'2021-08-09 02:36:11',	'2021-08-09 02:36:16',	-7.090910999999999,	107.668887),
(51,	3,	'2021-08-10 02:36:11',	'2021-08-10 02:36:16',	-7.090910999999999,	107.668887),
(52,	3,	'2021-08-11 02:36:11',	'2021-08-11 02:36:16',	-7.090910999999999,	107.668887),
(53,	3,	'2021-08-12 02:36:11',	'2021-08-12 02:36:16',	-7.090910999999999,	107.668887),
(54,	3,	'2021-08-13 02:36:11',	'2021-08-13 02:36:16',	-7.090910999999999,	107.668887),
(55,	3,	'2021-08-14 02:36:11',	'2021-08-14 02:36:16',	-7.090910999999999,	107.668887),
(56,	3,	'2021-08-15 02:36:11',	'2021-08-15 02:36:16',	-7.090910999999999,	107.668887),
(57,	3,	'2021-08-16 02:36:11',	'2021-08-16 02:36:16',	-7.090910999999999,	107.668887),
(58,	3,	'2021-08-17 02:36:11',	'2021-08-17 02:36:16',	-7.090910999999999,	107.668887),
(59,	3,	'2021-08-18 02:36:11',	'2021-08-18 02:36:16',	-7.090910999999999,	107.668887),
(60,	3,	'2021-08-19 02:36:11',	'2021-08-19 02:36:16',	-7.090910999999999,	107.668887),
(61,	3,	'2021-08-20 02:36:11',	'2021-08-20 02:36:16',	-7.090910999999999,	107.668887),
(62,	3,	'2021-08-21 02:36:11',	'2021-08-21 02:36:16',	-7.090910999999999,	107.668887),
(63,	3,	'2021-08-22 02:36:11',	'2021-08-22 02:36:16',	-7.090910999999999,	107.668887),
(64,	3,	'2021-08-23 02:36:11',	'2021-08-23 02:36:16',	-7.090910999999999,	107.668887),
(81,	3,	'2021-09-08 16:00:38',	NULL,	-7.0909109,	107.668887);

DROP TABLE IF EXISTS `access_menu`;
CREATE TABLE `access_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `access_menu` (`id`, `level`, `menu_id`) VALUES
(1,	1,	1),
(2,	1,	2),
(3,	2,	3),
(4,	2,	4),
(5,	3,	5);

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE `blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `image` varchar(100) NOT NULL,
  `isi` longtext NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `blogs` (`id`, `category_id`, `title`, `image`, `isi`, `date`) VALUES
(1,	1,	'Kenapa si kita harus ngopi?',	'kenapaharusngopi.jpg',	'<p>Dengan kandungan antioksidan yang tinggi dan zat-zat gizi lainnya, kopi sebenarnya adalah minuman yang sangat menyehatkan, lho. Bahkan, kopi memiliki khasiat yang luar biasa untuk otak, kulit dan tubuh kamu. Hal ini dibuktikan melalui penelitian yang menunjukkan bahwa peminum kopi memiliki risiko penyakit yang lebih rendah dibandingkan yang bukan peminum kopi. Setidaknya ada 5 alasan kamu harus minum kopi setiap hari. Apa saja itu? Yuk, simak uraian berikut.</p>\r\n\r\n<p><strong>Meningkatkan Kecerdasan dan Energi</strong></p>\r\n\r\n<p>Stimulan dalam kopi yang dikenal sebagai kafein diserap dalam aliran darah dan mengalir ke otak yang meningkatkan berbagai aspek fungsi otak, di antaranya memori, suasana hati, kewaspadaan, tingkat energi, reaksi dan fungsi kognitif secara keseluruhan.</p>\r\n\r\n<p><strong>Meningkatkan Performa Fisik Secara Drastis</strong></p>\r\n\r\n<p>Kafein menstimulasi sistem saraf untuk mengirimkan sinyal ke sel lemak untuk membakar lemak tubuh, dan melepaskannya ke aliran darah sebagai bahan bakar tubuh dan meningkatkan performa fisik kamu rata-rata sebesar 11-12%.</p>\r\n\r\n<p><strong>Mempercepat Pembakaran Lemak</strong></p>\r\n\r\n<p>Good news buat kamu yang lagi diet! Kafein adalah salah satu zat alami yang telah terbukti mempercepat pembakaran lemak. Beberapa penelitian menunjukkan bahwa kafein bisa meningkatkan kadar metabolik sekitar 3-11% dan mempercepat pembakaran lemak sekitar 10%.</p>\r\n\r\n<p><strong>Melawan Depresi dan Pemicu Rasa Bahagia</strong></p>\r\n\r\n<p>Kopi bisa menurunkan risiko depresi. Menurut penelitian yang dirilis oleh Harvard pada 2011, perempuan yang minum kopi sebanyak 4 atau lebih cangkir setiap hari memiliki kecenderungan risiko depresi yang lebih rendah dibandingkan mereka yang tidak minum kopi.</p>\r\n\r\n<p><strong>Panjang Umur</strong></p>\r\n\r\n<p>Peminum kopi memiliki risiko terkena penyakit yang lebih rendah dibandingkan mereka yang tidak minum kopi. Bahkan, beberapa penelitian membuktikan bahwa peminum kopi memiliki risiko kematian yang lebih rendah.&nbsp;</p>\r\n\r\n<p>Nah, tunggu apa lagi? Yuk, datang kecoffeshop kami.</p>\r\n',	'2021-07-09 02:07:52'),
(3,	1,	'Perbedaan Kopi Robusta dan Arabika?',	'Indische-Perbedaan-Arabika-dan-Robusta.png',	'<p>Dapat dikatakan, kopi Robusta dan kopi Arabika merupakan jenis kopi yang paling populer. Hampir sebagian besar&nbsp;<em>coffee shop&nbsp;</em>di seluruh dunia menyajikan kedua jenis biji kopi yang terkenal ini ke dalam menunya.</p>\r\n\r\n<p>Meski keduanya sama-sama nikmat, sebenarnya ada beberapa perbedaan mendasar antara kopi Robusta dan Arabika. Bukan pencinta kopi sejati namanya jika Anda belum mengetahui perbedaannya. Berikut adalah 7 perbedaan antara kopi Robusta dan Arabika yang perlu diperhatikan.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table cellpadding=\"30\" cellspacing=\"0\" style=\"width:945px\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"vertical-align:top\"><strong>Arabika</strong></td>\r\n			<td style=\"vertical-align:top\"><strong>Robusta</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\"><strong>1</strong></td>\r\n			<td style=\"vertical-align:top\"><strong>Biji Kopi</strong></td>\r\n			<td style=\"vertical-align:top\">Lebih besar, pipih,<br />\r\n			dan sedikit lonjong (oval).</td>\r\n			<td style=\"vertical-align:top\">Lebih kecil dan berbentuk bulat.</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\"><strong>2</strong></td>\r\n			<td style=\"vertical-align:top\"><strong>Kadar Kafein</strong></td>\r\n			<td style=\"vertical-align:top\">Lebih rendah.<br />\r\n			Antara 1,1%-1,5%.</td>\r\n			<td style=\"vertical-align:top\">Lebih tinggi.<br />\r\n			Antara 2,2% &ndash; 2,7%.</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\"><strong>3</strong></td>\r\n			<td style=\"vertical-align:top\"><strong>Aroma &amp; Rasa</strong></td>\r\n			<td style=\"vertical-align:top\">Biasanya memiliki aroma buah-buahan dengan sedikit rasa asam dan manis.</td>\r\n			<td style=\"vertical-align:top\">Biasanya memiliki aroma kacang yang dominan dengan rasa cenderung pahit.</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\"><strong>4</strong></td>\r\n			<td style=\"vertical-align:top\"><strong>Penyajian</strong></td>\r\n			<td style=\"vertical-align:top\">Lebih nikmat disajikan tanpa tambahan gula agar cita rasa asli dapat dinikmati secara otentik.</td>\r\n			<td style=\"vertical-align:top\">Lebih sering disajikan dengan tambahan elemen lain seperti&nbsp;<em>cream</em>,&nbsp;<em>foam</em>, atau susu.</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\"><strong>5</strong></td>\r\n			<td style=\"vertical-align:top\"><strong>Kadar Keasaman</strong></td>\r\n			<td style=\"vertical-align:top\">Lebih rendah, sekitar 5,5-8% sehingga lebih aman dikonsumsi untuk pemilik lambung sensitif.</td>\r\n			<td style=\"vertical-align:top\">Lebih tinggi, sekitar 7-10%.</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\"><strong>6</strong></td>\r\n			<td style=\"vertical-align:top\"><strong>Lokasi Tanam</strong></td>\r\n			<td style=\"vertical-align:top\">Baik ditanam di dataran tinggi, 700-1.700 m di atas permukaan laut.</td>\r\n			<td style=\"vertical-align:top\">Hanya tumbuh pada daerah berketinggian 400-700 m di atas permukaan laut.</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\"><strong>7</strong></td>\r\n			<td style=\"vertical-align:top\"><strong>Budidaya</strong></td>\r\n			<td style=\"vertical-align:top\">Lebih sulit ditanam karena hanya mampu tumbuh di lingkungan yang lembap. Ini yang menyebabkan harga kopi Arabika relatif lebih mahal dibandingkan kopi Robusta.</td>\r\n			<td style=\"vertical-align:top\">Lebih mudah ditanam karena dapat bertahan di cuaca ekstrim atau panas. Selain itu, kopi Robusta juga tahan terhadap serangan hama dan penyakit karena kadar keasaman klorogenik yang cukup tinggi.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n',	'2021-07-09 01:17:47'),
(4,	1,	'Kopi, Secangkir Minuman yang Nikmat',	'ini-manfaat-konsumsi-kopi-hitam-dan-efek-sampingnya-untuk-kesehatan.jpg',	'<p>Kopi merupakan salah satu minuman yang paling digemari banyak orang. Dari setiap tiga orang di dunia, salah satunya adalah peminum kopi. Kopi memang sungguh nikmat jika diminum baik pagi hari, atau saat malam hari ketika pekerjaan menumpuk. Kopi merupakan salah satu minuman yang paling dinikmati banyak orang, yang tidak sekadar diteguk saja, namun juga dinikmati. Bisnis kopi pun telah menjadi bisnis puluhan milyar dolar, yang hanya mampu disaingi oleh bisnis minyak bumi.</p>\r\n\r\n<p>Artikel ini berjudul &quot;Kopi, Secangkir Minuman yang Nikmat&quot; dan dapat dibaca selengkapnya dengan klik tautan berikut: https://kumpulan.info/sehat/kopi-minuman-nikmat</p>\r\n\r\n<p>Silahkan kunjungi situs web www.kumpulan.info untuk artikel menarik lainnya.</p>\r\n\r\n<h2><strong>Sejarah Penyebarang Kopi</strong></h2>\r\n\r\n<p><br />\r\nBiji tanaman kopi dipanggang lalu dihaluskan dan dihidangkan. Metode pemanggangan biji kopi sendiri belum diketahui kapan dimulainya. Namun tanaman kopi berasal dari dataran tinggi di Ethiopia, yang pada saat itu merupakan tanaman liar di Ethiopia. Lalu tanaman kopi dari sini dikembangkan di Semenanjung Arab sekitar abad ke-15, yang terkenal menjadi Kopi Arabika. Kopi Arabika saat ini menjadi jenis kopi yang paling banyak diproduksi di dunia yaitu mencapai lebih dari 60 persen produksi kopi dunia.</p>\r\n\r\n<p>Menurut legenda, kopi ditemukan oleh seorang pemuda Arab bernama Kaldi, seorang penggembala kambing. Ia selalu memperhatikan bahwa kambingnya selalu menunjukkan gejala gembira setelah menggigit biji dan daun suatu tanaman hijau. Karena penasaran, ia mencoba biji tanaman tersebut dan merasakan efek semangat serta gembira. Akhirnya penemuan ini menyebar dari mulut ke mulut, sejak itu lahirlah kopi menurut legenda di Arab.</p>\r\n\r\n<p>Pada tahun 1610, tanaman kopi pertama ditanam di daerah India. Bangsa Belanda mulai mempelajari pengembangbiakan kopi pada tahun 1614. Lalu pada tahun 1616, mereka berhasil memperoleh bibit dan tanaman kopi yang subur dan langsung mendirikan perkebunan kopi di Srilanka dan tanah Jawa (Indonesia) pada tahun 1699. Kemudian oleh bangsa Belanda, tanaman ini disebar ke koloni Belanda di Amerika Tengah seperti di Suriname dan Kepulauan Karibia. Kemudian bangsa Perancis juga tertarik dengan perdagangan kopi ini. Mereka membeli bibit kopi dari Belanda lalu dikembangkan di Pulau R&eacute;union sebelah timur Madagaskar. Namun mereka gagal mengembangkan kopi di sini. Lalu pada tahun 1723, bangsa Perancis mencoba mengembangkan tanaman kopi di daerah Pulau Martinik. Pada tahun 1800-an, tanaman kopi dikembangkan di Hawaii. Belakangan tanaman ini juga dikembangkan di Brasil dan daerah-daerah lainnya.</p>\r\n\r\n<p>Artikel ini berjudul &quot;Kopi, Secangkir Minuman yang Nikmat&quot; dan dapat dibaca selengkapnya dengan klik tautan berikut: https://kumpulan.info/sehat/kopi-minuman-nikmat</p>\r\n\r\n<p>Silahkan kunjungi situs web www.kumpulan.info untuk artikel menarik lainnya.</p>\r\n\r\n<p><strong>Asal Kata Kopi</strong><br />\r\nKata kopi atau dalam bahasa Inggris coffee berasal dari bahasa Arab qahwah, yang berarti kekuatan. Kemudian kata kopi yang kita kenal saat ini berasal dari bahasa Turki yaitu kahveh yang kemudian belakangan menjadi koffie dalam bahasa Belanda dan coffee dalam bahasa Inggris. Kata tersebut diserap ke dalam bahasa Indonesia menjadi kopi.</p>\r\n\r\n<p><strong>Kopi pada Zaman Dahulu hingga Sekarang</strong><br />\r\nAwalnya kopi digunakan sebagai produk makanan. Kemudian kopi digunakan sebagai pengganti minuman anggur. Belakangan kopi digunakan juga sebagai obat. Dan saat ini kopi terkenal sebagai minuman yang cukup digemari.</p>\r\n\r\n<p>Pada awalnya kopi digunakan sebagai makanan. Seluruh biji kopi dihancurkan, lalu ditambahkan minyak. Lalu adonan ini dibentuk berbentuk bundar dan menjadi makanan. Sampai saat ini, beberapa suku di Afrika masih memakan kopi dalam bentuk seperti itu.</p>\r\n\r\n<p>Belakangan, kopi digunakan sebagai pengganti minuman anggur. Biji kopi dibuat sebagai minuman yang mirip dengan anggur. Beberapa orang membuat minuman seperti ini dengan menuangkan air mendidih ke biji kopi yang sudah dikeringkan.</p>\r\n\r\n<p>Sebagai obat, kopi dapat bermanfaat untuk mengobati migrain, sakit kepala, gangguan jantung, asma kronis dan gangguan buang air. Meski demikian, untuk konsumsi kopi berlebih bisa berakibat buruk. Jika mengkonsumsi kopi secara belebih dapat meningkatkan asam lambung, menyebabkan ketegangan, dan mempercepat detak jantung. Selain itu, konsumsi kopi secara berlebih, sering dikaitkan dengan sakit maag.</p>\r\n\r\n<p>Belakangan, kopi digunakan sebagai minuman yang cukup nikmat. Biji kopi dikeringkan lalu dipanggang dan digiling dalam batok. Hasilnya kemudian bisa menjadi minuman kopi yang nikmat. Belakangan ditemukan mesin penggiling biji kopi yang memudahkan produksi kopi sebagai minuman.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Berbagai Macam Kegunaan Kopi</strong><br />\r\nBerbagai rasa kopi yang khas membuat sensasi menyenangkan di mulut. Misalnya es kopi atau iced coffee yang manis biasanya menyegarkan. Es krim rasa kopi pun juga menjadi favorit bagi banyak orang. Kopi juga menjadi salah satu bahan dasar beberapa jenis kue rasa kopi. Dan yang paling populer adalah kopi polos dan juga kopi susu.</p>\r\n\r\n<p>Namun para ilmuwan juga menyelediki manfaat lain dari kopi. Sisa bubuk dari kopi bermanfaat sebagai pupuk yang baik. Selain itu, beberapa produk disinfektan maupun isolasi untuk dinding, lantai dan atap juga dapat dibuat dari kopi. Gliserin yang merupakan produk sampingan dari sabun, dapat dibuat dari minyak kopi. Minyak kopi juga biasa digunakan sebagai bahan pembuat cat, sabun, maupun produk lainnya.</p>\r\n\r\n<p>Biji kopi dapat bermanfaat untuk berbagai produk dan kegunaan. Namun yang paling populer tentu saja sebagai minuman yang nikmat yang diminum banyak orang setiap harinya.</p>\r\n\r\n<p>Artikel ini berjudul &quot;Kopi, Secangkir Minuman yang Nikmat&quot; dan dapat dibaca selengkapnya dengan klik tautan berikut: https://kumpulan.info/sehat/kopi-minuman-nikmat</p>\r\n\r\n<p>Silahkan kunjungi situs web www.kumpulan.info untuk artikel menarik lainnya.</p>\r\n',	'2021-08-23 12:49:26');

DROP TABLE IF EXISTS `careers`;
CREATE TABLE `careers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `detail` longtext NOT NULL,
  `limit_aply` date NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `careers` (`id`, `name`, `detail`, `limit_aply`, `created_at`) VALUES
(1,	'Barista Girl Partime',	'<p>Syarat :</p>\r\n\r\n<p>&bull; Wanita</p>\r\n\r\n<p>&bull; Usia max 25 tahun</p>\r\n\r\n<p>&bull; Good atitude</p>\r\n\r\n<p>&bull; Good looking</p>\r\n\r\n<p>&bull; Domisili Depok dan sekitarnya</p>\r\n\r\n<p>Job desk :</p>\r\n\r\n<p>&bull; Bertanggung jawab dalam pembuatan kopi.</p>\r\n\r\n<p>&bull; Bertanggung jawab dalam kebersihan coffeshop.</p>\r\n',	'2021-08-31',	'2021-07-09'),
(2,	'Barista Full Time',	'<p>Syarat :</p>\r\n\r\n<p>&bull; Wanita &amp; Pria</p>\r\n\r\n<p>&bull; Usia max 25 tahun</p>\r\n\r\n<p>&bull; Good atitude</p>\r\n\r\n<p>&bull; Good looking</p>\r\n\r\n<p>&bull; Berpengalaman&nbsp;atau tidak berpengalaman (Berpengalaman lebih diutamakan)</p>\r\n\r\n<p>&bull; Domisili Depok dan sekitarnya</p>\r\n\r\n<p>Job desk :</p>\r\n\r\n<p>&bull; Bertanggung jawab dalam pembuatan kopi.</p>\r\n\r\n<p>&bull; Bertanggung jawab dalam kebersihan coffeshop.</p>\r\n\r\n<p>&bull; Bertanggung jawab dalam pekerjaan.</p>\r\n',	'2021-08-26',	'2021-08-19'),
(3,	'testhjhj',	'<p>hjh</p>\r\n',	'2021-08-25',	'2021-08-24');

DROP TABLE IF EXISTS `careers_apply`;
CREATE TABLE `careers_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `careers_id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `education` varchar(20) NOT NULL,
  `address` longtext NOT NULL,
  `findme` varchar(20) NOT NULL,
  `resume` varchar(30) NOT NULL,
  `additional` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `careers_apply` (`id`, `careers_id`, `firstname`, `lastname`, `phone`, `email`, `education`, `address`, `findme`, `resume`, `additional`) VALUES
(1,	2,	'Reza',	'Huda',	'089282929292',	'rezahu@gmail.com',	'SMK',	'Cimanggis Depok',	'Friend',	'KPU_TA.pdf',	''),
(2,	1,	'Sella',	'Ananda Ayu',	'081892920002',	'shellaananda@gmail.co.id',	'SMK',	'Cibubur, Jakarta Timur',	'Social Media',	'2131-6183-2-PB.pdf',	'');

DROP TABLE IF EXISTS `carts`;
CREATE TABLE `carts` (
  `id_carts` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL,
  PRIMARY KEY (`id_carts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `carts` (`id_carts`, `id_menu`) VALUES
(46,	3);

DROP TABLE IF EXISTS `category_blogs`;
CREATE TABLE `category_blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `category_blogs` (`id`, `name`, `date`) VALUES
(1,	'info',	'2021-08-19 19:00:52');

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `comment` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `comments` (`id`, `name`, `email`, `subject`, `comment`) VALUES
(1,	'Yuni',	'yuni@gmail.com',	'Kopinya enakkk',	'Kopi nya enak banget, tempatnya nyaman dan harga nya juga ramah dikantong, wajib banget buat dateng lagi kesini.'),
(2,	'Junior',	'juni@gmail.com',	'Tempatnya enak',	'tempatnya enak dan kopi nya enak, parkiran juga luas cocok dah buat acara reunian.'),
(3,	'Reni',	'renisa@gmail.com',	'Kopi',	'cappucino nya enak, beda sama coffeshop lain rasanya disini lebih kenaaa.'),
(4,	'Heni',	'heni@gmail.com',	'wifi',	'saran aja nih, wifinya coba dinaikin lagi mbps nya, soalnya lama bangettt hehe');

DROP TABLE IF EXISTS `detail_user`;
CREATE TABLE `detail_user` (
  `id_detail` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `gender` enum('Laki - laki','Perempuan') NOT NULL,
  `facebook` varchar(50) NOT NULL,
  `instagram` varchar(50) NOT NULL,
  `twitter` varchar(50) NOT NULL,
  PRIMARY KEY (`id_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `detail_user` (`id_detail`, `user_id`, `firstname`, `lastname`, `email`, `photo`, `phone`, `gender`, `facebook`, `instagram`, `twitter`) VALUES
(1,	1,	'Fikih',	'Alan',	'pikiha52@gmail.com',	'kih.jpg',	'089522616459',	'Laki - laki',	'https://web.facebook.com/piqih.piqih/',	'https://www.instagram.com/pikihaja/',	''),
(2,	2,	'Reni',	'Safitri',	'renisa@gmail.com',	'default.png',	'089522616459',	'Perempuan',	'',	'',	''),
(3,	3,	'Fahkri',	'Selo',	'fahri@gmail.com',	'default.png',	'08977789667',	'Laki - laki',	'fahkri',	'fahkri',	'fahkri'),
(4,	4,	'jono',	'jono',	'asa',	'asasa',	'asasa',	'Perempuan',	'',	'',	'');

DROP TABLE IF EXISTS `level`;
CREATE TABLE `level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `menu` (`id`, `menu`) VALUES
(1,	'Website Management'),
(2,	'User Management'),
(3,	'Management'),
(4,	'For You'),
(5,	'For You');

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `numberorder` varchar(20) NOT NULL,
  `id_menu` varchar(100) NOT NULL,
  `id_user` int(11) NOT NULL,
  `qty` varchar(100) NOT NULL,
  `total` varchar(100) NOT NULL,
  `grand_total` int(11) NOT NULL,
  `cash` int(11) NOT NULL,
  `change` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `orders` (`id_order`, `numberorder`, `id_menu`, `id_user`, `qty`, `total`, `grand_total`, `cash`, `change`, `created_at`) VALUES
(8,	'#ORD-0001',	'[\"1\",\"2\",\"3\"]',	3,	'[\"1\",\"1\",\"1\"]',	'[\"25000\",\"10000\",\"25000\"]',	60000,	100000,	'-40000',	'2021-07-31 02:47:39'),
(9,	'#ORD-0002',	'[\"1\",\"2\",\"3\"]',	1,	'[\"2\",\"1\",\"2\"]',	'[\"50000\",\"10000\",\"50000\"]',	110000,	150000,	'-40000',	'2021-07-31 02:56:53'),
(10,	'#ORD-0002',	'[\"1\",\"2\",\"3\"]',	1,	'[\"2\",\"1\",\"2\"]',	'[\"50000\",\"10000\",\"50000\"]',	110000,	150000,	'-40000',	'2021-08-05 02:56:53'),
(11,	'#ORD-0003',	'[\"1\",\"3\",\"5\",\"5\",\"7\"]',	3,	'[\"1\",\"1\",\"1\",\"1\",\"1\"]',	'[\"25000\",\"25000\",\"30000\",\"30000\",\"10000\"]',	120000,	150000,	'-30000',	'2021-08-13 07:21:17'),
(12,	'#ORD-0004',	'[\"3\",\"5\"]',	3,	'[\"1\",\"1\"]',	'[\"25000\",\"30000\"]',	55000,	60000,	'-5000',	'2021-08-13 07:24:25'),
(13,	'#ORD-0005',	'[\"2\",\"6\"]',	4,	'[\"1\",\"1\"]',	'[\"10000\",\"15000\"]',	25000,	50000,	'-25000',	'2021-08-14 05:17:52'),
(14,	'#ORD-0006',	'[\"5\",\"1\"]',	3,	'[\"1\",\"1\"]',	'[\"30000\",\"25000\"]',	55000,	60000,	'-5000',	'2021-08-19 12:25:07'),
(15,	'#ORD-0007',	'[\"2\",\"5\"]',	3,	'[\"1\",\"1\"]',	'[\"10000\",\"30000\"]',	40000,	50000,	'-10000',	'2021-08-19 12:27:18'),
(16,	'#ORD-0008',	'[\"3\",\"1\",\"5\",\"4\"]',	3,	'[\"1\",\"1\",\"1\",\"1\"]',	'[\"25000\",\"25000\",\"30000\",\"20000\"]',	100000,	100000,	'0',	'2021-08-19 12:27:41'),
(17,	'#ORD-0009',	'[\"5\",\"6\"]',	3,	'[\"1\",\"1\"]',	'[\"30000\",\"15000\"]',	45000,	50000,	'-5000',	'2021-08-22 02:51:28'),
(18,	'#ORD-0010',	'[\"7\",\"3\",\"5\"]',	3,	'[\"1\",\"1\",\"1\"]',	'[\"10000\",\"25000\",\"30000\"]',	65000,	100000,	'-35000',	'2021-08-27 12:11:24'),
(19,	'#ORD-0011',	'[\"3\"]',	3,	'[\"1\"]',	'[\"25000\"]',	25000,	25000,	'0',	'2021-08-27 12:13:25');

DROP TABLE IF EXISTS `our_menu`;
CREATE TABLE `our_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `title` varchar(50) NOT NULL,
  `price` varchar(20) NOT NULL,
  `porsi` int(10) NOT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `our_menu` (`id_menu`, `image`, `title`, `price`, `porsi`) VALUES
(1,	'capu2.jpeg',	'Cappucino',	'25000',	10),
(2,	'ess2.jpeg',	'Espresso',	'10000',	10),
(3,	'index2.jpeg',	'Moccacino',	'25000',	10),
(4,	'roti-bakar1.jpg',	'Roti Bakar',	'20000',	10),
(5,	'chicken-finger1.jpeg',	'Chicken Finger',	'30000',	10),
(6,	'omelette-mie-ayam-sosis1.jpg',	'Omelette Mie Ayam',	'15000',	10),
(7,	's2zD2hoe8pvHiitudC89LgHWSGZdwMra-31363033303231363236d41d8cd98f00b204e9800998ecf8427e_800x80011.jpg',	'Pisang Bakar',	'10000',	10);

DROP TABLE IF EXISTS `position`;
CREATE TABLE `position` (
  `id_position` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id_position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `position` (`id_position`, `name`) VALUES
(1,	'C.E.O'),
(2,	'Manager'),
(3,	'Barista'),
(4,	'Cashier');

DROP TABLE IF EXISTS `salarys`;
CREATE TABLE `salarys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `salary` int(11) NOT NULL,
  `transport` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `salarys` (`id`, `position_id`, `user_id`, `salary`, `transport`) VALUES
(1,	2,	2,	90000,	30000),
(2,	3,	3,	80000,	20000),
(3,	4,	4,	80000,	15000);

DROP TABLE IF EXISTS `salarys_total`;
CREATE TABLE `salarys_total` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `total_salary` varchar(50) NOT NULL,
  `status` enum('Belum','Sudah') NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `salarys_total` (`id`, `id_user`, `total_salary`, `status`, `date`) VALUES
(1,	3,	'800000',	'Sudah',	'2021-07-01'),
(2,	3,	'2700000',	'Sudah',	'2021-06-01'),
(3,	2,	'120000',	'Sudah',	'2021-07-09');

DROP TABLE IF EXISTS `slides`;
CREATE TABLE `slides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `image` varchar(50) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `slides` (`id`, `name`, `image`, `created_at`) VALUES
(1,	'Take a break and enjoy coffee for a while',	'caffee-221.jpeg',	'2021-07-09');

DROP TABLE IF EXISTS `sub_menu`;
CREATE TABLE `sub_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `title` varchar(20) NOT NULL,
  `url` varchar(50) NOT NULL,
  `icon` varchar(20) NOT NULL,
  `is_active` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1,	1,	'Dashboards',	'admin/welcome',	'home',	1),
(2,	1,	'Category Blogs',	'admin/categoryblogscontroller',	'archive',	1),
(3,	1,	'Blogs',	'admin/blogscontroller',	'book-open',	1),
(4,	1,	'Slide',	'admin/slidescontroller',	'image',	1),
(5,	1,	'Careers',	'admin/careersadmincontroller',	'thumbs-up',	1),
(6,	2,	'Reports Absent',	'admin/absensicontroller',	'monitor',	1),
(7,	2,	'Users',	'admin/userscontroller',	'users',	1),
(8,	2,	'Positions',	'admin/positionadmincontroller',	'award',	1),
(9,	2,	'Salarys',	'admin/salarycontroller',	'dollar-sign',	1),
(10,	1,	'Comments',	'admin/commentscontroller',	'message-circle',	1),
(11,	3,	'Dashboards',	'admin/welcome',	'home',	1),
(12,	3,	'Users',	'admin/userscontroller',	'users',	1),
(13,	4,	'Reports Absent',	'admin/absensicontroller',	'monitor',	1),
(14,	3,	'Ours Menu',	'admin/menucontroller',	'menu',	1),
(15,	1,	'Ours Menu',	'admin/menucontroller',	'menu',	1),
(16,	5,	'Dashboards',	'admin/welcome',	'home',	1),
(17,	5,	'Reports Absent',	'admin/absensicontroller',	'monitor',	1);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id_user` int(20) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) NOT NULL,
  `level` int(10) NOT NULL,
  `id_position` int(11) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `status_user` int(11) NOT NULL,
  `token` varchar(200) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `users` (`id_user`, `password`, `level`, `id_position`, `nip`, `status_user`, `token`) VALUES
(1,	'$2y$10$u715NRWorwRzYi3EGv.HEODLRnkvrC448X9yHu3rnzIbgYRR8F6Mi',	1,	1,	'202101',	1,	'ac53f0603a1e55a961496d79228468eeabfec276dc5d9fd6da9dc57bc021f16425b3b7a2734e5207'),
(2,	'$2y$10$u715NRWorwRzYi3EGv.HEODLRnkvrC448X9yHu3rnzIbgYRR8F6Mi',	2,	2,	'202102',	1,	''),
(3,	'$2y$10$u715NRWorwRzYi3EGv.HEODLRnkvrC448X9yHu3rnzIbgYRR8F6Mi',	3,	3,	'202103',	1,	'4ec5223c088bcb2f1b7c93a09fb8f6969c8a156d0561dce045167b0f8e8bb0ec129ac0c70b98cd31'),
(4,	'$2y$10$u715NRWorwRzYi3EGv.HEODLRnkvrC448X9yHu3rnzIbgYRR8F6Mi',	3,	4,	'202104',	1,	'');

-- 2021-09-09 06:59:34
