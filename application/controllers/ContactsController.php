<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactsController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('comment');
	}

	public function index()
	{
		$data = [
			'title' => 'Coffee &#8211; Contacts'
		];

		$this->load->view('templates/header', $data);
		$this->load->view('frontend/contacts/index');
		$this->load->view('templates/footer');
	}

	public function store()
	{
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$subject = $this->input->post('subject');
		$comment = $this->input->post('comment');

		$data = array(
			'name' => $name,
			'email' => $email,
			'subject' => $subject,
			'comment' => $comment,
		);


		$store = $this->comment->store('comments', $data);
		if ($store) {
			$this->session->set_flashdata('success', 'contact_success');
		}else{
			$this->session->set_flashdata('error', 'contact_failed');
		}
		redirect('Welcome');
	}
	
}
