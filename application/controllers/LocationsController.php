<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LocationsController extends CI_Controller {


	public function __construct()
	{
		parent::__construct();

		$this->load->model('blogs');
	}

	public function index()
	{
		$blogs = $this->blogs->selectAll();
		$data = [
			'title' => 'Coffee &#8211; Blogs',
			'blogs' => $blogs
		];

		$this->load->view('templates/header', $data);
		$this->load->view('frontend/locations/index');
		$this->load->view('templates/footer');
	}
}
