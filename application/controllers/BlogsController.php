<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BlogsController extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('CategoryBlogs');
		$this->load->model('blogs');
	}

	public function index()
	{	
		$category = $this->CategoryBlogs->selectAll();
		$count = $this->CategoryBlogs->count($category);
		$blogs = $this->blogs->selectAll();
		$data = [
			'title' => 'Coffee &#8211; Blogs',
			'blogs' => $blogs,
			'category' => $category,
			'count' => $count 
		];

		// var_dump($blogs);
		// die();

		$this->load->view('templates/header', $data);
		$this->load->view('frontend/blogs/index');
		$this->load->view('templates/footer');
	}

	public function byCategory($id)
	{	
		$category = $this->CategoryBlogs->selectbyCategoryId($id);
		$data = [
			'title' => 'Coffee &#8211; Blogs',
			'category' => $category
		];

		// var_dump($category);
		// die();

		$this->load->view('templates/header', $data);
		$this->load->view('frontend/blogs/bycategory');
		$this->load->view('templates/footer');
	}


	public function show($id)
	{	
		$category = $this->CategoryBlogs->selectAll();
		$blogs = $this->blogs->detail($id);
		$recentblogs = $this->blogs->recent($id)->result();
		$data = [
			'title' => 'Coffee &#8211; Blogs',
			'blogs' => $blogs,
			'category' => $category,
			'recentblogs' => $recentblogs
		];

		$this->load->view('templates/header', $data);
		$this->load->view('frontend/blogs/detail');
		$this->load->view('templates/footer');
	}
}
