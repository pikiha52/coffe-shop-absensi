<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('menu');
		$this->load->model('comment');
		$this->load->model('profiles');
		$this->load->model('slides');
		$this->load->model('blogs');
		$this->load->helper('rupiah_helper');
	}

	public function index()
	{	
		$profile = $this->profiles->showteam();
		$slide = $this->slides->selectAll();
		$year = date('Y');
		$allMenu = $this->menu->selectAll();
		$blogs = $this->blogs->homeBlogs();
		$comment = $this->comment->selectAll();
		$data = [
			'title' => 'Coffee &#8211; Welcome, Coffe with Love',
			'slide' => $slide,
			'profile' => $profile,
			'blogs'	=> $blogs,
			'allMenu' => $allMenu,
			'comment'	=> $comment
		];

		// var_dump($slide);
		// die();

		
		$this->load->view('templates/header', $data);
		$this->load->view('frontend/home/index');
		$this->load->view('templates/footer');
	}


	public function showMenu($id)
	{
		$recent = $this->menu->recentMenu($id);
		$menu = $this->menu->byId($id);
		$data = [
			'title' => 'Coffee &#8211; Welcome, Coffe with Love',
			'menu' 	=> $menu,
			'recent' => $recent
		];


		$this->load->view('templates/header', $data);
		$this->load->view('frontend/home/showMenu');
		$this->load->view('templates/footer');

	}


}
