<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CareersController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('careers');
	}

	public function index()
	{

		$careers = $this->careers->selectAll();
		$data = [
			'title' => 'Coffee &#8211; Join with Coffee',
			'careers' => $careers
		];

		$this->load->view('templates/header', $data);
		$this->load->view('frontend/careers/index');
		$this->load->view('templates/footer');	
	}

	public function show($id)
	{
		$today = date('Y-m-d');
		$careers = $this->careers->showPost($id);
	
		$data = [
			'title' => 'Coffee &#8211; Join with Coffee',
			'careers' => $careers,
			'today'	=> $today
		];

		$this->load->view('templates/header', $data);
		$this->load->view('frontend/careers/detailcareer');
		$this->load->view('templates/footer');			

	}

	public function apply($id)
	{	
		$careers = $this->careers->showPost($id);
		
		$data = [
			'title' => 'Coffee &#8211; Upload your CV',
			'careers' => $careers
		];

		$this->load->view('templates/header', $data);
		$this->load->view('frontend/careers/applycareer');
		$this->load->view('templates/footer');				
	}

	public function store()
	{
		$config['upload_path']          = './assets/resume/';
		$config['allowed_types']        = 'pdf|zip';
		$config['max_size']             = 0;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
		$this->load->library('upload', $config);
		$this->upload->do_upload('resume');
		{
			$_data = array('upload_data' => $this->upload->data());
			$data = array(
				'careers_id' => $this->input->post('careers_id'),
				'firstname'=> $this->input->post('firstname'),
				'lastname'=> $this->input->post('lastname'),
				'phone'=> $this->input->post('phone'),
				'email'=> $this->input->post('email'), 
				'education'=> $this->input->post('education'),
				'address'=> $this->input->post('address'),
				'findme'=> $this->input->post('findme'),
				'resume' => $_data['upload_data']['file_name'],
				'additional'=> $this->input->post('additional')
			);


			$insert = $this->careers->store('careers_apply',$data);
			if($insert){
				$this->session->set_flashdata('success', 'careers_success');
			}else{
				$this->session->set_flashdata('error', 'careers_failed');
			}
			redirect('CareersController');
		}
	}

}

?>
