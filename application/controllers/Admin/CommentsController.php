<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CommentsController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('profiles');
		$this->load->model('comment');
		$this->load->model('login');
		if($this->login->isNotLogin()) redirect(site_url('LoginController'));
	}


	public function index()
	{
		$level = $this->session->userdata('level');
		if ($level == "3") {
			$title['title'] = 'Not for you!';
			$this->load->view('templates/header-admin', $title);
			$this->load->view('404/index');
			$this->load->view('templates/footer-admin');
		} else {
		$id = $this->session->userdata('id_user');
		$level = $this->session->userdata('level');
		$profile = $this->profiles->selectById($id);
		$comments = $this->comment->selectAllforHome();
		$data = [
			'title' => 'Coffee &#8211; Comments',
			'profile' => $profile,
			'comments' => $comments
		];


		$this->load->view('templates/header-admin',$data);
		$this->load->view('templates/sidebar-admin');
		$this->load->view('Admin/comments/index');
		$this->load->view('templates/footer-admin');
	}
}


}
