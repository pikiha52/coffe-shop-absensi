<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MoneyInReport extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('profiles');
		$this->load->model('login');
		$this->load->model('reports');
		$this->load->model('menu');
		$this->load->helper('rupiah');
		$this->load->helper('tgl_indo');
		if($this->login->isNotLogin()) redirect(site_url('LoginController'));
	}


	public function index()
	{
		$level = $this->session->userdata('level');
		if ($level == "3") {
			$title['title'] = 'Not for you!';
			$this->load->view('templates/header-admin', $title);
			$this->load->view('404/index');
			$this->load->view('templates/footer-admin');
		} else {
		$id = $this->session->userdata('id_user');
		$profile = $this->profiles->selectById($id);
		$data = [
			'title' => 'Coffee &#8211; Report Money In',
			'profile' => $profile
		];

		$this->load->view('templates/header-admin', $data);
		$this->load->view('templates/sidebar-admin');
		$this->load->view('Admin/reportmoneyin/index');
		$this->load->view('templates/footer-admin');

	}
}

	public function lihat()
	{
		$today = date('Y/m/d');
		$id = $this->session->userdata('id_user');
		$profile = $this->profiles->selectById($id);
		$fromdate = $this->input->post('fromdate');
		$todate = $this->input->post('todate');
		if ($todate < $fromdate) {
			$this->session->set_flashdata('error_fromdate','fromdate'); 
			redirect('Admin/MoneyInReport');
		} elseif ($fromdate > $today) {
			$this->session->set_flashdata('error_today','today');
			redirect('Admin/MoneyInReport');
		} else {

			$reports = $this->reports->countmoneyIn($fromdate, $todate);
			$count = 0;
			$data = [
				'title'   => 'Coffee &#8211; Report Money In',
				'reports' => $reports,
				'count'	  => $count,
				'profile' => $profile,
				'today'	  => $today,
				'fromdate' => $fromdate,
				'todate'  => $todate
			];

			$this->load->view('templates/header-admin', $data);
			$this->load->view('templates/sidebar-admin');
			$this->load->view('Admin/reportmoneyin/print');
			$this->load->view('templates/footer-admin');	

		}

	}

	public function detailOrder($date)
	{
		$id = $this->session->userdata('id_user');
		$profile = $this->profiles->selectById($id);
		$detailOrder = $this->reports->detailOrder($date); 
		$data = [
			'title' => 'Coffee &#8211; Detail Order',
			'profile' => $profile,
			'detailOrder' => $detailOrder,
			'date'	=> $date
		];

		$this->load->view('templates/header-admin', $data);
		$this->load->view('templates/sidebar-admin');
		$this->load->view('Admin/order/detailOrder');
		$this->load->view('templates/footer-admin');
	}

}
