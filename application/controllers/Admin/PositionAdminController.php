<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PositionAdminController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('profiles');
		$this->load->model('login');
		$this->load->model('positions');
		$this->load->helper('tgl_indo');
		$this->load->helper('rupiah');
		if($this->login->isNotLogin()) redirect(site_url('LoginController'));
	}

	public function index()
	{
		$level = $this->session->userdata('level');
		$id = $this->session->userdata('id_user');
		$profile = $this->profiles->selectById($id);
		$position = $this->positions->selectAll();
		if ($level == '1') { 
			$data = [
				'title' => 'Coffee &#8211; Postions',
				'profile' => $profile,
				'position' => $position
			];

			$this->load->view('templates/header-admin', $data);
			$this->load->view('templates/sidebar-admin');
			$this->load->view('Admin/position/index');
			$this->load->view('templates/footer-admin');

		}else{

			$data = [
				'title' => 'Coffee &#8211; Postions',
				'profile' => $profile
			];

			$this->load->view('templates/header-admin', $data);
			$this->load->view('404/index');
			$this->load->view('templates/footer-admin');
		}
	}


	public function store()
	{
		$name	=	$this->input->post('name');
		$cek 	=	$this->positions->cekdata($name);
		if ($cek) {
			$this->session->set_flashdata('error', 'message_failed');
			redirect('Admin/PositionAdminController');
		}else{
			$data = array(
				'name'	=> $name
			);
			$insert = $this->positions->store('position', $data);
			if ($insert) {
				$this->session->set_flashdata('success', 'message_success');
			}else{
				$this->session->set_flashdata('error', 'message_failed');
			}
			redirect('Admin/PositionAdminController');
		}
	}

	public function update()
	{
		$id = $this->input->post('id');
		$data = array(
		  'name'=> $this->input->post('name') 
		);
		$query = $this->db->update('position', $data, array('id_position' => $id));;
		$this->session->set_flashdata('success', 'message_success');
		redirect(base_url('Admin/PositionAdminController'));   
	}

	public function delete($id)
	{
		$this->positions->destroy($id);
		$this->session->set_flashdata('success', 'message_success');
		redirect(base_url('Admin/PositionAdminController'));
	}


}
