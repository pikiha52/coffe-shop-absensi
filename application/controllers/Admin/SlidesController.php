<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SlidesController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('profiles');
		$this->load->model('slides');
		$this->load->model('login');
		if($this->login->isNotLogin()) redirect(site_url('LoginController'));
	}

	public function index()
	{	
		$level = $this->session->userdata('level');
		if ($level == "3") {
			$title['title'] = 'Not for you!';
			$this->load->view('templates/header-admin', $title);
			$this->load->view('404/index');
			$this->load->view('templates/footer-admin');
		} else {
		$id = $this->session->userdata('id_user');
		$profile = $this->profiles->selectById($id);
		$slide = $this->slides->selectAll();
		$data = [
			'title' => 'Coffee &#8211; Slides',
			'slide' => $slide,
			'profile' => $profile
		];

		$this->load->view('templates/header-admin',$data);
		$this->load->view('templates/sidebar-admin');
		$this->load->view('Admin/slide/index');
		$this->load->view('templates/footer-admin');
	}
}


	function store()
	{
		$config['upload_path']          = './assets/image/slide/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 0;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
		$this->load->library('upload', $config);
		$this->upload->do_upload('image'); 
		{
			$_data = array('upload_data' => $this->upload->data());
			$data = array(
				'name'=> $this->input->post('name'),
				'image' => $_data['upload_data']['file_name']
			);

			$insert = $this->slides->store('slides',$data);
			if($insert){
				$this->session->set_flashdata('success', 'message_success');
			}else{
				$this->session->set_flashdata('error', 'message_failed');
			}
			redirect('Admin/SlidesController');
		}
	}

	public function delete($id)
	{
		$this->slides->destroy($id);
		$this->session->set_flashdata('success', 'message_success');
		redirect(base_url('Admin/SlidesController'));
	}


	public function update()
	{
		$id = $this->input->post('id');
		$today = date('Y-m-d h:i:s');
		$config['upload_path']          = './assets/image/slide/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 0;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
		$this->load->library('upload', $config);
		$this->upload->do_upload('image');
		{
			if (!$this->upload->do_upload('image')){

				$data = array(
					'name'			=> $this->input->post('name'),
					'created_at'	=> $today,
				);

				// var_dump($data);
				// die();

				$query = $this->db->update('slides', $data, array('id' => $id));;
				$this->session->set_flashdata('success', 'message_success');
				redirect(base_url('Admin/SlidesController'));      

			}else{
				$_data = array('upload_data' => $this->upload->data());
				$data = array(
					'name'			=> $this->input->post('name'),
					'created_at'	=> $today,
					'image' => $_data['upload_data']['file_name']
				);

				// var_dump($data);
				// die();

				$query = $this->db->update('slides', $data, array('id' => $id));
				$this->session->set_flashdata('success', 'message_success');
				redirect(base_url('Admin/SlidesController'));     
			} 
		}
	}
}

