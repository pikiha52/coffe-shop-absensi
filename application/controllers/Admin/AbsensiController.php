<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AbsensiController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('profiles');
    $this->load->model('absensi');
    $this->load->model('users');
    $this->load->model('login');
    if($this->login->isNotLogin()) redirect(site_url('LoginController'));
  }

  public function index()
  {
    $level = $this->session->userdata('level');
    $id = $this->session->userdata('id_user');
    if($level == "1"){
      $absensi = $this->absensi->get_detailabsensi($id);
      $absen = $this->users->selectAll($level);
      $profile = $this->profiles->selectById($id);
      $data = [
        'title' => 'Coffee &#8211; Detail Absensi',
        'profile' => $profile,
        'absen' => $absen,
        'absensi' => $absensi
      ];

      $this->load->view('templates/header-admin', $data);
      $this->load->view('templates/sidebar-admin');
      $this->load->view('Admin/absensi/index');
      $this->load->view('templates/footer-admin');
    }else{
      $month = date('m');
      $year = date('Y');
      $absensi = $this->absensi->get_detailabsensi($id);
      $absen = $this->users->selectAll($level);
      $profile = $this->profiles->selectById($id);
      $absensi= $this->absensi->selectAllAbsensiByUser($id,$month,$year); 
      $data = [
        'title' => 'Coffee &#8211; Detail Absensi',
        'profile' => $profile,
        'absen' => $absen,
        'absensi' => $absensi
      ];

      $this->load->view('templates/header-admin', $data);
      $this->load->view('templates/sidebar-admin');
      $this->load->view('Admin/absensi/index');
      $this->load->view('templates/footer-admin');
    }
  }

  public function detail($id)
  {
    $month = date('m');
    $year = date('Y');
    $level = $this->session->userdata('level');
    $absensi = $this->absensi->selectAllAbsensiByUser($id, $month, $year);
    $profile = $this->profiles->selectById($id);
    $data = [
      'title' => 'Coffee &#8211; Detail Absensi',
      'profile' => $profile,
      'absensi' => $absensi
            //   'absen' => $absen
    ];

    $this->load->view('templates/header-admin', $data);
    $this->load->view('templates/sidebar-admin');
    $this->load->view('Admin/absensi/detail-absen');
    $this->load->view('templates/footer-admin');

  }

  public function show_by_date()
  { 

    $year = $this->input->post("tahun");
    $month = $this->input->post("bulan");
    $id_user = $this->input->post("id_user");
    $id = $this->session->userdata('id_user');
    $profile = $this->profiles->selectById($id);

    if(!empty($month) && !empty($year)){
      $absensi ='';
      $level = $this->session->userdata('level');
      $groomer = $this->profiles->selectById($id_user);
      $absensi = $this->absensi->get_detailabsensi();
      if($level == "1") {
        $absensi = $this->absensi->selectAllAbsensiByUser($id_user,$month,$year);
        $data = [
          'title' => 'Coffee &#8211; Detail Absensi',
          'absensi' => $absensi,
          'groomer' => $groomer,
          'profile' => $profile
        ];
        
        $this->load->view('templates/header-admin', $data);
        $this->load->view('templates/sidebar-admin');
        $this->load->view('Admin/absensi/bymonth');
        $this->load->view('templates/footer-admin');

      }else{

        $absensi = $this->absensi->selectAllAbsensiByUser($id,$month,$year);
        $data = [
          'title' => 'Coffee &#8211; Detail Absensi',
          'absensi' => $absensi,
          'groomer' => $groomer,
          'profile' => $profile
        ];

        $this->load->view('templates/header-admin', $data);
        $this->load->view('templates/sidebar-admin');
        $this->load->view('Admin/absensi/detail-absen');
        $this->load->view('templates/footer-admin');

      }
    } 
  }

	public function listToday()
	{
		$today = date('Y/m/d');
		$id = $this->session->userdata('id_user');
    $profile = $this->profiles->selectById($id);
		$listToday = $this->absensi->listToday($today);

		$data = [
			'title' => 'Coffe &#8211; List Absensi Today',
			'profile' => $profile,
			'listToday' => $listToday
		];

		$this->load->view('templates/header-admin', $data);
		$this->load->view('templates/sidebar-admin');
		$this->load->view('Admin/absensi/listToday');
		$this->load->view('templates/footer-admin');

	}

}
