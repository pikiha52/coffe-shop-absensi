<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SalaryController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('profiles');
		$this->load->model('salarys');
		$this->load->model('users');
		$this->load->helper('rupiah_helper');
		$this->load->helper('tgl_indo');
		$this->load->model('login');
		if($this->login->isNotLogin()) redirect(site_url('LoginController'));
	}

	public function index()
	{
		$id = $this->session->userdata('id_user');
		$level = $this->session->userdata('level');
		if ($level == '3') {
			$title['title'] = 'Not for you!';
			$this->load->view('templates/header-admin', $title);
			$this->load->view('404/index');
			$this->load->view('templates/footer-admin');
		} else {
		$profile = $this->profiles->selectById($id);
		$salary = $this->salarys->selectSalarys();
		$position = $this->users->get_position();
		$user_id = $this->users->selectAll($level);
		$data = [
			'title' => 'Coffee &#8211; Salarys',
			'profile' => $profile,
			'salary' => $salary,
			'position' => $position,
			'user_id' => $user_id
		];


		$this->load->view('templates/header-admin',$data);
		$this->load->view('templates/sidebar-admin');
		$this->load->view('Admin/salary/index');
		$this->load->view('templates/footer-admin');
	}
}


	public function getSalary($id)
	{   
		$today = date('Y-m-d');
		$month = date('m');
		$year = date('Y');
		$id_user = $this->session->userdata('id_user');
		$position = $this->users->get_position();
		$salary = $this->salarys->selectSalarysUsers($id, $month, $year);
		$status = $this->salarys->storeSalary($id, $month, $year);
		$profile = $this->profiles->selectById($id_user);

		if ($salary == NULL) {
			$this->session->set_flashdata('error', 'message_error');
			redirect('Admin/UsersController');
		}else{

			$data = [
				'title' => 'Coffee &#8211; Data Karyawan',
				'profile' => $profile,
				'position' => $position,
				'salary' => $salary,
				'month' => $month,
				'today' => $today,
				'status' => $status,
				'id' => $id
			];


			$this->load->view('templates/header-admin',$data);
			$this->load->view('templates/sidebar-admin');
			$this->load->view('Admin/salary/detail');
			$this->load->view('templates/footer-admin');

		}
	} 


	public function search($id)
	{   
		$today = date('Y-m-d');
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$id_user = $this->session->userdata('id_user');
		$position = $this->users->get_position();
		$salary = $this->salarys->selectSalarysUsers($id, $month, $year);
		$status = $this->salarys->storeSalary($id, $month, $year);
		$profile = $this->profiles->selectById($id_user);

		if ($salary == NULL) {
			$this->session->set_flashdata('error', 'message_error');
			redirect('Admin/SalaryController/getSalary/'.$id);
		}else{

			$data = [
				'title' => 'Coffee &#8211; Data Karyawan',
				'profile' => $profile,
				'position' => $position,
				'salary' => $salary,
				'month' => $month,
				'today' => $today,
				'status' => $status,
				'id'	=> $id
			];

		// var_dump($salary);
		// die();

			$this->load->view('templates/header-admin',$data);
			$this->load->view('templates/sidebar-admin');
			$this->load->view('Admin/salary/detail');
			$this->load->view('templates/footer-admin');

		}


	} 


	public function store()
	{

		$id_position 	= $this->input->post('id_position');
		$user_id 		= $this->input->post('user_id');
		$salary 		= $this->input->post('salary');
		$transport 		= $this->input->post('transport');
		$cek 			= $this->salarys->cek($user_id);
		if ($cek) {
				$this->session->set_flashdata('error', 'message_failed');
				redirect('Admin/SalaryController');
		}else{
			$data = array(

				'user_id'		=> $user_id,
				'position_id'	=> $id_position,
				'salary'		=> $salary,
				'transport'		=> $transport

			);

			$insert = $this->salarys->store('salarys', $data);
			if ($insert) {
				$this->session->set_flashdata('success', 'message_success');
			}else{
				$this->session->set_flashdata('error', 'message_failed');
			}
			redirect('Admin/SalaryController');
		}	
	}


	public function storeTotal_salary()
	{
		$datetime = $this->input->post('date');
		$date = strtotime($datetime);
		$today = date('Y-m-d', $date);
		$id = $this->input->post('id_user');
		$salary = $this->input->post('total_salary');

		$data = array(
			'id_user' => $id,
			'total_salary' => $salary,
			'status' => 'Sudah',
			'date' => $today
		);

		$insert = $this->salarys->store("salarys_total", $data);
		if ($insert) {
			$this->session->set_flashdata('success', 'message_success');
			redirect('Admin/SalaryController/getSalary/'. $id);
		}

	}


	public function update($id)
	{
		$id_position	=	$this->input->post('position_id');
		$user_id 		=	$this->input->post('user_id');
		$salary 		=	$this->input->post('salary');
		$transport 		=	$this->input->post('transport');
		$cek 			=	$this->salarys->cek($salary);
		if ($cek) {
			$this->session->set_flashdata('error', 'message_failed');
			redirect('Admin/SalaryController');
		}else{

			$data = array(

				'user_id'		=> $user_id,
				'position_id'	=> $id_position,
				'salary'		=> $salary,
				'transport'		=> $transport

			);


			$insert = $this->db->update('salarys', $data, array('id' => $id));
			if ($insert) {
				$this->session->set_flashdata('success', 'message_success');
			}else{
				$this->session->set_flashdata('error', 'message_failed');
			}
			redirect('Admin/SalaryController');
		}
	}


	public function delete($id)
	{
		$this->salarys->destroy($id);
		$this->session->set_flashdata('success', 'message_success');
		redirect(base_url('Admin/SalaryController'));
	}


}
