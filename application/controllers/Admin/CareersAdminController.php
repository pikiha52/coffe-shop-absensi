<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CareersAdminController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('profiles');
		$this->load->model('absensi');
		$this->load->model('users');
		$this->load->model('careers');
		$this->load->model('login');
		if($this->login->isNotLogin()) redirect(site_url('LoginController'));
		$this->load->helper(array('download'));	

	}


	public function index()
	{	
		$level = $this->session->userdata('level');
		if ($level == "3") {
			$title['title'] = 'Not for you!';
			$this->load->view('templates/header-admin', $title);
			$this->load->view('404/index');
			$this->load->view('templates/footer-admin');
		} else {
		$id = $this->session->userdata('id_user');
		$profile = $this->profiles->selectById($id);
		$careers = $this->careers->selectAll();
		$data = [
			'title' => 'Coffee &#8211; Careers ',
			'profile' => $profile,
			'careers' => $careers
		];

        // var_dump($profile);
        // die();

		$this->load->view('templates/header-admin',$data);
		$this->load->view('templates/sidebar-admin');
		$this->load->view('Admin/careers/index');
		$this->load->view('templates/footer-admin');
	}
}

	public function store()
	{
		$today = date('Y-m-d');
		$data = array(
			'name' => $this->input->post('name'),
			'detail' => $this->input->post('detail'),
			'limit_aply' => $this->input->post('limit_aply'),
			'created_at' => $today
		);

		$insert = $this->careers->store('careers', $data);
		if ($insert) {
			$this->session->set_flashdata('success', 'message_success');
		}else{
			$this->session->set_flashdata('error', 'message_failed');
		}
		redirect('Admin/CareersAdminController');
	}


	public function update()
	{

		$id = $this->input->post('id');
		$data = array(
			'name'=> $this->input->post('name'),
			'detail'=> $this->input->post('detail') ,
			'limit_aply' => $this->input->post('limit_aply')
		);
		$query = $this->db->update('careers', $data, array('id' => $id));;
		$this->session->set_flashdata('success', 'message_success');
		redirect(base_url('Admin/CareersAdminController'));      

	}

	public function showApply($id)
	{
		$level = $this->session->userdata('level');
		if ($level == "3") {
			$title['title'] = 'Not for you!';
			$this->load->view('templates/header-admin', $title);
			$this->load->view('404/index');
			$this->load->view('templates/footer-admin');
		} else {	
		$id_user = $this->session->userdata('id_user');
		$profile = $this->profiles->selectById($id_user);
		$apply = $this->careers->selectApplywithID($id);

		$data = [
			'title' => 'Coffee &#8211; Careers ',
			'profile' => $profile,
			'apply' => $apply
		];

		// var_dump($apply);
		// die();

		$this->load->view('templates/header-admin',$data);
		$this->load->view('templates/sidebar-admin');
		$this->load->view('Admin/careers/apply');
		$this->load->view('templates/footer-admin');

	}
}

	public function detailApply($id)
	{
		$level = $this->session->userdata('level');
		if ($level == "3") {
			$title['title'] = 'Not for you!';
			$this->load->view('templates/header-admin', $title);
			$this->load->view('404/index');
			$this->load->view('templates/footer-admin');
		} else {
		$id_user = $this->session->userdata('id_user');
		$profile = $this->profiles->selectById($id_user);
		$apply = $this->careers->detailApplywithID($id);

		$data = [
			'title' => 'Coffee &#8211; Careers ',
			'profile' => $profile,
			'apply' => $apply	
		];

		$this->load->view('templates/header-admin',$data);
		$this->load->view('templates/sidebar-admin');
		$this->load->view('Admin/careers/detailapply');
		$this->load->view('templates/footer-admin');

	}
}


	function download($resume)
	{
		force_download('assets/resume/'. $resume, NULL);
	}


	public function delete($id)
	{
		$this->careers->destroy($id);
		$this->session->set_flashdata('success', 'message_success');
		redirect(base_url('Admin/CareersAdminController'));
	}
}
