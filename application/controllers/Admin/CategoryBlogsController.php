<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryBlogsController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');
    $this->load->model('profiles');
    $this->load->model('CategoryBlogs');
    $this->load->model('login');
    if($this->login->isNotLogin()) redirect(site_url('LoginController'));
  }


  public function index()
  {
    $level = $this->session->userdata('level');
		if ($level == "3") {
			$title['title'] = 'Not for you!';
			$this->load->view('templates/header-admin', $title);
			$this->load->view('404/index');
			$this->load->view('templates/footer-admin');
		} else {
    $id = $this->session->userdata('id_user');
    $profile = $this->profiles->selectById($id);
    $category = $this->CategoryBlogs->selectAll();
    $count = $this->CategoryBlogs->count($category);
    $data = [
      'title' => 'Coffee &#8211; Blogs',
      'profile' => $profile,
      'category' => $category,
      'count' => $count
    ];

    $this->load->view('templates/header-admin',$data);
    $this->load->view('templates/sidebar-admin');
    $this->load->view('Admin/blog/category');
    $this->load->view('templates/footer-admin');
  }
}


  public function add()
  {     
  $datetime = date('Y-m-d h:i:s');
   $data = array(
    'name'=> $this->input->post('name'),
    'date' => $datetime
  );

   $insert = $this->CategoryBlogs->store('category_blogs',$data);
   if($insert){
     $this->session->set_flashdata('success', 'message_success');
   }else{
     $this->session->set_flashdata('error', 'message_failed');
   }
   redirect('Admin/CategoryBlogsController') ;
 }

 public function update()
 {
  $id = $this->input->post('id');
  $data = array(
    'name'=> $this->input->post('name') 
  );
  $query = $this->db->update('category_blogs', $data, array('id' => $id));;
  $this->session->set_flashdata('success', 'message_success');
  redirect(base_url('Admin/CategoryBlogsController'));      
}

public function delete($id)
{
  $this->CategoryBlogs->destroy($id);
  $this->session->set_flashdata('success', 'message_success');
  redirect(base_url('Admin/CategoryBlogsController'));
}

}