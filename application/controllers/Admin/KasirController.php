<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KasirController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('menu');
		$this->load->model('profiles');
		$this->load->model('login');
		$this->load->helper('rupiah');
		$this->load->model('carts');
		$this->load->model('absensi');
		$this->load->model('menu');
		if($this->login->isNotLogin()) redirect(site_url('LoginController'));
	}

	public function index()
	{
		$kasir = $this->session->userdata('nip');
		$data = [
			'title' => 'Login for Cashier App',
			'kasir'	=> $kasir
		];

		$this->load->view('kasir/login', $data);
	}

	public function kasirApps($lat, $lng)
	{	
		$lat_company = -7.0909109;
		$lng_company = 107.668887;
    	$calculate = $this->haversineGreatCircleDistance($lat,$lng,$lat_company,$lng_company,2000000);
    	$meters = substr($calculate,0,3);
		if ($meters <= 100) {
			
			$level = $this->session->userdata('level');
			if ($level == "2") {
				$title['title'] = 'Not for you!';
				$this->load->view('templates/header-admin', $title);
				$this->load->view('404/index');
				$this->load->view('templates/footer-admin');
			} else {
			$today = date('Y-m-d');
			$time = date('h:i:s');
			$month = date('m');
			$year = date('Y'); 
			$id = $this->session->userdata('id_user');
			$profile = $this->profiles->selectById($id);
			$menu = $this->menu->selectAll();
			$carts = $this->carts->selectCarts();
			$numberorder = $this->carts->getNoOrder();
			$lastorder = $this->carts->getLastOrder();
			$getAllOrder = $this->carts->getOrdersbyId($id ,$month, $year);
			$access = $this->absensi->check($id, $today);
	
			if ($access == null) {
				$this->session->set_flashdata('error', 'kasir_failed');
				redirect('admin/welcome');
			} else {
	
				if($lastorder == NULL){
				
					$data = [
						'title' 	=> 'Coffee &#8211; Cashier Apps',
						'profile'	=> $profile,
						'menu'		=> $menu,
						'today'		=> $today,
						'time'		=> $time,
						'carts'		=> $carts,
						'numberorder' => $numberorder,
						'getAllOrder'	=> $getAllOrder,
						'profile'	=> $profile,
						'lat' => $lat,
						'lng' => $lng
					];
		
				}else{
		
					foreach($lastorder as $lastNumber){
						$lastIdOrder = $lastNumber->numberorder;
				}
		
				$orders = $this->carts->getOrders($lastIdOrder);
				$data = [
					'title' 	=> 'Coffee &#8211; Cashier Apps',
					'profile'	=> $profile,
					'menu'		=> $menu,
					'today'		=> $today,
					'time'		=> $time,
					'carts'		=> $carts,
					'numberorder' => $numberorder,
					'orders'	=> $orders,
					'getAllOrder'	=> $getAllOrder,
					'lastIdOrder'	=> $lastIdOrder,
					'profile'	=> $profile,
					'lat' => $lat,
					'lng' => $lng
				];
				}
			}
	
			$this->load->view('kasirtemp/header', $data);
			$this->load->view('kasir/index');
			$this->load->view('kasirtemp/footer');
	
		}

		} else {

			$title['title'] = 'Not for you!';
			$this->load->view('templates/header-admin', $title);
			$this->load->view('404/index');
			$this->load->view('templates/footer-admin');

		}
}


	function storeCarts($lat, $lng, $id)
	{	
		$id = $id;
		$data = [
			'id_menu' => $id
		];

		$insert = $this->carts->store('carts', $data);
		if ($insert) {
			$this->session->set_flashdata('success', 'message_success');
			redirect('admin/KasirController/kasirApps/'.$lat.'/'.$lng);
		}
	}

	public function storeOrder()
	{
		$id_cart = $this->input->post('id_carts');
		$datetime = date('Y:m:d G:i:s');
		$id_menu = $this->input->post('id_menu');
		$id_user = $this->session->userdata('id_user');
		$qty	= $this->input->post('qty');
		$total	= $this->input->post('total');
		$number = $this->input->post('number');
		$cash = $this->input->post('cash');
		$change = $this->input->post('change');
		$grand = $this->input->post('grand_total');
		$lat = $this->input->post('lat');
		$lng = $this->input->post('lng');
		$porsi = $this->input->post('porsi');

		if($cash < $grand){
			$this->session->set_flashdata('error', 'message_failed');
			redirect(base_url('admin/KasirController/kasirApps/'.$lat.'/'.$lng));
		}else{
		
			$data = array(
				'numberorder' => $number,
				'id_menu' => json_encode($id_menu),
				'id_user' => $id_user,
				'qty'	=> json_encode($qty),
				'total' => json_encode($total),
				'grand_total' => $grand,
				'cash' => $cash,
				'change' => $change,
				'created_at' => $datetime
			);
			
			}
	
			if($id_menu == NULL){
				$this->session->set_flashdata('error', 'message_menu_kosong');
				redirect(base_url('admin/KasirController/kasirApps/'.$lat.'/'.$lng));
			}elseif($cash == NULL){
				$this->session->set_flashdata('error', 'message_cash_kosong');
				redirect(base_url('admin/KasirController/kasirApps/'.$lat.'/'.$lng));
			}else{
				$countporsi = $porsi - json_encode($qty);
				$dataupdate = array(
					'porsi' => $countporsi,
					'menu_id' => $id_menu
				);
				$updatemenu = $this->db->update('our_menu', $data, array('id_menu' => $id_menu));
				$insert = $this->db->insert('orders', $data);
				if($insert|$updatemenu){
					$this->carts->destroy($id_cart);
					$this->session->set_flashdata('success');
					$this->session->set_flashdata('success', 'message_berhasil');
					redirect('admin/KasirController/kasirApps/'.$lat.'/'.$lng);
				}
			}
		}
	

	public function destroy($id)
	{
		$this->carts->destroy($id);
		$this->session->set_flashdata('success', 'message_success');
		redirect(base_url('Kasir/KasirController/kasirApps'));
	}


	function haversineGreatCircleDistance(
		$latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 2000000)
	  {
		// convert from derajat to radians
		$latFrom = deg2rad($latitudeFrom);
		$lonFrom = deg2rad($longitudeFrom);
		$latTo = deg2rad($latitudeTo);
		$lonTo = deg2rad($longitudeTo);
	
		$latDelta = $latTo - $latFrom;
		$lonDelta = $lonTo - $lonFrom;
	
		$angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
		  cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
		return $angle * $earthRadius;
	  }

}
