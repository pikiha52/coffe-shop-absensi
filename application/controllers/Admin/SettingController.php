<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SettingController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('profiles');
		$this->load->model('users');
		$this->load->model('login');
		if($this->login->isNotLogin()) redirect(site_url('LoginController'));
	}


	public function index()
	{
		$id = $this->session->userdata('id_user');
		$profile = $this->profiles->selectById($id);
		$detail_user = $this->profiles->selectByIdDetailUser($id);
		$data = [
			'title' => 'Coffee &#8211; Account settings',
			'profile' => $profile,
			'detail_user' => $detail_user
		];

		$this->load->view('templates/header-admin',$data);
		$this->load->view('templates/sidebar-admin');
		$this->load->view('Admin/users/setting');
		$this->load->view('templates/footer-admin');
	}


	public function update()
	{
		$id = $this->input->post('id_detail');
		$config['upload_path']		= './assets/image/profile/';
		$config['allowed_types']	= 'gif|jpg|jpeg|png';
		$config['max_size']			= 0;
		$config['max_width']		= 0;
		$config['max_height']		= 0;
		$this->load->library('upload', $config);

		if(!$this->upload->do_upload('photo')){

			$data = array(
				'email' => $this->input->post('email'),
				'firstname' => $this->input->post('firstname'),
				'lastname'	=> $this->input->post('lastname'),
				'facebook'	=> $this->input->post('facebook'),
				'instagram'	=> $this->input->post('instagram'),
				'twitter'	=> $this->input->post('twitter')
			);

			$query = $this->db->update('detail_user', $data, array('id_detail' => $id));;
			$this->session->set_flashdata('success', 'message_success');
			redirect(base_url('Admin/ProfileController'));
		}else{
			$_data = array('upload_data' => $this->upload->data());
			$data = array(
				'email' => $this->input->post('email'),
				'firstname' => $this->input->post('firstname'),
				'lastname'	=> $this->input->post('lastname'),
				'photo'     => $_data['upload_data']['file_name']
			);


			$query = $this->db->update('detail_user', $data, array('id_detail' => $id));;
			$this->session->set_flashdata('success', 'message_success');
			redirect(base_url('Admin/ProfileController'));
		}
	}

	public function update_password()
	{
		$id = $this->input->post('id_user');
		$nip = $this->input->post('nip');
		$cek_login = $this->login->cek_login($nip);
		$old_password = htmlspecialchars($this->input->post('old_password'));
		if (password_verify($old_password, $cek_login->password)) {
			$this->session->set_flashdata('error', 'password_error');
			redirect('Admin/SettingController');

		}else{

			$new_password = $this->input->post('new_password');
			$pass = password_hash($new_password, PASSWORD_DEFAULT);
			$data = array(
				'nip' => $nip,
				'password' => $pass,
			);

			// var_dump($data);
			// die();

			$query = $this->db->update('users', $data, array('id_user' => $id));
			$this->session->set_flashdata('success', 'password_succes');
			redirect('Admin/SettingController');
		}

	}


}