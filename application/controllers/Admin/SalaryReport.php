<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SalaryReport extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('profiles');
		$this->load->model('login');
		$this->load->model('reports');
		$this->load->helper('rupiah');
		$this->load->helper('tgl_indo');
		if($this->login->isNotLogin()) redirect(site_url('LoginController'));
	}

	public function index()
	{	
		$level = $this->session->userdata('level');
		if ($level == "3" && "2") {
			$title['title'] = 'Not for you!';
			$this->load->view('templates/header-admin', $title);
			$this->load->view('404/index');
			$this->load->view('templates/footer-admin');
		} else {
		$id = $this->session->userdata('id_user');
		$profile = $this->profiles->selectById($id);
		$data = [
			'title' => 'Coffee &#8211; Report Salarys',
			'profile' => $profile
		];

		$this->load->view('templates/header-admin', $data);
		$this->load->view('templates/sidebar-admin');
		$this->load->view('Admin/reportsalary/index');
		$this->load->view('templates/footer-admin');

	}
}

	public function lihat()
	{
		$today = date('Y:m:d');
		$id = $this->session->userdata('id_user');
		$profile = $this->profiles->selectById($id);
		$year = $this->input->post("tahun");
		$month = $this->input->post("bulan");
		$reports = $this->reports->selectReportSalary($year, $month);
		$count = $this->reports->countSalary($year, $month);
		$data = [
			'title'   => 'Coffee &#8211; Report Salarys',
			'reports' => $reports,
			'profile' => $profile,
			'month'	  => $month,
			'count'	  => $count,
			'today'	  => $today
		];



		$this->load->view('templates/header-admin', $data);
		$this->load->view('templates/sidebar-admin');
		$this->load->view('Admin/reportsalary/print');
		$this->load->view('templates/footer-admin');		

	}

}
