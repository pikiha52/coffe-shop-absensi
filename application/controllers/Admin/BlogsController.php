<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BlogsController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model('profiles');
    $this->load->model('blogs');
    $this->load->model('CategoryBlogs');
    $this->load->model('login');
    if($this->login->isNotLogin()) redirect(site_url('LoginController'));
  }


  public function index()
  {
    $level = $this->session->userdata('level');
    if ($level == "3") {
      $title['title'] = 'Not for you!';
			$this->load->view('templates/header-admin', $title);
			$this->load->view('404/index');
			$this->load->view('templates/footer-admin');
    } else {
    $id = $this->session->userdata('id_user');
    $profile = $this->profiles->selectById($id);
    $blog = $this->blogs->selectAllAdmin();
    $category = $this->CategoryBlogs->selectAll();
		$count = count($blog);
    $data = [
      'title' => 'Coffee &#8211; Blogs',
      'profile' => $profile,
      'blog' => $blog,
      'category' => $category,
			'count' => $count
    ];

    // var_dump($blog);
    // die();

    $this->load->view('templates/header-admin',$data);
    $this->load->view('templates/sidebar-admin');
    $this->load->view('Admin/blog/index');
    $this->load->view('templates/footer-admin');
  }
}

  public function add()
  {     
    $datetime = date('Y-m-d h:i:s');
    $config['upload_path']          = './assets/image/blogs/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['max_size']             = 0;
    $config['max_width']            = 0;
    $config['max_height']           = 0;
    $this->load->library('upload', $config);
    $this->upload->do_upload('image');
    {
      $_data = array('upload_data' => $this->upload->data());
      $data = array(
        'category_id' => $this->input->post('category_id'),
        'title'=> $this->input->post('title'),
        'isi'=> $this->input->post('isi'),
        'image' => $_data['upload_data']['file_name'],
        'date' => $datetime
      );

      $insert = $this->blogs->store('blogs',$data);
      if($insert){
       $this->session->set_flashdata('success', 'message_success');
     }else{
      $this->session->set_flashdata('error', 'message_failed');
    }
    redirect('Admin/BlogsController');
  }
}

function update()
{
  $id = $this->input->post('id');
  $config['upload_path']          = './assets/image/blogs/';
  $config['allowed_types']        = 'gif|jpg|png|jpeg';
  $config['max_size']             = 0;
  $config['max_width']            = 0;
  $config['max_height']           = 0;
  $this->load->library('upload', $config);

  if (!$this->upload->do_upload('images')){

   $data = array(
    'title'=> $this->input->post('title'),
    'isi'=> $this->input->post('isi'),
    'category_id' => $this->input->post('category_id'),
  );
            // var_dump($data);
            // die();

   $query = $this->db->update('blogs', $data, array('id' => $id));;
   $this->session->set_flashdata('success', 'message_success');
   redirect(base_url('Admin/BlogsController'));      

 }else{
  $_data = array('upload_data' => $this->upload->data());
  $data = array(
    'title'=> $this->input->post('title'),
    'isi'=> $this->input->post('isi'),
    'image' => $_data['upload_data']['file_name'],
    'category_id' => $this->input->post('category_id'),
  );
            // var_dump($data);
            // die();

  $query = $this->db->update('blogs', $data, array('id' => $id));
  $this->session->set_flashdata('success', 'message_success');
  redirect(base_url('Admin/BlogsController'));     
}
}




public function delete($id)
{
  $this->blogs->destroy($id);
  $this->session->set_flashdata('success', 'message_success');
  redirect(base_url('Admin/BlogsController'));
}

}
