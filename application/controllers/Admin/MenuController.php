<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MenuController extends CI_Controller {

    public function __construct()
    {
      parent::__construct();
      $this->load->model('menu');
      $this->load->model('profiles');
      $this->load->model('login');
      $this->load->helper('rupiah');
      if($this->login->isNotLogin()) redirect(site_url('LoginController'));
  }

  public function index()
  {
    $level = $this->session->userdata('level');
    if ($level == "3") {
        $title['title'] = 'Not for you!';
        $this->load->view('templates/header-admin', $title);
        $this->load->view('404/index');
        $this->load->view('templates/footer-admin');
    } else {
    $id = $this->session->userdata('id_user');
    $profile = $this->profiles->selectById($id);
    $ourmenu = $this->menu->selectAll();
    $data = [
        'title' => 'Coffee &#8211; Our Menu',
        'profile' => $profile,
        'ourmenu' => $ourmenu
    ];

    // var_dump($menu);
    // die();

    $this->load->view('templates/header-admin',$data);
    $this->load->view('templates/sidebar-admin');
    $this->load->view('Admin/menu/index');
    $this->load->view('templates/footer-admin');

    }
}

function add()
{
    $config['upload_path']          = './assets/image/menu/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['max_size']             = 0;
    $config['max_height']           = 0;
    $config['max_width']            = 0;
    $this->load->library('upload', $config);
    $this->upload->do_upload('image');
    {
        $_data = array('upload_data' => $this->upload->data());
        $data = array(
            'title'=> $this->input->post('title'),
            'price'=> $this->input->post('price'),
            'image' => $_data['upload_data']['file_name']
        );

        $insert = $this->menu->store('our_menu',$data);
        if($insert){
            echo 'berhasil di upload';
            redirect('Admin/MenuController');
        }else{
            echo 'gagal upload';
        }
    }
}

function addTopping()
{
    $config['upload_path']          = './assets/image/menu/topping/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['max_size']             = 0;
    $config['max_height']           = 0;
    $config['max_width']            = 0;
    $this->load->library('upload', $config);
    $this->upload->do_upload('images');
    {
        $_data = array('upload_data'  => $this->upload->data());
        $data = array(
            'name_topping'=> $this->input->post('name_topping'),
            'price_topping'=> $this->input->post('price_topping'),
            'images' => $_data['upload_data']['file_name']
        );

        var_dump($data);
        die();


    }
}

public function delete($id)
{
    $this->menu->destroy($id);
    $this->session->set_flashdata('pesan', '<script>alert("Data berhasil dihapus")</script>');
    redirect(base_url('Admin/MenuController'));
}

public function update()
{
    $id = $this->input->post('id_menu');
    $config['upload_path']          = './assets/image/menu/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['max_size']             = 0;
    $config['max_width']            = 0;
    $config['max_height']           = 0;
    $this->load->library('upload', $config);
  
    if (!$this->upload->do_upload('images')){
  
     $data = array(
      'title'=> $this->input->post('title'),
      'price'=> $this->input->post('price'),
    );
  
     $query = $this->db->update('our_menu', $data, array('id_menu' => $id));;
     $this->session->set_flashdata('success', 'message_success');
     redirect(base_url('Admin/MenuController'));      
  
   }else{

    $_data = array('upload_data' => $this->upload->data());
    $data = array(
      'title'=> $this->input->post('title'),
      'price'=> $this->input->post('price'),
      'image' => $_data['upload_data']['file_name']
    );
    // var_dump($data);
    // die();
    $query = $this->db->update('our_menu', $data, array('id_menu' => $id));
    $this->session->set_flashdata('success', 'message_success');
    redirect(base_url('Admin/MenuController'));     
  }
}

}
