<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PrintController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');
    $this->load->model('reports');
    $this->load->model('login');
    $this->load->model('menu');
    $this->load->helper('tgl_indo');
    $this->load->helper('rupiah');
    if($this->login->isNotLogin()) redirect(site_url('LoginController'));
  }

  public function money()
  {
    $fromdate = $this->input->post('fromdate');
    $todate = $this->input->post('todate');
    $today = date('Y-m-d');
    $reports = $this->reports->countMoneyin($fromdate, $todate);
    $count = 0;
    $data = [
      'title' => 'Print Reports',
      'reports' => $reports,
      'fromdate' => $fromdate,
      'todate' => $todate,
      'today' => $today,
      'count' => $count
    ];


    $this->load->view('templates/header-admin', $data);
    $this->load->view('admin/print/moneyIn');

  }  

  public function moneyIn($date)
  {
    $order = $this->reports->detailOrder($date);
    $data = [
      'title' => 'Print Reports',
      'order' => $order,
      'date' => $date
    ];

    $this->load->view('templates/header-admin', $data);
    $this->load->view('admin/print/moneyIndetail');

  }

	public function lihat()
	{
		$today = date('Y:m:d');
		$year = $this->input->post("tahun");
		$month = $this->input->post("bulan");
		$reports = $this->reports->selectReportSalary($year, $month);
		$count = $this->reports->countSalary($year, $month);
		$data = [
			'title'   => 'Coffee &#8211; Report Salarys',
			'reports' => $reports,
			'month'	  => $month,
			'count'	  => $count,
			'today'	  => $today
		];


		$this->load->view('templates/header-admin', $data);
		$this->load->view('admin/print/moneyOut');
	}


}