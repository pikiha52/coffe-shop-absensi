<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileController extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');
    $this->load->model('profiles');
    $this->load->model('absensi');
    $this->load->model('users');
    $this->load->model('login');
    if($this->login->isNotLogin()) redirect(site_url('LoginController'));
  }

  public function index()
  {	
   $id = $this->session->userdata('id_user');
   $profile = $this->profiles->selectById($id);
   $data = [
    'title' => 'Coffee &#8211; Data Karyawan',
    'profile' => $profile,
  ];

        // var_dump($profile);
        // die();

  $this->load->view('templates/header-admin',$data);
  $this->load->view('templates/sidebar-admin');
  $this->load->view('Admin/profile/index');
  $this->load->view('templates/footer-admin');
}

}