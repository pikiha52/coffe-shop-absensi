<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');
    $this->load->model('profiles');
    $this->load->model('absensi');
    $this->load->model('users');
    $this->load->model('login');
    $this->load->model('careers');
		$this->load->model('reports');
		$this->load->model('menu');
		$this->load->helper('rupiah');
    if($this->login->isNotLogin()) redirect(site_url('LoginController'));
    $this->load->library('pagination');
  }

  public function index()
  {    
    $config['base_url'] = 'http://localhost/coffeshop/Admin/welcome';
    $config['total_rows'] = $this->careers->count();
    $config['per_page'] = 5;
    $config['first_link']       = 'First';
    $config['last_link']        = 'Last';
    $config['next_link']        = 'Next';
    $config['prev_link']        = 'Prev';
    $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
    $config['full_tag_close']   = '</ul></nav></div>';
    $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['num_tag_close']    = '</span></li>';
    $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
    $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
    $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
    $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['prev_tagl_close']  = '</span>Next</li>';
    $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
    $config['first_tagl_close'] = '</span></li>';
    $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['last_tagl_close']  = '</span></li>';

    $this->pagination->initialize($config);
    $start = $this->uri->segment(3);
    $day = date('d');
    $month = date('m');
    $year = date('Y');
    $id = $this->session->userdata('id_user');
    $level = $this->session->userdata('level');
    $absensi = $this->absensi->get_absensi($id, $day,$month,$year);
    $kasir = $this->session->userdata('id_position');
    $status = '';
    if ($level == "1") {
      $status = 'Anda tidak perlu absen';
    }else{
      if ($level == "2"||"3") {
        if ($absensi == NULL) {
          $status = 'Anda belum melakukan absen';
        }else{
          if ($absensi->check_out == NULL) {
            $status = 'Anda belum absen pulang';
          }else{
            $status = 'Anda sudah absen';
          }
        }
      }
    }
		$today = date('Y:m:d');
    $level = $this->session->userdata('level');
    $id = $this->session->userdata('id_user');
    $users = $this->users->selectAllNonActive();
    $apply = $this->careers->listAplly($config['per_page'], $start);
    if($level == "1"){
			$month = date('Y-m');
			$emp_absen = $this->absensi->countToday($today);
			$active_emp = $this->users->selectAllactive();
			$count_act = count($active_emp);
			$moneyIn = $this->reports->countmoneyinMonth($month);
			$getmoney = $this->reports->getMoney($year);
      $profile = $this->profiles->selectById($id);
			$topMenu = $this->menu->counttopMenu($year)->result_array();
      $data = [
        'title' => 'Coffee &#8211; Dashboard',
        'profile' => $profile,
        'status' => $status,
        'apply' => $apply,
        'users' => $users,
				'emp_absen' => $emp_absen,
				'active_emp'	=> $count_act,
				'moneyIn' => $moneyIn,
				'topMenu' => $topMenu,
        'getmoney' => $getmoney
      ];
      

      $this->load->view('templates/header-admin',$data);
      $this->load->view('templates/sidebar-admin');
      $this->load->view('Admin/home/index');
      $this->load->view('templates/footer-admin');

    }else{
      $year = date('Y');
      $month = date('m');
			$date = date('Y-m-d');
      $profile = $this->profiles->selectById($id);
      $rekap = $this->absensi->selectAbsensibyMonth($config['per_page'], $start, $id, $month, $year);
			$access = $this->absensi->check($id, $date);
      $data = [
        'title' => 'Coffee &#8211; Dashboard',
        'profile' => $profile,
        'status' => $status,
        'rekap' => $rekap,
        'start' => $start,
        'apply' => $apply,
        'users' => $users,
        'kasir' => $kasir,
				'access' => $access

      ];


      $this->load->view('templates/header-admin',$data);
      $this->load->view('templates/sidebar-admin');
      $this->load->view('Admin/home/index');
      $this->load->view('templates/footer-admin');
    }
  }

  public function absen($lat,$lng)
  {
		$lat_company = -7.0909109;
		$lng_company = 107.668887;
    $calculate = $this->haversineGreatCircleDistance($lat,$lng,$lat_company,$lng_company,2000000);
    $meters = substr($calculate,0,3);
    $today = date('Y-m-d G:i:s');
    $day = date('d');
    $month = date('m');
    $year = date('Y');
    $current_datetime = date('Y-m-d G:i:s');
    $id = $this->session->userdata('id_user');
    $absensi = $this->absensi->get_absensi($id, $day,$month,$year);
    $hours = date('G:i:s');
    $user = $this->users->get_user($id);
    $hoursin = date('09:00');
    $hoursout = date('21:00');
    //absen masuk
    if ($hours >= $hoursin) {
        if ($meters < 200) {
          if ($absensi == NULL) {
              $data = [
              'id_user' => $this->session->id_user,
              'check_in' => $today,
              'check_out' => NULL,
              'lat' => $lat,
              'lng' => $lng
       ]; 
        $result = $this->absensi->insert_absen($data);
        if ($result) {
         $this->session->set_flashdata('success', 'absen_berhasil');
         } else {
          $this->session->set_flashdata('error', 'absen_gagal');
        }
            redirect('Admin/Welcome');
          } else {
            if ($hours >= $hoursout) {
              if ($absensi->check_out == NULL) {
                   $data = [
                  'check_out' => $current_datetime
                ];  
                $result = $this->absensi->update_data($absensi->id, $data);
                if ($result) {
                   $this->session->set_flashdata('success', 'absen_berhasil');
                } else {
                   $this->session->set_flashdata('error', 'absen_gagal');
               }
                redirect('Admin/Welcome');
              } else {
              $this->session->set_flashdata('success', 'sudah_absen');
              redirect('Admin/Welcome');
              } 
            } else {
              echo "tidak bisa absen jam segini";
              die;
           }
        }  
      } else {
        $this->session->set_flashdata('error', 'absen_jarak');
        redirect('Admin/Welcome');
      }
    }
  }



  //--------------------------------------------------------------------

  function haversineGreatCircleDistance(
    $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 2000000)
  {
    // convert from derajat to radians
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);

    $latDelta = $latTo - $latFrom;
    $lonDelta = $lonTo - $lonFrom;

    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
      cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
    return $angle * $earthRadius;
  }

}
