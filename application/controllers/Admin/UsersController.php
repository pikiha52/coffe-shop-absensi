<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsersController extends CI_Controller {

    public function __construct()
    {
      parent::__construct();

      $this->load->model('profiles');
      $this->load->model('users');
      $this->load->model('login');
      $this->load->model('salarys');
      $this->load->model('login');
      if($this->login->isNotLogin()) redirect(site_url('LoginController'));
      $this->load->helper('rupiah_helper');
      $this->load->helper('tgl_indo');
  }

  public function index()
  {
    $level = $this->session->userdata('level');
    if ($level == "3") {
        $title['title'] = 'Not for you!';
        $this->load->view('templates/header-admin', $title);
        $this->load->view('404/index');
        $this->load->view('templates/footer-admin');
    } else {
    if($level == "1"){
		$getNip = $this->users->getNumberUsers();
        $id = $this->session->userdata('id_user');
        $position = $this->users->get_position();
        $groomer = $this->users->selectAll($level); 
        $profile = $this->profiles->selectById($id);
        $data = [
            'title' => 'Coffee &#8211; Data Karyawan',
            'profile' => $profile,
            'groomer' => $groomer,
            'position' => $position,
			'getNip'	=> $getNip
        ];

        $this->load->view('templates/header-admin',$data);
        $this->load->view('templates/sidebar-admin');
        $this->load->view('Admin/users/index');
        $this->load->view('templates/footer-admin');

    }else{

        $id = $this->session->userdata('id');
        $groomer = $this->users->selectAll($level);
        $profile = $this->profiles->selectById($id);
        $data = [
            'title' => 'Coffee &#8211; Data Karyawan',
            'profile' => $profile,
            'groomer' => $groomer
        ];
        $this->load->view('templates/header-admin',$data);
        $this->load->view('templates/sidebar-admin');
        $this->load->view('Admin/users/index');
        $this->load->view('templates/footer-admin');

    }
}
  }


public function usersNonactive()
{
    $id = $this->session->userdata('id_user');
    $level = $this->session->userdata('level');
    $profile = $this->profiles->selectById($id);
    $groomer = $this->users->selectAllNonActive();

    if ($groomer == NULL) {

         $this->session->set_flashdata('error', 'message_error');
        redirect(base_url('Admin/UsersController'));

    } else {

    $data = [
        'title' => 'Coffee &#8211; Data Karyawan Non Active',
        'profile'   => $profile,
        'groomer'   => $groomer
    ];

    $this->load->view('templates/header-admin',$data);
    $this->load->view('templates/sidebar-admin');
    $this->load->view('Admin/users/index');
    $this->load->view('templates/footer-admin');
    }
}


function add()
{
    $this->form_validation->set_rules('firstname', 'Firstname', 'required');
    $this->form_validation->set_rules('lastname', 'lastname', 'required');
    $this->form_validation->set_rules('nip', 'Nip', 'required');
    $this->form_validation->set_rules('id_position', 'Id_position', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required|trim');

    if ($this->form_validation->run() == FALSE) {

        $errors = $this->form_validation->error_array();
        $this->session->set_flashdata('errors', $errors);
        $this->session->set_flashdata('input', $this->input->post());
        redirect('Admin/UsersController');

    } else {

        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $nip = $this->input->post('nip');
		$gender = $this->input->post('gender');
        $position = $this->input->post('id_position');
        $password = $this->input->post('password');
        $pass = password_hash($password, PASSWORD_DEFAULT);

        $data = [
            'nip' => $nip,
            'id_position' => $position, 
            'password' => $pass,
            'level' => 3,
            'status_user' => 1
        ];

		$store = $this->login->register("users", $data);
		$user = $this->users->getLastId();
		foreach($user as $us):
			$id = $us['id_user']; 
		endforeach;

        $data_detail = [
			'user_id' => $id,
            'firstname' => $firstname,
            'lastname' => $lastname,
			'gender' => $gender,
            'photo' => "default.png"
        ];

        $insert = $this->login->register('detail_user', $data_detail);

        if($insert){

            $this->session->set_flashdata('success', 'message_success');
            redirect('Admin/UsersController');

        }
    }
}

public function updateUsers()
{
    $id = $this->input->post('user_id');
    $status = $this->input->post('status_user');
    if ($status == NULL) {

        $data = array(
            'firstname' => $this->input->post('firstname'),
            'lastname'  => $this->input->post('lastname'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            'gender' => $this->input->post('gender'),
            'facebook'  => $this->input->post('facebook'),
            'instagram' => $this->input->post('instagram'),
            'twitter'   => $this->input->post('twitter')
        );

        $query = $this->db->update('detail_user', $data, array('user_id' => $id));
        $this->session->set_flashdata('success', 'message_success');
        redirect(base_url('Admin/UsersController'));

    } else {
      $_data = array(
        'status_user'   => $status
    );

      $query = $this->db->update('users', $_data, array('id_user' => $id));
      $this->session->set_flashdata('success', 'message_success');
      redirect(base_url('Admin/UsersController'));  
  } 
}


public function updateAccount()
{
    $id = $this->input->post('id_user');
    $password = $this->input->post('password');
    $pass = password_hash($password, PASSWORD_DEFAULT);
    $data = array(
        'nip' => $this->input->post('nip'),
        'id_position'  => $this->input->post('id_position'),
        'password' => $pass
    );

    $query = $this->db->update('detail_user', $data, array('user_id' => $id));;
    $this->session->set_flashdata('success', 'message_success');
    redirect(base_url('Admin/UsersController'));
}

public function updateStatus($id)
{

	$users = $this->users->byId($id);
	
	foreach($users as $user):
		$status = $user->status_user;
	endforeach;

	if($status == '1' ){
		$data = array(
			'status_user' => '2'
		);
	}else{
		$data = array(
			'status_user' => '1'
		);
	}

	$update = $this->db->update('users', $data, array('id_user' => $id));
    $this->session->set_flashdata('success', 'message_success');
    redirect(base_url('Admin/UsersController'));
	
}

public function delete($id)
{
    $this->users->destroy($id);
    $this->session->set_flashdata('success', 'message_success');
    redirect(base_url('Admin/UsersController'));
}
}
