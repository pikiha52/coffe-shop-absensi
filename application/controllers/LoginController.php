<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('login');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('user_agent');
    }

    public function index()
    {
        $check = $this->session->userdata('user_logged');
        if ($check == NULL) {
         
            $data = [
                'title' => 'Login for Employees'
            ];
            $this->load->view('frontend/login/index', $data);

        } else {

            $this->session->set_flashdata('sudah_login', 'login');
            redirect('Admin/welcome');

        }
    }
    
    public function proses_login()
    {   
        $this->form_validation->set_rules('nip', 'Nip', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE) {

            $errors = $this->form_validation->error_array();
            $this->session->set_flashdata('errors', $errors);
            $this->session->set_flashdata('input', $this->input->post());
            redirect('LoginController');

        } else {

            $nip = htmlspecialchars($this->input->post('nip'));
            $pass = htmlspecialchars($this->input->post('password')); 
            $cek_login = $this->login->cek_login($nip)->row();
			$cek_status = $this->login->cek_login($nip)->result();
		
			foreach($cek_status as $users) :
				$status = $users->status_user;
			endforeach;

            if($cek_login == FALSE)
            {

                $this->session->set_flashdata('error', 'login_failed_nip');
                redirect('LoginController');

            } elseif($status == 2 ){
				
                $this->session->set_flashdata('error', 'login_failed_status');
                redirect('LoginController');
				
			} elseif($status == 1) {

                if(password_verify($pass, $cek_login->password)){
                  
					$this->session->set_userdata(['user_logged' => $cek_login]);
                    $this->session->set_userdata('id_user', $cek_login->id_user);
                    $this->session->set_userdata('nip', $cek_login->nip);
                    $this->session->set_userdata('level', $cek_login->level); 
                    $this->session->set_flashdata('id_position', $cek_login->id_position);
                    
					{
					
						$this->session->set_flashdata('success', 'message_login');
                        redirect('Admin/welcome');
                    
					} 

                } else {

                    $this->session->set_flashdata('error', 'login_failed');
                    redirect('LoginController');

                }
            }
        }
    }
    
    public function logout()
    {
        $this->session->sess_destroy();
        echo '<script>
        alert("Sukses! Anda berhasil logout."); 
        window.location.href="'.base_url('LoginController').'";
        </script>';
    }

    public function notLogin()
    {
        $data = [
            'title' => 'Coffee &#8211; Input your password'
        ];
        $this->load->view('templates/header-admin', $data);
        $this->load->view('frontend/login/lock');
    }
}
