<?php

class Users extends CI_Model
{
	

	public function selectAll($level)
	{
		$query = $this->db->query("
			SELECT users.id_user, users.level,users.nip,users.status_user,detail_user.id_detail,detail_user.firstname,detail_user.lastname,detail_user.email,detail_user.photo,detail_user.phone,detail_user.gender,position.name,detail_user.facebook,detail_user.instagram,detail_user.twitter,users.id_position, detail_user.user_id FROM users 
			LEFT JOIN detail_user ON users.id_user = detail_user.user_id
			LEFT JOIN position ON users.id_position = position.id_position WHERE NOT level = '1' AND NOT level = {$level} 
			")->result();
		return $query;
	}

	public function selectAllNonActive()
	{
		$query = $this->db->query("
			SELECT users.id_user, users.level,users.nip,users.status_user,detail_user.id_detail,detail_user.firstname,detail_user.lastname,detail_user.email,detail_user.photo,detail_user.phone,detail_user.gender,position.name,detail_user.facebook,detail_user.instagram,detail_user.twitter,users.id_position, detail_user.user_id FROM users 
			LEFT JOIN detail_user ON users.id_user = detail_user.id_detail
			LEFT JOIN position ON users.id_position = position.id_position WHERE users.status_user = '2'
			")->result();
		return $query;
	}

	public function selectAllactive()
	{
		$query = $this->db->query("
		SELECT * FROM users WHERE status_user = '1' AND NOT level = '1'
		")->result();
		return $query;
	}


	public function update($table, $id, $users, $colomn)
	{
		$query = $this->db->query("
			UPDATE users INNER JOIN detail_user ON
			users.id_user = detail_user.user_id
			SET users.nip = 'pikiha'
			WHERE detail_user.user_id = {$id}
			")->result();
		return $query;
	}

	public function destroy($id)
	{
		$query = $this->db->query("
			DELETE users.*, detail_user.*, salarys.*, salarys_total.*, absen.*
			FROM users
			LEFT JOIN detail_user ON users.id_user = detail_user.user_id
			LEFT JOIN salarys ON users.id_user = salarys.user_id
			LEFT JOIN salarys_total ON users.id_user = salarys_total.id_user
			LEFT JOIN absen ON users.id_user = absen.id_user
			WHERE users.id_user = {$id}
			");
		return $query;
	}

	function get_position()  
	{
		$query = $this->db->query('SELECT * FROM position')->result();
		return $query;
	}

	function get_user($id)    {
		$query = $this->db->query("SELECT * FROM users where id_user = ${id}")->row();
		return $query;
	}

	function getNumberUsers()
	{
		$this->db->select('RIGHT(nip,2) as kode', FALSE);
		$this->db->order_by('id_user', 'DESC');
		$this->db->limit(1); 
		$query = $this->db->get('users');
		if ($query->num_rows() <> 0) {
			$data = $query->row();
			$kode = intval($data->kode) + 1;
		} else {
			$kode = 1;
		}
		$today = date('Y');
		$kodemax = str_pad($kode, 2, "0", STR_PAD_LEFT);
		$kodejadi = $today . $kodemax;
		return $kodejadi;
	}

	function getLastId()
	{
		$query = "SELECT id_user FROM users ORDER BY id_user DESC LIMIT 1";
		return $this->db->query($query)->result_array();
	}

	function byId($id)
	{
		$query = $this->db->query("
			SELECT * FROM users WHERE id_user = '$id'
		")->result();
		return $query;
	}


	// function count()
	// {
	// 	$query = $this->db->query('SELECT COUNT(nip) FROM users')->result();
	// 	return $query;
	// }
}

?>
