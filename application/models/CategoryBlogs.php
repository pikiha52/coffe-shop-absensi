<?php

class CategoryBlogs extends CI_Model
{

	function selectAll()
	{
		$query = $this->db->query("
			SELECT * FROM `category_blogs` WHERE id")->result();
		return $query;
	}


	function selectbyCategoryId($id)
	{
		$query = $this->db->query("
			SELECT category_blogs.id,
			category_blogs.name, blogs.category_id,blogs.title,blogs.image,blogs.isi,blogs.date,blogs.id as id_blogs
			FROM category_blogs
			RIGHT JOIN blogs ON category_blogs.id = blogs.category_id WHERE category_blogs.id = {$id}
			")->result();
		return $query;
	}


	function count($category)
	{
		return $this->db->count_all('category_blogs' ,$category);
	}


	public function store($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	public function destroy($id)
	{
		$query = $this->db->query("
		DELETE category_blogs, blogs FROM category_blogs INNER JOIN blogs
		WHERE category_blogs.id = blogs.category_id AND category_blogs.id = {$id}
			");
		return $query;
	}


}

?>
