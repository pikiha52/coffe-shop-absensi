<?php

class Menu extends CI_Model
{

	public function createId()
	{
		$query = $this->db->query("
			SELECT max(id_menu) as menu_id from our_menu 
		");
		if ($query->num_rows() <> 0) {
			$data = $query->row();
			$kode = intval($data->menu_id) +1;
		} else {
			$kode = 1;
		}
		$kodemax = str_pad($kode, 2, "0", STR_PAD_LEFT);
		$kodejadi = 'MNU' . $kodemax;
		return $kodejadi;
	}

	public function selectAll()
	{
		$query = $this->db->query("
			SELECT * FROM `our_menu`
			")->result();
		return $query;
	}

	public function show($id)
	{
		$query = $this->db->query("
		SELECT title FROM `our_menu` WHERE id_menu IN ($id);
			")->row();
		return $query->title;
	}

	public function byId($id)
	{
		$query = $this->db->query("
		SELECT * FROM `our_menu` where id_menu = '{$id}'
		")->row();
	return $query;

	}

	public function recentMenu($id)
	{
		$query = $this->db->query("
			SELECT * FROM `our_menu` where not id_menu = '{$id}' 
			group by title limit 3
			")->result();
		return $query;
	}

	public function destroy($id)
	{
		$query = $this->db->query("
			DELETE our_menu
			FROM our_menu
			WHERE our_menu.id_menu = {$id}
			");
		return $query;
	}

	function counttopMenu($years)
	{
		$query = $this->db->query(
			"SELECT our_menu.title, our_menu.image,our_menu.id_menu, 
			 COUNT(orders.id_menu) as total,
			 SUM(orders.qty) as qty
			 FROM our_menu
			 JOIN orders ON our_menu.id_menu = orders.id_menu
			 WHERE year(orders.created_at) = '$years'
			 GROUP BY our_menu.title LIMIT 3"
		);
		return $query;
	}

	public function store($table, $data)
	{
		return $this->db->insert($table, $data);
	}

}

?>
