<?php

class Blogs extends CI_Model
{

	public function selectAll()
	{
		$query = $this->db->query("
		SELECT blogs.*,
		category_blogs.name 
		FROM blogs
		LEFT JOIN category_blogs ON blogs.category_id = category_blogs.id
		")->result();
		return $query;
	}

	public function homeBlogs()
	{
		$query = $this->db->query("
		SELECT blogs.*,
		category_blogs.name 
		FROM blogs
		LEFT JOIN category_blogs ON blogs.category_id = category_blogs.id
		ORDER BY blogs.id DESC LIMIT 1
		")->result();
		return $query;
	}

	public function selectAllAdmin()
	{
		$query = $this->db->query("
			SELECT * FROM `blogs` 
			")->result();
		return $query;
	}

	public function detail($id){

		$query = $this->db->get_where('blogs', array('id' => $id))->row();
		return $query;

	}

	public function store($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	public function destroy($id)
	{
		$query = $this->db->query("
			DELETE blogs
			FROM blogs
			WHERE blogs.id = {$id}
			");
		return $query;
	}

	public function recent($id)
	{
		$query = $this->db->query("
		SELECT * FROM blogs WHERE NOT id = {$id} LIMIT 3;
		");
	return $query;
	}

	public function byCategoryid($id)
	{
		$query = $this->db->query("
		SELECT * FROM blogs WHERE category_id = {$id} 
			")->result();
		return $query;
	}

}

?>
