<?php

class Careers extends CI_Model
{

	public function selectAll()
	{
		$query = $this->db->query("
			SELECT * FROM `careers`
			")->result();
		return $query;
	}

	public function store($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	public function showPost($id)
	{
		$query = $this->db->get_where('careers', array('id' => $id))->row();
		return $query;
	}

	public function selectApplywithID($id)
	{
		$query = $this->db->query("
			SELECT careers_apply.*,
			careers.name
			FROM careers_apply
			LEFT JOIN careers ON careers_apply.careers_id = careers.id WHERE careers.id = {$id}
			")->result();
		return $query;
	}

	public function listAplly($limit)
	{
		$query = $this->db->query("
			SELECT careers_apply.*,
			careers.name
			FROM careers_apply
			LEFT JOIN careers on careers_apply.careers_id = careers.id LIMIT {$limit} 
			")->result();
		return $query;
	}

	public function detailApplywithID($id)
	{
		$query = $this->db->query("
			SELECT careers_apply.*,
			careers.name
			FROM careers_apply
			LEFT JOIN careers ON careers_apply.careers_id = careers.id WHERE careers_apply.id = {$id}
			")->result();
		return $query;
	}

	public function destroy($id)
	{
		$query = $this->db->query("
			DELETE careers, careers_apply FROM careers INNER JOIN careers_apply
			WHERE careers.id = careers_apply.careers_id AND careers.id = {$id}
			");
		return $query;
	}

	public function count()
	{
		$query = $this->db->query("
				SELECT * FROM `careers_apply`
			");
		return $query->num_rows();
	}

}

?>