<?php

class Slides extends CI_Model
{

	public function selectAll()
	{
		$query = $this->db->query("
			SELECT * FROM `slides` WHERE id")->result();
		return $query;
	}

	public function store($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	public function destroy($id)
	{
		$query = $this->db->query("
			DELETE slides
			FROM slides
			WHERE slides.id = {$id}
			");
		return $query;
	}

}

?>