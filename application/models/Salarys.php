<?php

class Salarys extends CI_Model
{

	function store($tabel, $data)
	{
		return $this->db->insert($tabel, $data);
	}

	function cek($salary)
	{
		$query = $this->db->query("
			SELECT salarys.* FROM salarys WHERE user_id = '$salary'")->result();
		return $query;
	}


	function selectSalarys()
	{
		$query = $this->db->query("
			SELECT salarys.*, salarys.position_id, position.name, detail_user.user_id , detail_user.firstname, detail_user.lastname
			FROM salarys 
			LEFT JOIN position ON salarys.position_id = position.id_position 
			LEFT JOIN detail_user on salarys.user_id = detail_user.user_id 
			WHERE salarys.id
			")->result();
		return $query;
	}


	function selectSalarysUsers($id,$month, $year)
	{
		$query = $this->db->query("
			SELECT salarys.id, salarys.salary, position.name, detail_user.* , 
			absen.id_user,absen.check_in,absen.check_out, salarys.transport, 
			SUM(salary) AS basic_salary, SUM(transport) AS transport_total,SUM(salary + transport) 
			as total_salary FROM salarys LEFT JOIN position ON salarys.position_id = position.id_position 
			LEFT JOIN detail_user on salarys.user_id = detail_user.user_id 
			LEFT JOIN absen ON detail_user.user_id = absen.id_user WHERE detail_user.user_id = {$id} 
			AND MONTH(absen.check_in) = {$month} AND year(absen.check_in) = {$year} GROUP BY detail_user.user_id
			")->result();
		return $query;
	}


	function countSalary()
	{
		$query = $this->db->query("
			SELECT SUM(salary) AS salary FROM salarys
			");
		return $query->row();
	}

	function storeSalary($id, $month, $year)
	{
		$query = $this->db->query("
			SELECT * FROM `salarys_total` WHERE id_user = {$id} AND MONTH(date) = {$month} AND YEAR(date) = {$year}
			")->result();
		return $query;
	}

	public function destroy($id)
	{
		$query = $this->db->query("
			DELETE salarys
			FROM salarys
			WHERE salarys.id = {$id}
			");
		return $query;
	}
}

?>
