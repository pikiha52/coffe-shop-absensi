<?php

class Absensi extends CI_Model
{

  public function get_absensi($id_user, $day,$month,$year ){
    $query = $this->db->query("
      SELECT * FROM 
      absen
      WHERE id_user =  $id_user
      and YEAR(check_in) = {$year} AND MONTH(check_in) = {$month} and DAY(check_in) = {$day}")->row();
    return $query;
  }

  public function get_detailabsensi($id = NULL){

    $query = $this->db->get_where('absen', array('id_user' => $id))->row();
    return $query;

  }

  function selectAllAbsensi($id_user)
  {
    $query = $this->db->query("
      SELECT * FROM `absen` WHERE id_user=$id_user")->result();
    return $query;
  }

  function selectAllAbsensiByUser($id_user, $month, $year)
  {
    $query = $this->db->query("
      SELECT absen.*,
      detail_user.* FROM absen LEFT JOIN detail_user ON absen.id_user = detail_user.user_id 
			WHERE absen.id_user = {$id_user} AND MONTH(absen.check_in) = {$month} AND YEAR(absen.check_in) = {$year}
      ")->result();
    return $query;
  }

	function check($id, $today)
  {
    $query = $this->db->query("
			SELECT absen.* FROM absen
			WHERE id_user = '$id' AND date(check_in) = '$today'
      ")->result();
    return $query;
  }

  public function insert_absen($data)
  {
    $result = $this->db->insert('absen', $data);
    return $result;
  }

  public function update_data($id, $data)
  {
    $this->db->where('id', $id);
    $result = $this->db->update('absen', $data);
    return $result;
  }

  public function selectAbsensibyMonth($limit, $start, $id, $month, $year)
  {
    $query = $this->db->query("
      SELECT absen.id,
      absen.id_user, absen.check_in, absen.check_out, detail_user.firstname, detail_user.lastname, detail_user.photo, detail_user.user_id
      FROM absen
      LEFT JOIN detail_user on absen.id_user = detail_user.user_id WHERE MONTH(absen.check_in) = {$month}  AND YEAR(absen.check_in) = {$year} AND detail_user.user_id = {$id} LIMIT {$limit}
      ")->result();
    return $query;
  }

  public function countbyMonth($id ,$month, $year)
  {
    $query = $this->db->query("
      SELECT * FROM absen WHERE MONTH(check_in) = {$month} AND id_user = {$id} AND YEAR(check_in) = {$year}
      ");
    return $query->num_rows();
  }

	public function countToday($today)
	{
		$query = $this->db->query("
		SELECT * FROM absen WHERE date(check_in) = '$today'
		");
		return $query->num_rows();
	}

	public function listToday($today)
	{
		$query = $this->db->query("
			SELECT absen.*,detail_user.firstname, detail_user.lastname,
			detail_user.photo FROM absen
			LEFT JOIN detail_user ON absen.id_user = detail_user.user_id
			WHERE date(absen.check_in) = '2021/06/25'
		")->result();
		return $query;
	}

}

?>
