<?php

class Positions extends CI_Model
{

	public function selectAll()
	{
		$query = $this->db->query("
			SELECT position.* FROM position
			")->result();
		return $query;
	}

	public function cekdata($name)
	{
		$query = $this->db->query("
			SELECT position.* FROM position WHERE name = '$name'")->result();
		return $query;
	}

	public function store($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	public function destroy($id)
	{
		$query = $this->db->query("
			DELETE position
			FROM position
			WHERE position.id_position = {$id}
			");
		return $query;
	}

}
?>