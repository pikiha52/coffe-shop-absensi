<?php

class Comment extends CI_Model
{

	public function selectAll()
	{
		$query = $this->db->query("
			SELECT * FROM comments WHERE id 
		")->result();
		return $query;
	}

	public function selectAllforHome()
	{
		$query = $this->db->query("
			SELECT * FROM comments WHERE id LIMIT 10 
		")->result();
		return $query;
	}

	public function store($table, $data)
	{
		return $this->db->insert($table, $data);
	}


}
?>
