<?php

class Carts extends CI_Model
{

	function selectCarts()
	{
		$query = $this->db->query("
			SELECT carts.*,
			our_menu.image, our_menu.title, our_menu.price, our_menu.porsi
			FROM carts
			LEFT JOIN our_menu on carts.id_menu = our_menu.id_menu
			")->result();
		return $query;
	}

	function getLastOrder()
	{
		$query = $this->db->query("
		SELECT * FROM orders ORDER BY numberorder DESC LIMIT 1
			")->result();
		return $query;
	}

	function getOrders($number)
	{
		$query = $this->db->query("
		SELECT orders.*,
		our_menu.title, detail_user.firstname, detail_user.lastname
		FROM orders
		LEFT JOIN our_menu on orders.id_menu = our_menu.id_menu
		LEFT JOIN detail_user on orders.id_user = detail_user.user_id
		WHERE orders.numberorder = '$number'
		")->result();
		return $query;
	}

	function getAllOrders($month, $year)
	{
		$query = $this->db->query("
		SELECT orders.*,
		our_menu.title, detail_user.firstname, detail_user.lastname
		FROM orders
		LEFT JOIN our_menu on orders.id_menu = our_menu.id_menu
		LEFT JOIN detail_user on orders.id_user = detail_user.user_id
		WHERE month(orders.created_at) = '$month' AND year(orders.created_at) ='$year'
		ORDER BY orders.numberorder DESC
		")->result();
		return $query;
	}

	function getOrdersbyId($id,$month, $year)
	{
		$query = $this->db->query("
		SELECT orders.*,
		detail_user.firstname, detail_user.lastname
		FROM orders
		LEFT JOIN detail_user on orders.id_user = detail_user.user_id
		WHERE orders.id_user = '$id' AND month(orders.created_at) = '$month' AND year(orders.created_at) ='$year'
		ORDER BY orders.numberorder DESC
		")->result();
		return $query;
	}

	public function store($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	public function destroy($id)
	{
        $this->db->where_in('id_carts', $id);
		$this->db->delete('carts');
	}

	function getNoOrder()
	{
		$this->db->select('RIGHT(numberorder,3) as kode', FALSE);
		$this->db->order_by('id_order', 'DESC');
		$this->db->limit(1); 
		$query = $this->db->get('orders');
		if ($query->num_rows() <> 0) {
			$data = $query->row();
			$kode = intval($data->kode) + 1;
		} else {
			$kode = 1;
		}
		$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
		$kodejadi = '#ORD-' . $kodemax;
		return $kodejadi;
	}

}

?>
