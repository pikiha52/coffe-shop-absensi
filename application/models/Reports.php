<?php

class Reports extends CI_Model
{

	function selectReportSalary($year, $month)
	{
		$query = $this->db->query("
			SELECT salarys_total.id_user,
			salarys_total.total_salary,salarys_total.date,detail_user.firstname,detail_user.lastname,detail_user.email,detail_user.phone
			FROM salarys_total
			LEFT JOIN detail_user ON salarys_total.id_user = detail_user.user_id WHERE YEAR(salarys_total.date) = {$year} AND MONTH(salarys_total.date) = {$month}
			")->result();
		return $query;
	}

	function countSalary($year, $month)
	{
		$query = $this->db->query("
			SELECT salarys_total.total_salary, SUM(total_salary) AS total FROM salarys_total WHERE YEAR(salarys_total.date) = {$year} AND MONTH(salarys_total.date) = {$month}
			")->result();
		return $query;
	}

	public function getMoney($year)
    {
		$query = $this->db->query("
		SELECT month(orders.created_at) as bulan, 
		SUM(orders.grand_total) AS total FROM orders
		WHERE year(orders.created_at) = '$year'
		GROUP BY month(orders.created_at);	
		")->result();
		return $query;
    }


	function moneyIn($fromdate, $todate)
	{
		$query = $this->db->query("
			SELECT orders.id_order,
			orders.id_menu,orders.id_user,orders.total,orders.created_at 
			FROM orders WHERE DATE(created_at) >= '$fromdate' AND DATE(created_at) <= '$todate'
			")->result();
		return $query;
	}

	function countmoneyIn($fromdate, $todate)
	{
		$query = $this->db->query("
		SELECT SUM(grand_total) AS grand_tot , year(created_at) AS year, month(created_at) as mounth , date(created_at) as date
		FROM orders
		WHERE date(created_at) >= '$fromdate' AND date(created_at) <= '$todate'
		GROUP BY year(created_at), month(created_at), date(created_at)
			")->result();
		return $query;
	}

	function countmoneyinMonth($month)
	{
		$query = $this->db->query("
			SELECT SUM(grand_total) AS grand_tot 
			FROM orders
			WHERE date(created_at) AND year(created_at) = '$month'
		")->result();
		return $query;

	}

	function detailOrder($date)
	{
		$query = $this->db->query("
		SELECT orders.*,
		detail_user.firstname, detail_user.lastname
		FROM orders
		LEFT JOIN detail_user on orders.id_user = detail_user.user_id
		WHERE date(orders.created_at) = '$date'
		")->result();
		return $query;
	}
}
?> 
