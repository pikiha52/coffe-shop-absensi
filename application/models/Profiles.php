<?php

class Profiles extends CI_Model
{

  public function selectAll()
  {
    $query = $this->db->query("
      SELECT users.id_user, users.level,users.nip,detail_user.firstname,detail_user.lastname,detail_user.email,detail_user.photo,detail_user.phone,detail_user.gender,detail_user.facebook,detail_user.twitter,detail_user.instagram,position.name 
      FROM users 
      LEFT JOIN detail_user ON users.id_user = detail_user.user_id
      LEFT JOIN position ON users.id_position = position.id_position WHERE users.id_user 
      ")->result();
    return $query;
  }

  public function showteam()
  {
    $query = $this->db->query(
      "SELECT users.id_user, users.level,users.nip,detail_user.firstname,detail_user.lastname,detail_user.email,detail_user.photo,detail_user.phone,detail_user.gender,detail_user.facebook,detail_user.twitter,detail_user.instagram,position.name
      FROM users 
      LEFT JOIN detail_user ON users.id_user = detail_user.user_id
      LEFT JOIN position ON users.id_position = position.id_position 
      WHERE users.id_user and NOT position.id_position = '4' and not position.id_position = '9' order BY users.id_user ASC"
    )->result();
    return $query;
  }

  public function selectById($id)
  {
    $query = $this->db->query("
      SELECT users.id_user, users.level,users.nip,detail_user.firstname,detail_user.lastname,detail_user.email,detail_user.photo,detail_user.phone,detail_user.gender,detail_user.facebook,detail_user.twitter,detail_user.instagram,position.name 
      FROM users 
      LEFT JOIN detail_user ON users.id_user = detail_user.user_id
      LEFT JOIN position ON users.id_position = position.id_position WHERE users.id_user = '{$id}'
      ")->result();
    return $query;
  }

  public function selectByIdDetailUser($id)
  {
    $query = $this->db->query("
        SELECT * FROM `detail_user` WHERE user_id = {$id}
      ")->result();
    return $query;
  }


  function update_data($where,$data,$table){
    $this->db->where($where);
    $this->db->update($table,$data);
  }	
  
  function insert_data($tabel,$data){
    return $this->db->insert($tabel, $data);
  }

}