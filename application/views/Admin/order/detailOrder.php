<!-- ############ Content START-->
<div id="content" class="flex">
	<!-- ############ Main START-->
	<div>
		<div class="page-hero page-container" id="page-hero">
			<div class="padding d-flex">
				<div class="page-title">
					<h2 class="text-md text-highlight">Detail Order </h2><small class="text-muted"></small>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="card">
				<div class="p-3-4">
					<div class="d-flex">
						<div>
							<div>Detail Order</div><small class="text-muted">Date: <?php echo $date ?></small>
						</div><span class="flex"></span>
						<!-- <div><a href="<?php echo base_url('admin/printcontroller/printMoneyin/'. $date) ?>" class="btn btn-sm btn-info"><i data-feather="printer"></i></a></div> -->
						<div><a href="<?php echo base_url('admin/printcontroller/moneyIn/'. $date) ?>" target="_blank" class="btn btn-sm btn-info"><i data-feather="printer"></i></a></div>
					</div>
				</div>
				<table class="table table-bordered table-hover" id="table-id" style="font-size:13px;">
					<thead>
						<th>Number Order</th>
						<th>Cashier</th>
						<th>Name Menu</th>
						<th>Qty</th>
						<th>Total</th>
						<th>Grand Total</th>
						<th>Money Cash</th>
						<th>Money Change</th> 
					</thead>
					<tbody>
						<?php $i = 1; ?>
						<?php foreach ($detailOrder as $order) :?>
						<tr>
							<td class="flex"><?php echo $order->numberorder ?></td>
							<td class="flex"><?php echo $order->firstname.' '. $order->lastname ?></td>
							<td class="flex">
								<?php foreach(json_decode($order->id_menu) as $orders) :?>
									<li><?php echo $this->menu->show($orders);?></li>
								<?php endforeach ;?>
							</td>
							<td class="flex">
								<?php foreach (json_decode($order->qty) as $qty) :?>
								<li><?php echo $qty ?></li>
								<?php endforeach ;?>
								</td>
							<td class="flex">
								<?php foreach (json_decode($order->total) as $total) :?>
									<li>Rp <?php echo rupiah($total) ?></li>
								<?php endforeach ;?>
							</td>
							<td class="flex">Rp <?php echo rupiah($order->grand_total) ?></td>
							<td class="flex">Rp <?php echo rupiah($order->cash) ?></td>
							<td class="flex">Rp <?php echo str_replace('-', ' ', rupiah($order->change)) ?></td>
						</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</div>
