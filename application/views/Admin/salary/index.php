    <!-- ############ Content START-->
    <div id="content" class="flex">
    	<!-- ############ Main START-->
    	<div>
    		<div class="page-hero page-container" id="page-hero">
    			<div class="padding d-flex">
    				<div class="page-title">
    					<h2 class="text-md text-highlight">Coffe shop</h2><small class="text-muted">List Salarys</small>
    				</div>
    			</div>
    		</div>
            <div class="col-md-12">
                <div class="card">
                    <div class="p-3-4">
                        <div class="d-flex">
                            <div>
                                <div>Data salarys : </div><small class="text-muted"></small>
                            </div><span class="flex"></span>
                            <div><a href="#" class="btn btn-sm btn-white" data-toggle="modal" data-target="#addModal"><i data-feather="plus"></i></a></div>
                        </div>
                    </div>
                    <table class="table table-bordered table-hover" id="table-id" style="font-size:13px;">
					<thead>
                        <th style="min-width:30px;text-align:center">#</th>
						<th>Name</th>
						<th>Position</th>
						<th>Salary Month</th>
						<th>Transport</th>
						<!-- <th>Action</th> -->
					</thead>
                        <tbody>
                            <?php $i = 1 ; ?>  <?php foreach($salary as $sal) : ?>
                            <tr id="<?php echo $sal->id ?>">
                                <td style="min-width:30px;text-align:center"><?= $i ?></td>
                                <td class="flex"><a class="item-company ajax h-1x"><span class="font-weight-bold"><?php echo $sal->firstname.' '. $sal->lastname ?></span></a>
                                </td>
                                <td><a class="btn btn-info badge"><span style="color:white;"><?php echo $sal->name ?></span></a></td>
                                <td><span class="item-amount d-none d-sm-block text-sm">Rp. <?php echo rupiah($sal->salary) ?>/days</span></td>
                                <td><span class="item-amount d-none d-sm-block text-sm">Rp. <?php echo rupiah($sal->transport) ?>/days</span></td>
                                <td>
                                 <div class="item-action dropdown"><a href="#" data-toggle="dropdown" class="text-muted"><i
                                    data-feather="more-vertical"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right bg-black" role="menu">
                                       <a class="dropdown-item" data-toggle="modal" data-target="#editmodal<?php echo $sal->id?>">Edit</a><a href="#!" class="dropdown-item remove">Delete</a>
                                   </div>
                               </div>
                           </td>
                       </tr>
                       <?php $i++ ?> <?php endforeach ;?>
                   </tbody>
               </table>
           </div>
       </div>


   </div>
   <!-- ============================================================== -->
   <!-- end hoverable table -->
   <!-- hoverable table -->
   <!-- ============================================================== -->
</div>


<!-- Add Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add Salarys</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
           <form action="<?php echo base_url('Admin/SalaryController/store'); ?>" method="post">
            <div class="form-group col-md-12">
                <label for="inputDivisi4">Users</label>
                <select class="form-control mb-2" name="user_id">
                    <option class="hidden" selected disabled>Select users
                    </option>
                    <?php foreach($user_id as $row) { ?>
                       <option value="<?php echo $row->id_user;?>"><?php echo $row->firstname;?></option>
                   <?php } ?>
               </select>
           </div>

           <div class="form-group col-md-12">
            <label for="inputDivisi4">Position</label>
            <select class="form-control mb-2" name="id_position">
                <option class="hidden" selected disabled>Select your Position
                </option>
                <?php foreach($position as $row) { ?>
                   <option value="<?php echo $row->id_position;?>"><?php echo $row->name;?></option>
               <?php } ?>
           </select>
       </div>
       <div class="form-group col-md-12">
        <label for="inputPassword4">Salarys</label>
        <input type="number" class="form-control" name="salary" id="salary" value="">
    </div>
    <div class="form-group col-md-12">
        <label for="inputPassword4">Transport</label>
        <input type="number" class="form-control" name="transport" id="transport" value="">
    </div>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">Save changes</button>
</div>
</div>
</form>
</div>
</div>
</div>



<!-- Edit Modal-->

<?php $no = 0;
foreach ($salary as $sal) : $no++; ?>
    <div class="modal fade" id="editmodal<?php echo $sal->id?>" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="<?php echo base_url(). 'Admin/SalaryController/update/'. $sal->id; ?>" enctype="multipart/form-data" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                     <div class="form-group col-md-12">
                        <label for="inputDivisi4">Users</label>
                        <select class="form-control mb-2" name="user_id" >
                            <?php foreach($user_id as $cat):?>
                              <option value="<?= $cat->id_user?>"
                                <?php if ($cat->id_user == $sal->user_id) : ?> selected<?php endif; ?>>
                                <?= $cat->firstname.' '. $cat->lastname?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group col-md-12">
                    <label for="inputDivisi4">Position</label>
                    <select class="form-control mb-2" name="position_id" >
                        <?php foreach($position as $post):?>
                          <option value="<?= $post->id_position?>"
                            <?php if ($post->id_position == $sal->position_id) : ?> selected<?php endif; ?>>
                            <?= $post->name?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group col-md-12">
                <label for="inputPassword4">Salarys</label>
                <input type="number" class="form-control" name="salary" id="salary" value="<?php echo $sal->salary ?>">
            </div>
        </div>

        <div class="form-group col-md-12">
            <label for="inputPassword4">Transport</label>
            <input type="number" class="form-control" name="transport" id="transport" value="<?php echo $sal->transport ?>">
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" value="upload" class="btn btn-primary">Save</button>
        </div>
    </form>
</div>
</div>
</div>


<?php endforeach;?>

<!-- End Edit Modal-->



<!-- Sweet Alert Delete -->
<script>
    $(".remove").click(function(){
        var id = $(this).parents("tr").attr("id");

        swal({
            title: "Apa kamu yakin?",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            type: "warning",
            showConfirmButton: true,
            confirmButtonClass: "btn-danger btn-sm",
            showCancelButton: true,
            cancelButtonClass: "btn-primary btn-sm",
            buttons: true,
            closeOnConfirm: false,
            closeOnCancel: false,
        },
        function(isConfirm) {
            if (isConfirm) {
              $.ajax({
                url: "<?php echo site_url('Admin/SalaryController/delete')?>/"+id,
                type: 'DELETE',
                error: function() {
                    alert('Something is wrong');
                },
                success: function(data) {
                  $("#"+id).remove();
                  swal("Dihapus!", "Data berhasil dihapus.", "success");
              }
          });
          } else {
              swal({
                type: "error",
                icon: "error",
                title: "Gagal!",
                text: " Data gagal dihapus! ",
                timer: 2500,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
          }
      });

    });
</script>
<!-- Sweet Alert Delete -->
