    <!-- ############ Content START-->
    <div id="content" class="flex">
    	<!-- ############ Main START-->
    	<div>
    		<div class="page-hero page-container" id="page-hero">
    			<div class="padding d-flex">
    				<div class="page-title">
    					<h2 class="text-md text-highlight">Coffe shop</h2><small class="text-muted">List Salarys</small>
    				</div>
    			</div>
    		</div>
    		<div class="col-md-12">
    			<div class="card">
    				<div class="p-3-4">
    					<div class="d-flex">
    						<div>
    							<div class="mb-2">Detail salarys </div><small class="text-muted"></small>
    							<div class="col-md-12">
    								<form action="<?php echo base_url('Admin/SalaryController/search/'. $id)?>"
    									method="post">
    									<div class="row">
    										<div class="col-3">
    											<label for="">Tahun</label>
    											<input type="text" id="tahun" name="year" class="form-control"
    												value="<?=date('Y')?>">
    										</div>
    										<div class="col-7">
    											<label for="laporan-bulan">Bulan</label>
    											<select name="month" id="bulan" class="form-control">
													<option value="0">Pilih Bulan</option>
    												<option value="01">Januari</option>
    												<option value="02">Februari</option>
    												<option value="03">Maret</option>
    												<option value="04">April</option>
    												<option value="05">Mei</option>
    												<option value="06">Juni</option>
    												<option value="07">Juli</option>
    												<option value="08">Agustus</option>
    												<option value="09">September</option>
    												<option value="10">Oktober</option>
    												<option value="11">November</option>
    												<option value="12">Desember</option>
    											</select>
    										</div>
    										<div class="col-2">
    											<label for="laporan-btn-lihat" style="color: white">as</label>
    											<button type="submit" class="btn btn-info btn-block btn-bg-gradient-x-blue-cyan"
    												id="laporan-btn-lihat"><i data-feather="search"></i></button>
    										</div>
    									</div>
    								</form>
    							</div>
    						</div><span class="flex"></span>
    					</div>
    				</div>
    				<div class="table-responsive">
    					<table id="datatable" class="table table-theme table-row v-middle" data-plugin="dataTable">
    						<thead>
    							<tr>
    								<th><span class="text-muted">NO</span></th>
    								<th><span class="text-muted">Profile</span></th>
    								<th><span class="text-muted">Name</span></th>
    								<th><span class="text-muted">Positions</span></th>
    								<th><span class="text-muted d-none d-sm-block">Salarys dailys</span></th>
    								<th><span class="text-muted d-none d-sm-block">Salary this month</span></th>
    								<th><span class="text-muted d-none d-sm-block"><i
    											data-feather="settings"></i></span></th>
    								<th></th>
    							</tr>
    						</thead>
    						<tbody>
    							<?php $i = 1 ;?>
    							<?php foreach($salary as $sal) :?>
    							<tr class="" data-id="16">
    								<td style="min-width:30px;"><small class="text-muted"><?= $i++ ?></small></td>
    								<td>
    									<div class="avatar-group"><a
    											href="<?php echo site_url('Admin/groomer/detail/').$sal->user_id ?>"
    											class="avatar ajax w-32" data-toggle="tooltip" title="Urna"><img
    												src="<?php echo base_url('assets/image/profile/') . $sal->photo ?>"
    												alt="."> </a></div>
    								</td>
    								<td class="flex"><a href="#"
    										class="item-title text-color"><?php echo $sal->firstname ?>
    										<?php echo $sal->lastname ?></a>
    								</td>
    								<td><span
    										class="item-amount d-none d-sm-block text-sm"><?php echo $sal->name ?></span>
    								</td>
    								<td><span class="item-amount d-none d-sm-block text-sm [object Object]">Rp.
    										<?php echo rupiah($sal->salary) ?> /days</span>
    								</td>
    								<td><span class="item-amount d-none d-sm-block text-sm [object Object]">Rp.
    										<?php echo rupiah($sal->total_salary) ?></span>
    								</td>
    								<td>
    									<?php if($status == NULL): ?>

    									<button
    										class="btn btn-success btn-sm  btn-bg-gradient-x-purple-blue box-shadow-2 gaji-slip"
    										data-toggle="modal" data-target="#slip" value="" title="Lihat slip gaji"><i
    											data-feather="printer"></i></button>

    									<button
    										class="btn btn-danger btn-sm  btn-bg-gradient-x-purple-blue box-shadow-2 gaji-bayar"
    										data-toggle="modal" data-target="#bayar" value=""
    										title="Gaji karyawan Belum dibayar!"><i
    											data-feather="check-square"></i></button></td>
    								<?php else:?>

    								<button
    									class="btn btn-success btn-sm  btn-bg-gradient-x-purple-blue box-shadow-2 gaji-slip"
    									data-toggle="modal" data-target="#slip" value="" title="Lihat slip gaji"><i
    										data-feather="printer"></i></button>

    								<button
    									class="btn btn-info btn-sm  btn-bg-gradient-x-purple-blue box-shadow-2 gaji-bayar"
    									data-toggle="modal" data-target="#sudahbayar" value=""
    									title="Gaji karyawan Sudah dibayar!"><i
    										data-feather="check-square"></i></button></td>
    								<?php endif;?>
    							</tr>
    							<?php endforeach ;?>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>


    	</div>
    	<!-- ============================================================== -->
    	<!-- end hoverable table -->
    	<!-- hoverable table -->
    	<!-- ============================================================== -->
    </div>



    <!-- Modal slip -->
    <style type="text/css">
    	.tengah {
    		text-align: center;
    	}

    	.kotak {
    		border: 1px solid rgba(0, 0, 0, 0.1);
    		padding: 5px;
    	}

    	@media print {
    		body * {
    			visibility: hidden;
    		}

    		.kotak,
    		.kotak * {
    			visibility: visible;
    		}

    		.kotak {
    			position: absolute;
    			width: 100%;
    			margin-top: 300px;
    			transform: scale(2);
    			left: 0;
    			top: 0;
    		}
    	}

    </style>

    <div class="modal fade text-left" id="slip" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35"
    	aria-hidden="true">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<div class="modal-header d-print-none">
    				<h3 class="modal-title" id="myModalLabel35"> Look Salary Slip</h3>
    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    					<span aria-hidden="true">&times;</span>
    				</button>
    			</div>
    			<div class="modal-body ">
    				<?php foreach($salary as $sal) :?>
    				<div class="kotak d-print-block">
    					<div class="row">
    						<div class="col-12">
    							<div class="tengah">
    								<h3><b>Coffe</b></h3>
    							</div>
    							<div class="tengah">
    								<!-- alamat -->
    							</div>
    							<hr>
    							<div class="tengah"><b><u>EMPLOYEE SALARY SLIP</u></b></div>
    							<br>
    						</div>
    					</div>
    					<div class="row">
    						<div class="col-6">
    							<table>
    								<tr>
    									<td>Name</td>
    									<td>:</td>
    									<td><span class="slip-nama"><?php echo $sal->firstname ?>
    											<?php echo $sal->lastname ?></span></td>
    								</tr>
    								<tr>
    									<td>Positions</td>
    									<td>:</td>
    									<td><span id="slip-jabatan"><?php echo $sal->name ?></span></td>
    								</tr>
    								<tr>
    									<td>Phone Number</td>
    									<td>:</td>
    									<td><span id="slip-nohp"><?php echo $sal->phone ?></span></td>
    								</tr>
    							</table>
    						</div>
    						<div class="col-6">
    							<table>
    								<tr>
    									<td>Month</td>
    									<td>:</td>
    									<td><span class="slip-bulan"><?php echo tanggal($month) ?></span></td>
    								</tr>
    								<!--                                             <tr>
                                                <td>Number of Days of Entry</td>
                                                <td>:</td>
                                                <td><span id="slip-hari"></span></td>
                                            </tr> -->
    							</table>
    						</div>
    					</div>
    					<br>
    					<div class="row">
    						<div class="col-6">
    							<p><b><u>Income</u></b></p>
    							<table style="width: 100%">
    								<tr>
    									<td>Basic Salary</td>
    									<td>:</td>
    									<td>Rp. <?php echo rupiah($sal->basic_salary)?><span id="slip-gaji"></span></td>
    								</tr>
    								<tr>
    									<td>Transport</td>
    									<td>:</td>
    									<td>Rp. <?php echo rupiah($sal->transport_total)?><span id="slip-gaji"></span>
    									</td>
    								</tr>
    								<tr>
    									<td><b>Total</b></td>
    									<td><b>:</b></td>
    									<td><b>Rp. <?php echo rupiah($sal->total_salary)?><span
    												id="slip-total"></span></b></td>
    								</tr>
    							</table>
    						</div>
    					</div>
    					<br>
    					<div class="row">
    						<div class="col-12">
    							<p class="tengah"><b>CLEAN ACCEPTANCE = Rp. <?php echo rupiah($sal->total_salary)?><span
    										id="slip-bersih"></span></b></p>
    						</div>
    					</div>
    					<!--                          <div class="row">
                                    <div class="col-12">
                                        <p class="tengah"><i>In number : <span id="slip-terbilang"></span></i></p>
                                    </div>
                                </div> -->
    					<div class="row">
    						<div class="col-6 text-center">
    							<p><br></p>
    							<p>Receiver</p>
    							<br>
    							<br>
    							<br>
    							<p><b><u><span class="slip-nama"><?php echo $sal->firstname?>
    											<?php echo $sal->lastname?></span></u></b></p>
    						</div>
    						<div class="col-6 text-center">
    							<?php foreach($profile as $prof) :?>
    							<p>Jakarta, <?php echo tanggal($today) ?></p>
    							<p style="color: white;">Manajer</p>
    							<br>
    							<br>
    							<br>
    							<p><b><u><?php echo $prof->firstname ?> <?php echo $prof->lastname ?></u></b></p>
    							<?php endforeach ;?>
    						</div>
    					</div>
    				</div>
    			</div>
    			<?php endforeach;?>
    			<div class="modal-footer d-print-none">
    				<input type="reset" class="btn btn-secondary btn-bg-gradient-x-red-pink" data-dismiss="modal"
    					value="Close">
    				<button onclick="window.print()" class="btn btn-primary btn-bg-gradient-x-purple-blue"><i
    						class="fa fa-print"></i> Print
    				</button>
    			</div>
    		</div>
    	</div>
    </div>


    <!-- Modal bayar -->
    <div class="modal fade text-left" id="bayar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35"
    	aria-hidden="true">
    	<div class="modal-dialog" role="document">
    		<form action="<?php echo base_url(). 'Admin/SalaryController/storeTotal_salary'; ?>"
    			enctype="multipart/form-data" method="post">
    			<?php foreach($salary as $sal) :?>
    			<div class="modal-content">
    				<div class="modal-header">
    					<h3 class="modal-title" id="myModalLabel35"> Karyawan sudah menerima gaji ?</h3>
    					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    						<span aria-hidden="true">&times;</span>
    					</button>
    					<input type="hidden" name="id_user" value="<?php echo $sal->user_id ?>">
    					<input type="hidden" name="total_salary" value="<?php echo $sal->total_salary ?>">
						<input type="hidden" name="date" value="<?php echo $sal->check_in ?>">
    				</div>
    				<div class="modal-footer">
    					<input type="reset" class="btn btn-secondary btn-bg-gradient-x-red-pink" data-dismiss="modal"
    						value="Tutup">
    					<div id="tombol-konfirmasi">
    						<input type="submit" class="btn btn-info btn-bg-gradient-x-red-pink" value="Sudah Bayar">
    					</div>
    				</div>
    			</div>
    			<?php endforeach ;?>
    		</form>
    	</div>
    </div>

    <div class="modal fade text-left" id="sudahbayar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35"
    	aria-hidden="true">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<h3 class="modal-title" id="myModalLabel35"> Karyawan sudah digaji!</h3>
    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    					<span aria-hidden="true">&times;</span>
    				</button>
    			</div>
    			<div class="modal-footer">
    				<input type="reset" class="btn btn-info btn-bg-gradient-x-red-pink" data-dismiss="modal"
    					value="Tutup">
    			</div>
    		</div>
    		</form>
    	</div>
    </div>
