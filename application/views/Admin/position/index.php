   <!-- ############ Content START-->
   <div id="content" class="flex">
     <!-- ############ Main START-->
     <div>
      <div class="page-hero page-container" id="page-hero">
       <div class="padding d-flex">
        <div class="page-title">
         <h2 class="text-md text-highlight">Coffe shop</h2><small class="text-muted">List Positions</small>
     </div>
 </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="p-3-4">
            <div class="d-flex">
                <div>
                    <div>Data Positions : </div>
                </div><span class="flex"></span>
                <div><a href="#" class="btn btn-sm btn-white" data-toggle="modal" data-target="#addModal"><i data-feather="plus"></i></a></div>
            </div>
        </div>
        <table class="table table-theme v-middle m-0">
            <tbody>
                <?php $i = 1 ; ?>  <?php foreach($position as $pos) : ?>
                <tr id="<?php echo $pos->id_position ?>">
                    <td style="min-width:30px;text-align:center"><?= $i ?></td>
                    <td class="flex"><?php echo $pos->name ?></a>
                    </td>
                    <td><a data-toggle="modal" data-target="#editmodal<?php echo $pos->id_position?>" class="btn btn-info badge" type="button"><span style="color: white">Edit</span></a>
                        <a class="btn btn-danger badge remove" type="button">Delete</a>
                    </td>
                </tr>
                <?php $i++ ?> <?php endforeach ;?>
            </tbody>
        </table>
    </div>
</div>

</div>
</div>


<!-- Add Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add Positions</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
           <form action="<?php echo base_url('Admin/PositionAdminController/store'); ?>" method="post">
            <div class="form-group col-md-12">
                <label for="inputPassword4">Name Positions</label>
                <input type="text" class="form-control" name="name" id="inputStartTime4" required="">
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </div>
</form>
</div>
</div>
</div>


<!-- Edit Modal-->

<?php $no = 0;
foreach ($position as $pos) : $no++; ?>
    <div class="modal fade" id="editmodal<?php echo $pos->id_position?>" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="<?php echo base_url(). 'Admin/PositionAdminController/update'; ?>" enctype="multipart/form-data" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Title</label> 
                            <input class="form-control" name="id" type="hidden" value="<?php echo $pos->id_position ?>">
                            <input class="form-control" name="name" type="text" value="<?php echo $pos->name ?>">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" value="upload" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


<?php endforeach;?>

<!-- End Edit Modal-->


<!-- Sweet Alert Delete -->
<script>
$(".remove").click(function(){
    var id = $(this).parents("tr").attr("id");

    swal({
        title: "Apa kamu yakin?",
        text: "Data yang dihapus tidak dapat dikembalikan!",
        type: "warning",
        showConfirmButton: true,
        confirmButtonClass: "btn-danger btn-sm",
        showCancelButton: true,
        cancelButtonClass: "btn-primary btn-sm",
        buttons: true,
        closeOnConfirm: false,
        closeOnCancel: false,
    },
    function(isConfirm) {
        if (isConfirm) {
          $.ajax({
           url: "<?php echo site_url('Admin/PositionAdminController/delete')?>/"+id,
           type: 'DELETE',
           error: function() {
            alert('Something is wrong');
        },
        success: function(data) {
          $("#"+id).remove();
          swal("Dihapus!", "Data berhasil dihapus.", "success");
      }
  });
      } else {
          swal({
                type: "error",
                icon: "error",
                title: "Gagal!",
                text: " Data gagal dihapus! ",
                timer: 2500,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
      }
  });

});
</script>
<!-- Sweet Alert Delete -->
