    <!-- ############ Content START-->
    <div id="content" class="flex">
    	<!-- ############ Main START-->
    	<div>
    		<div class="page-hero page-container" id="page-hero">
    			<div class="padding d-flex">
    				<div class="page-title">
    					<h2 class="text-md text-highlight">Coffe shop</h2><small class="text-muted">List Absensi</small>
    				</div>
    			</div>
    		</div>
    		<?php if($this->session->userdata('level')==='1'):?>
    		<div class="col-md-12">
    			<div class="card">
    				<div class="p-3-4">
    					<div class="d-flex">
    						<div>
    							<div>Data users : </div><small class="text-muted">users active</small>
    						</div><span class="flex"></span>
    						<div><a href="#" class="btn btn-sm btn-white" data-toggle="modal" data-target="#addModal"><i
    									data-feather="plus"></i></a></div>
    					</div>
    				</div>
    				<table class="table table-theme v-middle m-0">
    					<tbody>

    						<?php if($groomer == NULL): ?>
							
    						<tr>
							<div class="alert alert-info alert-dismissible fade show" role="alert">
                        	     <strong>Employee</strong> is null!.
                        	     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        	         <span aria-hidden="true">&times;</span>
                        	     </button>
                        	 </div>	
    						</tr>

    						<?php else: ?>

    						<?php $i = 1 ; ?> <?php foreach($groomer as $grm) : ?>

    						<tr id="<?php echo $grm->id_user ?>">
    							<td style="min-width:30px;text-align:center"><?= $i ?></td>
    							<td>
    								<div class="avatar-group"><a
    										href="<?php echo site_url('Admin/groomer/detail/').$grm->id_user ?>"
    										class="avatar ajax w-32" data-toggle="tooltip" title="Urna"><img
    											src="<?php echo base_url('assets/image/profile/') . $grm->photo ?>"
    											alt="."> </a></div>
    							</td>
    							<td class="flex"><a href="<?php echo site_url('Admin/groomer/detail/').$grm->id_user ?>"
    									class="item-company ajax h-1x"><?php echo $grm->firstname ?>
    									<?php echo $grm->lastname ?></a>
    							</td>
    							<td><span class="item-amount d-none d-sm-block text-sm"><?php echo $grm->nip ?></span>
    							</td>
    							<td><span class="item-amount d-none d-sm-block text-sm"><?php echo $grm->name ?></span>
    							</td>
								
    							<?php if($grm->status_user == '1') :?>
    							
								<td><span class="item-amount d-none d-sm-block text-sm"><a href="<?php echo site_url('Admin/UsersController/updateStatus/'. $grm->id_user) ?>"
    										class="btn btn-info badge">Active</a></span></td>
    							
								<?php else:?>
    							
								<td><span class="item-amount d-none d-sm-block text-sm"><a href="<?php echo site_url('Admin/UsersController/updateStatus/'. $grm->id_user) ?>"
    										class="btn btn-danger badge">Non Active</a></span></td>
    							<?php endif;?>

    							<td>
    							<td><span class="item-amount d-none d-sm-block text-sm"><a
    										href="<?php echo base_url('Admin/SalaryController/getSalary/'. $grm->user_id)?>"
    										class="btn btn-info badge">Salary</a></span></td>
    							<td>
    								<div class="item-action dropdown"><a href="#" data-toggle="dropdown"
    										class="text-muted"><i data-feather="more-vertical"></i></a>
    									<div class="dropdown-menu dropdown-menu-right bg-black" role="menu"><a
    											class="dropdown-item"
    											href="<?php echo site_url('Admin/AbsensiController/detail/').$grm->id_user ?>">See
    											detail employee</a>
    										<a class="dropdown-item"
    											href="<?php echo site_url('Admin/AbsensiController/detail/').$grm->id_user ?>">See
    											detail absent</a>
    										<div class="dropdown-divider"></div><a class="dropdown-item trash"
    											data-toggle="modal"
    											data-target="#editmodal<?php echo $grm->user_id?>">Edit</a><a
    											class="dropdown-item trash" data-toggle="modal"
    											data-target="#updatepassword<?php echo $grm->id_user?>">Settings
    											Account</a><a href="#!" class="dropdown-item remove">Delete</a>
    									</div>
    								</div>
    							</td>
    						</tr>

    						<?php $i++ ?> <?php endforeach ;?>
							
    						<?php endif; ?>
    					</tbody>
    				</table>
    			</div>
    		</div>

    		<?php elseif($this->session->userdata('level')==='2'):?>
    		<div class="col-md-12">
    			<div class="card">
    				<div class="p-3-4">
    					<div class="d-flex">
    						<div>
    							<div>Data users :</div><small class="text-muted">all users
    								<!-- <a href="<?php echo base_url('Admin/UsersController/usersNonactive') ?>" class="btn btn-info badge">non active</a> --></small>
    						</div><span class="flex"></span>
    					</div>
    				</div>
    				<table class="table table-theme v-middle m-0">
    					<tbody>
    						<?php $i = 1 ; ?> <?php foreach($groomer as $grm) : ?>
    						<tr class="" data-id="16">
    							<td style="min-width:30px;text-align:center"><?= $i ?></td>
    							<td>
    								<div class="avatar-group"><a
    										href="<?php echo site_url('Admin/UsersController/detail/').$grm->id_user ?>"
    										class="avatar ajax w-32" data-toggle="tooltip" title="Urna"><img
    											src="<?php echo base_url('assets/image/profile/') . $grm->photo ?>"
    											alt="."> </a></div>
    							</td>
    							<td class="flex"><a href="<?php echo site_url('Admin/groomer/detail/').$grm->id_user ?>"
    									class="item-company ajax h-1x"><?php echo $grm->firstname ?>
    									<?php echo $grm->lastname ?></a>
    							</td>
    							<td><span class="item-amount d-none d-sm-block text-sm"><?php echo $grm->nip ?></span>
    							</td>
    							<td><span class="item-amount d-none d-sm-block text-sm"><?php echo $grm->name ?></span>
    							</td>
    							<?php if($grm->status_user == '1') :?>
    							<td><span class="item-amount d-none d-sm-block text-sm"><a
    										class="btn btn-info badge"><span
    											style="color: white">Active</span></a></span></td>
    							<?php else:?>
    							<td><span class="item-amount d-none d-sm-block text-sm"><a
    										class="btn btn-danger badge"><span style="color: white">Non
    											Active</span></a></span></td>
    							<?php endif;?>
    							<td>
    							<td><span class="item-amount d-none d-sm-block text-sm"><a
    										href="<?php echo base_url('Admin/SalaryController/getSalary/'. $grm->user_id)?>"
    										class="btn btn-info badge"><span
    											style="color: white">Salary</span></a></span></td>
    							<td>
    								<div class="item-action dropdown"><a href="#" data-toggle="dropdown"
    										class="text-muted"><i data-feather="more-vertical"></i></a>
    									<div class="dropdown-menu dropdown-menu-right bg-black" role="menu"><a
    											class="dropdown-item"
    											href="<?php echo site_url('Admin/AbsensiController/detail/').$grm->id_user ?>">See
    											detail employee</a>
    										<a class="dropdown-item"
    											href="<?php echo site_url('Admin/AbsensiController/detail/').$grm->id_user ?>">See
    											detail absent</a>
    										<div class="dropdown-divider"></div><a class="dropdown-item trash"
    											data-toggle="modal"
    											data-target="#editmodal<?php echo $grm->user_id?>">Edit</a>
    									</div>
    								</div>
    							</td>
    						</tr>
    						<?php $i++ ?> <?php endforeach ;?>
    					</tbody>
    				</table>
    			</div>
    		</div>
    		<?php elseif($this->session->userdata('level')==='3'):?>
    		<?php else:?>
    		<?php endif ;?>
    	</div>
    	<!-- ============================================================== -->
    	<!-- end hoverable table -->
    	<!-- hoverable table -->
    	<!-- ============================================================== -->
    </div>

    <!-- Add Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    	aria-hidden="true">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<h5 class="modal-title" id="exampleModalLabel">Add Employee</h5>
    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    					<span aria-hidden="true">&times;</span>
    				</button>
    			</div>
    			<div class="modal-body">
    				<form action="<?php echo base_url('Admin/UsersController/add'); ?>" method="post">
    					
						<div class="form-group col-md-12">
    						<label for="inputPassword4">Nip</label>
    						<input type="text" class="form-control" name="nip" value="<?= $getNip ?>" readonly>
    					</div>

						<div class="form-group col-md-12">
    						<label for="inputPassword4">Firstname</label>
    						<input type="text" class="form-control" name="firstname" id="inputStartTime4" value="">
    					</div>
    					
						<div class="form-group col-md-12">
    						<label for="inputPassword4">Lastname</label>
    						<input type="text" class="form-control" name="lastname" id="inputStartTime4" value="">
    					</div>

						<div class="form-group col-md-12">
    						<label for="inputDivisi4">Gender</label>
    						<select class="form-control mb-2" name="gender">
    							<option class="hidden" selected disabled>Select Gender
    							</option>
    							<option value="Laki - laki">Male</option>
								<option value="Perempuan">Female</option>
    						</select>
    					</div>    					
    					
						<div class="form-group col-md-12">
    						<label for="inputDivisi4">Position</label>
    						<select class="form-control mb-2" name="id_position">
    							<option class="hidden" selected disabled>Select your Position
    							</option>
    							<?php foreach($position as $row) { ?>
    							<option value="<?php echo $row->id_position;?>"><?php echo $row->name;?></option>
    							<?php } ?>
    						</select>
    					</div>
    					
						<div class="form-group col-md-12">
    						<label for="inputPassword4">Password</label>
    						<input type="password" class="form-control" name="password" id="inputStartTime4" value="">
    					</div>

    			</div>
    			<div class="modal-footer">
    				<button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
    				<button type="submit" class="btn btn-primary">Save changes</button>
    			</div>
    		</div>
    		</form>
    	</div>
    </div>

    <!-- Edit Modal-->

    <?php $no = 0;
                foreach ($groomer as $grm) : $no++; ?>
    <div class="modal fade" id="editmodal<?php echo $grm->user_id?>" tabindex="-1" role="dialog"
    	aria-labelledby="exampleModalLabel" aria-hidden="true">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<form action="<?php echo base_url(). 'Admin/UsersController/updateUsers'; ?>" method="post">
    				<div class="modal-header">
    					<h5 class="modal-title" id="exampleModalLabel">Edit</h5>
    					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    						<span aria-hidden="true">&times;</span>
    					</button>
    				</div>
    				<div class="modal-body">
    					<!--                                 <div class="form-group">
                                    <label>Images</label>
                                    <input class="form-control" name="photo" type="file" value="<?php echo $grm->photo ?>">
                                </div> -->

    					<div class="form-group">
    						<label for="inputDivisi4">Status</label>
    						<select class="form-control mb-2" name="status_user">
    							<?php if($grm->status_user == '1' ) :?>
    							<option class="hidden" selected value="">Active
    								<?php elseif($grm->status_user == '2'):?>
    							<option class="hidden" selected value="">Non Active
    								<?php else:?>
    								<?php endif;?>
    							</option>
    							<option value="1">Active</option>
    							<option value="2">Non Active</option>
    						</select>
    					</div>
    					
						<div class="form-group">
    						<label>Firstname</label>
    						<input class="form-control" name="user_id" type="hidden"
    							value="<?php echo $grm->id_user ?>">
    						<input class="form-control" name="firstname" type="text"
    							value="<?php echo $grm->firstname ?>">
    					</div>

    					<div class="form-group">
    						<label>Lastname</label>
    						<input class="form-control" name="lastname" type="text"
    							value="<?php echo $grm->lastname ?>">
    					</div>
    					
						<div class="form-group">
    						<label>Email</label>
    						<input class="form-control" name="email" type="email" value="<?php echo $grm->email ?>">
    					</div>
						
    					<div class="form-group">
    						<label>Number Phone</label>
    						<input class="form-control" name="phone" type="text" value="<?php echo $grm->phone ?>">
    					</div>

    					<div class="form-group">
    						<label for="inputDivisi4">Gender</label>
    						<select class="form-control mb-2" name="gender">
    							<option class="hidden" selected value="<?php echo $grm->gender ?>">
    								<?php echo $grm->gender ?>
    							</option>
    							<option value="Male">Male</option>
    							<option value="Female">Female</option>
    						</select>
    					</div>

    					<div class="form-group">
    						<label>Facebook</label>
    						<input class="form-control" name="facebook" type="text"
    							value="<?php echo $grm->facebook ?>">
    					</div>
    					
						<div class="form-group">
    						<label>Instagram</label>
    						<input class="form-control" name="instagram" type="text"
    							value="<?php echo $grm->instagram ?>">
    					</div>
    					
						<div class="form-group">
    						<label>Twitter</label>
    						<input class="form-control" name="twitter" type="text" value="<?php echo $grm->twitter ?>">
    					</div>
    				
					</div>

    				<div class="modal-footer">
    					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    					<button type="submit" value="upload" class="btn btn-primary">Save</button>
    				</div>
    			</form>
    		</div>
    	</div>
    </div>


    <?php endforeach;?>

    <!-- End Edit Modal-->

    <!-- Setting Account Modal-->

    <?php $no = 0;
        foreach ($groomer as $grm) : $no++; ?>
    <div class="modal fade" id="updatepassword<?php echo $grm->id_user?>" tabindex="-1" role="dialog"
    	aria-labelledby="exampleModalLabel" aria-hidden="true">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<form action="<?php echo base_url(). 'Admin/CategoryBlogsController/update'; ?>"
    				enctype="multipart/form-data" method="post">
    				<div class="modal-header">
    					<h5 class="modal-title" id="exampleModalLabel">Edit</h5>
    					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    						<span aria-hidden="true">&times;</span>
    					</button>
    				</div>
    				<div class="modal-body">
    					<div class="form-group">
    						<label>NIP</label>
    						<input class="form-control" name="nip" type="text" value="<?php echo $grm->nip ?>">
    					</div>

    					<div class="form-group">
    						<label for="inputDivisi4">Position</label>
    						<select class="form-control mb-2" name="id_position">
    							<?php foreach($position as $post):?>
    							<option value="<?= $post->id_position?>"
    								<?php if ($post->id_position == $grm->id_position) : ?> selected<?php endif; ?>>
    								<?= $post->name?>
    							</option>
    							<?php endforeach; ?>
    						</select>
    					</div>

    					<div class="form-group">
    						<label>Password</label>
    						<input class="form-control" name="password" type="text" placeholder="Input new password"
    							value="">
    						<input class="form-control" name="id_user" type="hidden"
    							value="<?php echo $grm->id_user ?>">
    					</div>
    				</div>

    				<div class="modal-footer">
    					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    					<button type="submit" value="upload" class="btn btn-primary">Save</button>
    				</div>
    			</form>
    		</div>
    	</div>
    </div>


    <?php endforeach;?>

    <!-- End -->


    <!-- Logout Delete Confirmation-->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    	aria-hidden="true">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
    				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
    					<span aria-hidden="true">×</span>
    				</button>
    			</div>
    			<div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
    			<div class="modal-footer">
    				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
    				<a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
    			</div>
    		</div>
    	</div>
    </div>

    <!-- Sweet Alert Delete -->
    <script>
    	$(".remove").click(function () {
    		var id = $(this).parents("tr").attr("id");

    		swal({
    				title: "Apa kamu yakin?",
    				text: "Data yang dihapus tidak dapat dikembalikan!",
    				type: "warning",
    				showConfirmButton: true,
    				confirmButtonClass: "btn-danger btn-sm",
    				showCancelButton: true,
    				cancelButtonClass: "btn-primary btn-sm",
    				buttons: true,
    				closeOnConfirm: false,
    				closeOnCancel: false,
    			},
    			function (isConfirm) {
    				if (isConfirm) {
    					$.ajax({
    						url: "<?php echo site_url('Admin/UsersController/delete')?>/" + id,
    						type: 'DELETE',
    						error: function () {
    							alert('Something is wrong');
    						},
    						success: function (data) {
    							$("#" + id).remove();
    							swal("Dihapus!", "Data berhasil dihapus.",
    								"success");
    						}
    					});
    				} else {
    					swal({
    						type: "error",
    						icon: "error",
    						title: "Gagal!",
    						text: " Data gagal dihapus! ",
    						timer: 2500,
    						showConfirmButton: false,
    						showCancelButton: false,
    						buttons: false,
    					});
    				}
    			});

    	});

    </script>
    <!-- Sweet Alert Delete -->


    <script>
    	//sweetalert for success or error message
    	<?php
    	if ($this -> session -> flashdata('success') == 'message_success'): ?>
    		swal({
    			type: "success",
    			icon: "success",
    			title: "Berhasil",
    			text: "Selamat, data berhasil diupdate ",
    			timer: 1800,
    			showConfirmButton: false,
    			showCancelButton: false,
    			buttons: false,
    		}); 
			<?php elseif($this -> session -> flashdata('error') == 'message_error') :?>
    		swal({
    			type: "error",
    			icon: "error",
    			title: "Gagal!",
    			text: "Data gagal diupdate!",
    			timer: 1800,
    			showConfirmButton: false,
    			showCancelButton: false,
    			buttons: false,
    		}); 
			<?php endif; ?>

    </script>
