        <!-- ############ Content START-->
        <div id="content" class="flex">
        	<div class="page-content page-container" id="page-content">
        		<div class="padding">
        			<div id="accordion">
        				<p class="text-muted"><strong>Account</strong></p>
        				<div class="card">
        					<?php foreach ($detail_user as $detail) :?>
        					<div class="d-flex align-items-center px-4 py-3 pointer" data-toggle="collapse"
        						data-parent="#accordion" data-target="#c_1">
        						<div><span class="w-48 avatar circle bg-info-lt" data-toggle-class="loading"><img
        									src="<?php echo base_url('assets/image/profile/'. $detail->photo)?>"
        									alt="."></span></div>
        						<div class="mx-3 d-none d-md-block"><strong><?php echo $detail->firstname ?>
        								<?php echo $detail->lastname ?></strong>
        							<div class="text-sm text-muted"><?php echo $detail->email ?></div>
        						</div>
        						<div class="flex"></div>
        						<div class="mx-3"><i data-feather="chevron-right"></i></div>
        						<div><a  href="#" data-target=".bs-example-modal-sm" data-toggle="modal"
        								class="text-prmary text-sm">Sign Out</a></div>
        					</div>
        					<div class="collapse p-4" id="c_1">
        						<form action="<?php echo base_url(). 'Admin/SettingController/update'; ?>"
        							enctype="multipart/form-data" method="post">
        							<div class="form-group"><label>Profile picture</label>
        								<div class="custom-file"><input type="file" name="photo"
        										class="custom-file-input" id="photo"><label class="custom-file-label"
        										for="photo">Choose file</label></div>
        							</div>
        							<div class="form-group"><input type="hidden" name="id_detail" id="id_detail"
        									class="form-control" value="<?php echo $detail->id_detail ?>"></div>

        							<div class="form-group"><label>Email</label><input type="text" name="email"
        									id="email" class="form-control" value="<?php echo $detail->email ?>"></div>

        							<div class="form-group"><label>First Name</label><input type="text" name="firstname"
        									id="firstname" class="form-control"
        									value="<?php echo $detail->firstname ?>"></div>

        							<div class="form-group"><label>Last Name</label><input type="text" name="lastname"
        									id="lastname" class="form-control"
        									value="<?php echo $detail->lastname ?>"><input type="hidden"
        									name="instagram" id="instagram" value="<?php echo $detail->instagram ?>"
        									class="form-control"><input type="hidden" name="facebook" id="facebook"
        									value="<?php echo $detail->facebook ?>" class="form-control"><input
        									type="hidden" name="twitter" id="twitter"
        									value="<?php echo $detail->twitter ?>" class="form-control">
                                    </div>

        							<button type="submit" class="btn btn-primary mt-2">Update</button>

        						</form>
        					</div>
        					<?php endforeach ;?>
        					<div class="d-flex align-items-center px-4 py-3 b-t pointer" data-toggle="collapse"
        						data-parent="#accordion" data-target="#c_2"><i data-feather="lock"></i>
        						<div class="px-3">
        							<div>Password</div>
        						</div>
        						<div class="flex"></div>
        						<div><i data-feather="chevron-right"></i></div>
        					</div>
        					<div class="collapse p-4" id="c_2">

        						<form action="<?php echo base_url(). 'Admin/SettingController/update_password'; ?>"
        							enctype="multipart/form-data" method="post">
        							<?php foreach($profile as $prof) :?>
        							<div class="form-group"><label>Old Password</label><input type="password"
        									class="form-control" name="old_password"><input type="hidden" name="nip"
        									class="form-control" value="<?php echo $prof->nip ?>">

        								<input type="hidden" name="id_user" class="form-control"
        									value="<?php echo $prof->id_user ?>"></div>
                                            
        							<div class="form-group"><label>New Password</label><input name="new_password"
        									type="password" class="form-control"></div>

        							<button type="submit" class="btn btn-primary mt-2">Update</button>

        							<?php endforeach ; ?>
        						</form>

        					</div>
        					<div class="d-flex align-items-center px-4 py-3 b-t pointer" data-toggle="collapse"
        						data-parent="#accordion" data-target="#c_4"><i data-feather="slack"></i>
        						<div class="px-3">
        							<div>Social Media</div>
        						</div>
        						<div class="flex"></div>
        						<div><i data-feather="chevron-right"></i></div>
        					</div>
        					<div class="collapse p-4" id="c_4">
        						<form action="<?php echo base_url(). 'Admin/SettingController/update'; ?>"
        							enctype="multipart/form-data" method="post">
        							<?php foreach ($detail_user as $detail) :?>
        							<div class="form-group"><label>Facebook<small> ( input your url profile
        										)</small></label><input type="text" name="facebook" id="facebook"
        									value="<?php echo $detail->facebook ?>" class="form-control">
        								<input type="hidden" name="id_detail" id="id_detail"
        									value="<?php echo $detail->id_detail ?>" class="form-control">
        							</div>
        							<div class="form-group"><label>Instagram<small> ( input your url profile
        										)</small></label><input type="text" name="instagram" id="instagram"
        									value="<?php echo $detail->instagram ?>" class="form-control"></div>
        							<div class="form-group"><label>Twitter<small> ( input your url profile
        										)</small></label><input type="text" name="twitter" id="twitter"
        									value="<?php echo $detail->twitter ?>" class="form-control"><input
        									type="hidden" name="firstname" id="firstname" class="form-control"
        									value="<?php echo $detail->firstname ?>"><input type="hidden"
        									name="lastname" id="lastname" class="form-control"
        									value="<?php echo $detail->lastname ?>"><input type="hidden" name="email"
        									id="email" class="form-control" value="<?php echo $detail->email ?>"></div>
        							<button type="submit" class="btn btn-primary mt-2">Update</button>
        							<?php endforeach ;?>
        						</form>
        					</div>
        				</div>

        			</div>
        		</div>
        	</div><!-- ############ Main END-->
        </div><!-- ############ Content END-->
        <script>
    //sweetalert for success or error message
    <?php if($this->session->flashdata('success') == 'password_succes'):?>
        swal({
            type: "success",
            icon: "success",
            title: "Berhasil",
            text: "Password anda berhasil diubah ",
            timer: 1800,
            showConfirmButton: false,
            showCancelButton: false,
            buttons: false,
        });
        <?php elseif($this->session->flashdata('error') == 'password_error'):?>
            swal({
                type: "error",
                icon: "error",
                title: "Gagal!",
                text: " Password lama salah! ",
                timer: 1800,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
        <?php endif; ?>
    </script>