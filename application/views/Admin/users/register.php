<button type="button" class="btn btn-primary" data-target="#addModal" data-toggle="modal"></button>


<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Add Employee</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                         <form action="<?php echo base_url('Admin/UsersController/add'); ?>" method="post">
                                            <div class="form-group col-md-12">
                                                <label for="inputPassword4">Firstname</label>
                                                <input type="text" class="form-control" name="firstname" id="inputStartTime4" value="">
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="inputPassword4">Lastname</label>
                                                <input type="text" class="form-control" name="lastname" id="inputStartTime4" value="">
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="inputPassword4">Nip</label>
                                                <input type="text" class="form-control" name="nip" value="<?= $getNip ?>" readonly>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="inputDivisi4">Position</label>
                                                <select class="form-control mb-2" name="id_position">
                                                    <option class="hidden" selected disabled>Select your Position
                                                    </option>
                                                    <?php foreach($position as $row) { ?>
                                                     <option value="<?php echo $row->id_position;?>"><?php echo $row->name;?></option>
                                                 <?php } ?>
                                             </select>
                                         </div>
                                         <div class="form-group col-md-12">
                                            <label for="inputPassword4">Password</label>
                                            <input type="password" class="form-control" name="password" id="inputStartTime4" value="">
                                            <!-- <input type="hidden" class="form-control" name="id_porgress" id="inputStartTime4" value="<?php echo $e->id_porgress ?>"> -->
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
