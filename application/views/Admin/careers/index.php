    <!-- ############ Content START-->
    <div id="content" class="flex">
        <!-- ############ Main START-->
        <div class="col-md-12">
            <div class="card">
                <div class="p-3-4">
                    <div class="d-flex">
                        <div>
                            <div>Careers</div><small class="text-muted"></small>
                        </div><span class="flex"></span>
                        <div><a href="#" class="btn btn-sm btn-white" data-toggle="modal" data-target="#addModal"><i
                            data-feather="plus"></i></a></div>
                        </div>
                    </div>
                    <table class="table table-theme v-middle m-0">
                        <thead>
                            <tr>
                                <th data-sortable="true" data-field="id">NO</th>
                                <th data-sortable="true" data-field="owner">Name careers</th>
                                <th data-sortable="true" data-field="project">Limit apply</th>
                                <th data-field="task"><span class="d-none d-sm-block">Date post</span></th>
                                <th data-field="finish"><span class="d-none d-sm-block">Actions</span></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($careers as $slid) :?>
                                <tr id="<?php echo $slid->id ?>">
                                    <td style="min-width:30px;text-align:center"><?= $i++ ?></td>
                                    <td class="flex"><a class="item-company ajax h-1x"><?php echo $slid->name ?></a>
                                    </td>
                                    <td class="flex"><a class="item-company ajax h-1x"><?php echo $slid->limit_aply ?></a>
                                    </td>
                                    <td><span
                                        class="item-amount d-none d-sm-block text-sm"><?php echo $slid->created_at ?></span>
                                    </td>
                                    <td>
                                        <a data-toggle="modal" data-target="#editmodal<?php echo $slid->id ?>" type="button" class="btn btn-warning btn-sm">Edit</a>
                                        <a href="<?php echo base_url('Admin/CareersAdminController/showApply/'. $slid->id)?>" type="button" class="btn btn-info btn-sm">Show apply</a>
                                        <a href="#!" type="button" class="btn btn-danger btn-sm remove" >Delete careers</a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <!-- Add Modal -->
        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Careers</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?php echo base_url(). 'Admin/CareersAdminController/store'; ?>"
                        enctype="multipart/form-data" method="post">
                        <div class="form-group col-md-12">
                            <label for="inputPassword4">Name</label>
                            <input type="text" class="form-control" name="name" id="inputStartTime4" value="">
                        </div>
                        <!--               <div class="form-group col-md-12">
                  <label for="inputPassword4">Images</label>
                  <input type="file" class="form-control" name="image" id="inputStartTime4">
              </div> -->
              <div class="form-group col-md-12">
                <label for="inputType9">Detail Careers</label>
                <textarea class="form-control" id="ckeditor" cols="12" rows="5" name="detail"></textarea>
            </div>
            <div class="form-group col-md-12">
                <label for="inputType9">Limit Apply</label>
                <input type="date" class="form-control" name="limit_aply" id="inputStartTime4" value="">
            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>
</form>
</div>
</div>
</div>

<!-- End Add Modal -->


<!-- Edit Modal-->

<?php $no = 0;
foreach ($careers as $car) : $no++; ?>
    <div class="modal fade" id="editmodal<?php echo $car->id?>" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="<?php echo base_url(). 'Admin/CareersAdminController/update'; ?>"
                    enctype="multipart/form-data" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" name="id" type="hidden" value="<?php echo $car->id ?>">
                            <input class="form-control" name="name" type="text" value="<?php echo $car->name ?>">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputType9">Limit Apply</label>
                        <input type="date" class="form-control" name="limit_aply" id="inputStartTime4" value="<?php echo $car->limit_aply ?>">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputType9">Detail Careers</label>
                        <textarea class="form-control" id="ckeditoredit" cols="12" rows="5"
                        name="detail"><?php echo $car->detail ?></textarea>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" value="upload" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


<?php endforeach;?>

<!-- End Edit Modal-->

<!-- Logout Delete Confirmation-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
        </div>
    </div>
</div>
</div>


<script src="<?php echo base_url('assets/jquery/jquery-3.3.1.js');?>"></script>
<script src="<?php echo base_url('assets/ckeditor/ckeditor.js');?>"></script>

<script type="text/javascript">
//ckeditor function
$(function () {
    CKEDITOR.replace('ckeditor',{
        filebrowserImageBrowseUrl : '<?php echo base_url('assets/kcfinder/browse.php');?>',
        height: '400px'             
    });
    CKEDITOR.replace('ckeditoredit',{
        filebrowserImageBrowseUrl : '<?php echo base_url('assets/kcfinder/browse.php');?>',
        height: '400px'             
    });
});
//sweet aller delete function
$(".remove").click(function(){
    var id = $(this).parents("tr").attr("id");

    swal({
        title: "Apa kamu yakin?",
        text: "Data yang dihapus tidak dapat dikembalikan!",
        type: "warning",
        showConfirmButton: true,
        confirmButtonClass: "btn-danger btn-sm",
        showCancelButton: true,
        cancelButtonClass: "btn-primary btn-sm",
        buttons: true,
        closeOnConfirm: false,
        closeOnCancel: false,
    },
    function(isConfirm) {
        if (isConfirm) {
          $.ajax({
           url: "<?php echo site_url('Admin/CareersAdminController/delete')?>/"+id,
           type: 'DELETE',
           error: function() {
            alert('Something is wrong');
        },
        success: function(data) {
          $("#"+id).remove();
          swal("Dihapus!", "Data berhasil dihapus.", "success");
      }
  });
      } else {
          swal({
                type: "error",
                icon: "error",
                title: "Gagal!",
                text: " Data gagal dihapus! ",
                timer: 2500,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
      }
  });

});
</script>
