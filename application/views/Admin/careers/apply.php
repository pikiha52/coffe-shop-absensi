 <!-- ############ Content START-->
 <div id="content" class="flex">
 	<!-- ############ Main START-->
 	<div class="col-md-12">
 		<div class="card">
 			<div class="p-3-4">
 				<div class="d-flex">
 					<div>
 						<div>List Apply Careers</div><small class="text-muted"></small>
 					</div><span class="flex"></span>
 				</div>
 			</div>
			 <div class="table-responsive">
 			<table class="table table-theme v-middle m-0">
 				<thead>
 					<tr>
 						<th data-sortable="true" data-field="id">NO</th>
 						<th data-sortable="true" data-field="owner">Firstname</th>
 						<th data-sortable="true" data-field="project">Lastname</th>
 						<th data-field="task"><span class="d-none d-sm-block">Phone</span></th>
 						<th data-field="finish"><span class="d-none d-sm-block">Email</span></th>
 						<th data-field="finish"><span class="d-none d-sm-block">Position applied</span></th>
 						<th data-field="finish"><span class="d-none d-sm-block">Actions</span></th>
 						<th></th>
 					</tr>
 				</thead>
 				<tbody>
 					<?php $i = 1; ?>
 					<?php foreach ($apply as $slid) :?>
 					<tr class="" data-id="16">
 						<td style="min-width:30px;text-align:center"><?= $i++ ?></td>
 						<td class="flex"><a class="item-company ajax h-1x"><?php echo $slid->firstname ?></a>
 						</td>
 						<td class="flex"><a class="item-company ajax h-1x"><?php echo $slid->lastname ?></a>
 						</td>
 						<td class="flex"><a class="item-company ajax h-1x"><?php echo $slid->phone ?></a>
 						</td>
 						<td class="flex"><a class="item-company ajax h-1x"><?php echo $slid->email ?></a>
 						</td>
 						<td class="flex"><a class="item-company ajax h-1x"><?php echo $slid->name ?></a>
 						</td>

 						<td>
						<div class="row">
 							<a href="<?php echo base_url('Admin/CareersAdminController/detailapply/'. $slid->id)?>"
 								type="button" class="btn btn-info btn-sm"><i data-feather="eye"></i></a>
 							<a href="<?php echo base_url('Admin/CareersAdminController/download/'. $slid->resume)?>"
 								type="button" class="btn btn-info btn-sm"><i data-feather="save"></i></a>
 						</div>
						</td>
 						<?php endforeach ;?>
 					</tr>
 				</tbody>
 			</table>
			 </div>
 		</div>
 	</div>
 </div>
