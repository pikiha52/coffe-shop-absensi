    <!-- ############ Content START-->
    <div id="content" class="flex">
    	<!-- ############ Main START-->
    	<div class="col-md-12">
    		<?php foreach ($apply as $app ) :?>
    			<div class="page-content page-container" id="page-content">
    				<div class="padding">
    					<div class="row">
    						<div class="col-md-12">
    							<div class="card">
    								<div class="card-header"><strong>Detail Apply <?php echo $app->firstname ?> <?php echo $app->lastname ?></strong></div>
    								<div class="card-body">
    									<div class="form-group row"><label class="col-sm-4 col-form-label">Position Apply</label>
    										<div class="col-sm-8"><input type="text" class="form-control" readonly="" value="<?php echo $app->name ?>"></div>
    									</div>
    									<div class="form-group row"><label class="col-sm-4 col-form-label">Firsname</label>
    										<div class="col-sm-8"><input type="text" class="form-control" readonly="" value="<?php echo $app->firstname ?>"></div>
    									</div>
    									<div class="form-group row"><label
    										class="col-sm-4 col-form-label">Lastname</label>
    										<div class="col-sm-8"><input type="text" class="form-control" readonly="" value="<?php echo $app->lastname ?>"></div>
    									</div>
    									<div class="form-group row"><label class="col-sm-4 col-form-label"
    										for="input-id-1">Number Phone</label>
    										<div class="col-sm-8"><input type="text" class="form-control"
    											id="input-id-1" readonly="" value="<?php echo $app->phone ?>"></div>
    										</div>
    										<div class="form-group row"><label
    											class="col-sm-4 col-form-label">Email</label>
    											<div class="col-sm-8"><input type="text"
    												class="form-control" readonly="" value="<?php echo $app->email ?>"></div>
    											</div>
    											<div class="form-group row"><label
    												class="col-sm-4 col-form-label">Last Educations</label>
    												<div class="col-sm-8"><input type="text" class="form-control" readonly="" value="<?php echo $app->education ?>"></div>
    											</div>
    											<div class="form-group row"><label class="col-sm-4 col-form-label">Address</label>
    												<div class="col-sm-8"><input type="text" class="form-control" readonly="" value="<?php echo $app->address ?>"></div>
    											</div>
    											<div class="form-group row"><label
    												class="col-sm-4 col-form-label">How were you referred to us?</label>
    												<div class="col-sm-8"><input class="form-control" type="text"
    													readonly="" value="<?php echo $app->findme ?>"></div>
    												</div>
    												<div class="form-group row"><label class="col-sm-4 col-form-label">Resume</label>
    													<div class="col-sm-8">
    														<div class="form-control-plaintext"><a href="<?php echo base_url('Admin/CareersAdminController/download/'. $app->resume)?>"><?php echo $app->resume ?></a></div>
    													</div>		
    												</div>
    												<div class="form-group row"><label class="col-sm-4 col-form-label">Address<small> (optionals)</small></label>
    													<div class="col-sm-8"><textarea class="form-control" readonly=""><?php echo $app->additional ?></textarea></div>
    												</div>

    												<div class="form-group row"><label class="col-sm-4 col-form-label"></label>
    													<div class="col-sm-8">    												
														<a href="<?php echo base_url('Admin/CareersAdminController/showApply/'.$app->careers_id)?>" type="button" class="btn btn-info roun">Back</a></div>
    												</div>

    											</div>
    										</div>
    									</div>
    								<?php endforeach ;?>
    							</div>
    						</div>
