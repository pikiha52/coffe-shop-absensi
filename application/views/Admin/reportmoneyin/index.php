  <style type="text/css">
   .kotak {
      padding: 5px;
  }

  @page {
      size: A4;
      margin: 0;
  }

  @media print {
      body * {
         visibility: hidden;
     }

     .kotak, .kotak * {
         visibility: visible;
     }

     .kotak {
         z-index: 2;
         position: absolute;
         width: 100%;
         top: 0;
         left: 0;
     }
 }
</style>

<!-- ############ Content START-->
<div id="content" class="flex">
   <!-- ############ Main START-->
   <div>

      <div class="container">
         <div class="card mb-12">
            <div class="col-md">
               <div class="card-header" style="text-align: center;">
                  <h2 style="font-family: Montserrat;"><?= $title ?></h2>
              </div>
              <div class="col-md">
                  <!-- <h4 class="font-weight-bold">Detail Absensi</h4> -->
                  <div class="card-body">
                     <div>
                        <form action="<?php echo base_url('Admin/MoneyInReport/lihat')?>" method="post">
                           <div class="row">
                              <div class="col-4">
                                 <label for="laporan-tahun">From Date<span style="color: red"> *</span></label>
                                 <input id="fromdate" width="276" name="fromdate" required />
                             </div>
                             <div class="col-4">
                                 <label for="laporan-bulan">To Date<span style="color: red"> *</span></label>
                                 <input id="todate" width="276" name="todate" required />
                             </div>
                             <div class="col-2">
                               <label for="laporan-btn-lihat" style="color: white">as</label>
                               <button class="btn btn-info btn-block btn-bg-gradient-x-blue-cyan" id="laporan-btn-lihat" type="submit">Lihat</button>
                           </div>
                       </div>
                   </form>
               </div>
               <div id="laporan" class="kotak">

               </div>
           </div>
       </div>
   </div>
</div>
</div>


</div>
</div>

<script>
    $('#fromdate').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy/mm/dd'
    }); 
    $('#todate').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy/mm/dd'
    });


</script>

<script type="text/javascript">
       //sweetalert for success or error message
       <?php if($this->session->flashdata('error_fromdate') == 'fromdate'):?>
        swal({
            type: "error",
            icon: "error",
            title: "Gagal!",
            text: "Silahkan cek kembali,To date! ",
            timer: 3000,
            showConfirmButton: false,
            showCancelButton: false,
            buttons: false,
        });
        <?php elseif($this->session->flashdata('error_today') == 'today'):?>
            swal({
                type: "error",
                icon: "error",
                title: "Failed!",
                text: " Silahkan cek kembali,Fromdate!",
                timer: 3000,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
        <?php endif; ?>
    </script>

