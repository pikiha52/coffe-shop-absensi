    <style type="text/css">
    	.kotak {
    		padding: 5px;
    	}

    	@page {
    		size: A4;
    		margin: 0;
    	}

    	@media print {
    		body * {
    			visibility: hidden;
    		}

    		.kotak,
    		.kotak * {
    			visibility: visible;
    		}

    		.kotak {
    			z-index: 2;
    			position: absolute;
    			width: 100%;
    			top: 0;
    			left: 0;
    		}
    	}

    </style>

    <!-- ############ Content START-->
    <div id="content" class="flex">
    	<!-- ############ Main START-->
    	<div>

    		<div class="container">
    			<div class="card mb-12">
    				<div class="col-md">
    					<div class="card-header" style="text-align: center;">
    						<h2 style="font-family: Montserrat;"><?= $title ?></h2>
    					</div>
    					<div class="col-md">
    						<!-- <h4 class="font-weight-bold">Detail Absensi</h4> -->
    						<div class="card-body">
    							<div>
    								<form action="<?php echo base_url('Admin/MoneyInReport/lihat')?>" method="post">
    									<div class="row">
    										<div class="col-4">
    											<label for="laporan-tahun">From Date<span style="color: red">
    													*</span></label>
    											<input id="fromdate" width="276" name="fromdate" required />
    										</div>
    										<div class="col-4">
    											<label for="laporan-bulan">To Date<span style="color: red">
    													*</span></label>
    											<input id="todate" width="276" name="todate" required />
    										</div>
    										<div class="col-2">
    											<label for="laporan-btn-lihat" style="color: white">as</label>
    											<button class="btn btn-info btn-block btn-bg-gradient-x-blue-cyan"
    												id="laporan-btn-lihat" type="submit">Lihat</button>
    										</div>
    									</div>
    								</form>
    							</div>
    							<hr>

    							<div class="d-print-none float-right">
    								<a href="#" data-toggle="modal" data-target="#modalPrint"
    									class="btn btn-sm btn-info"><i data-feather="printer"></i>Cetak</a>
    							</div>
    							<br><br>
    							<div id="laporan" class="kotak">
    								<div class="card-header" style="text-align: center;">
    									<h2 style="font-family: Montserrat;"><?= $title ?></h2>
    									<p style="font-family: Montserrat;">Laporan :
    										<?= $fromdate.' - '. $todate ?></p>
    								</div>
    								<table class="table table-theme v-middle m-0">
    									<thead>
    										<tr>
    											<th data-sortable="true" data-field="id">NO</th>
    											<th data-field="finish"><span class="d-none d-sm-block">Date</span></th>
    											<th data-field="finish"><span class="d-none d-sm-block">Money In</span>
    											</th>
    											<th data-field="finish"><span class="d-none d-sm-block">Action</span>
    											</th>
    										</tr>
    									</thead>
    									<tbody>
    										<?php if($reports == NULL):?>
    										<div class="alert alert-info alert-dismissible fade show" role="alert">
    											<strong>Report</strong> this month null!.
    											<button type="button" class="close" data-dismiss="alert"
    												aria-label="Close">
    												<span aria-hidden="true">&times;</span>
    											</button>
    										</div>
    										<?php else:?>
    										<?php $i = 1 ; ?>
    										<?php foreach($reports as $report) :?>
    										<tr class="" data-id="16">
    											<td style="min-width:30px;"><?= $i++ ?></td>
    											</td>
    											<td class="flex"><?php echo $report->date ?></td>
    											<td class="flex">Rp <?php echo rupiah($report->grand_tot)?></td>
    											<td class="flex"><a
    													href="<?php echo base_url('Admin/MoneyInReport/detailOrder/').$report->date?>"
    													class="btn btn-info"><i data-feather="settings"></i></a></td>
    											<td class="hidden">Rp <?= $count += $report->grand_tot ?></td>
    										</tr>
    										<?php endforeach;?>
    										<?php endif;?>
    									</tbody>
    									<tfoot>
    										<tr>
    											<th data-sortable="true" data-field="owner">Total</th>
    											<th data-sortable="true" data-field="owner"></th>
    											<th data-field="finish"><span class="d-none d-sm-block">Rp.
    													<?php echo rupiah($count) ?></span></th>
    										</tr>
    									</tfoot>
    								</table>
    								<br><br>
    								<div class="row">
    									<div class="col-8"></div>
    									<div class="col-4 text-center">
    										<p>Jakarta, <?php echo tanggal($today) ?></p>
    										<p>Manajer</p>
    										<br>
    										<br>
    										<br>
    										<p>Fikih Alan</p>
    									</div>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>


    	</div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalPrint" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    	aria-hidden="true">
    	<div class="modal-dialog modal-lg" role="document">
    		<form action="<?php echo base_url('admin/printcontroller/money') ?>" method="post" target="_blank">
    			<div class="modal-content">
    				<div class="modal-header">
    					<h5 class="modal-title" id="exampleModalLabel">Print Laporan</h5>
    					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    						<span aria-hidden="true">&times;</span>
    					</button>
    				</div>
    				<div class="modal-body">
    					<div class="form-row">
    						<div class="form-group col-6">
    							<label for="">Fromdate</label>
    							<input type="text" class="form-control" value="<?php echo $fromdate ?>" readonly name="fromdate" id="fromdate">
    						</div>
    						<div class="form-group col-6">
    							<label for="">Todate</label>
    							<input type="text" class="form-control" name="todate" value="<?php echo $todate ?>" readonly id="fromdate">
    						</div>
    					</div>
    				</div>
    				<div class="modal-footer">
    					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    					<button type="submit" class="btn btn-primary">Print</button>
    				</div>
    			</div>
    		</form>
    	</div>
    </div>

    <script>
    $('#fromdate').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy/mm/dd'
    });
    $('#todate').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy/mm/dd'
    });


</script>

<script type="text/javascript">
       //sweetalert for success or error message
       <?php if($this->session->flashdata('error_fromdate') == 'fromdate'):?>
        swal({
            type: "error",
            icon: "error",
            title: "Gagal!",
            text: "Silahkan cek kembali,To date! ",
            timer: 3000,
            showConfirmButton: false,
            showCancelButton: false,
            buttons: false,
        });
        <?php elseif($this->session->flashdata('error_today') == 'today'):?>
            swal({
                type: "error",
                icon: "error",
                title: "Gagal!",
                text: " Silahkan cek kembali,Fromdate!",
                timer: 3000,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
        <?php endif; ?>
    </script>