

<div class="page-content page-container" id="page-content">
	<div class="padding">
		<a href="" class="btn btn-info">Back</a>
		<table id="example" class="table table-theme v-middle">
			<thead>
				<tr>
					<th width="100%">Name</th>
					<th width="100%">Check in</th>
					<th width="100%">Check out</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($absensi as $absen) :?>
					<tr class="" data-id="20">
						</td>
							<td class="flex"><a href="#" class="item-title text-color"><?php echo $absen->firstname.' '.$absen->lastname ?></a>
						</td>
						<td><?php echo $absen->check_in ?></td>
						<td><?php echo $absen->check_out ?>
						</td>
					</tr>
				<?php endforeach ;?>
			</tbody>
		</table>
	</div>
</div>



<script>

	$(document).ready(function() {
		var table = $('#example').DataTable( {
			lengthChange: false,
			buttons: [ 'copy', 'excel', 'pdf', 'print' ]
		} );

		table.buttons().container()
		.appendTo( '#example_wrapper .col-md-6:eq(0)' );
	} );

</script>
