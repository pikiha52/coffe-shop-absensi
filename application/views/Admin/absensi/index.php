    <!-- ############ Content START-->
    <div id="content" class="flex">
    	<!-- ############ Main START-->
    	<div>
    		<div class="page-hero page-container" id="page-hero">
    			<div class="padding d-flex">
    			</div>
    		</div>
            <?php if($this->session->userdata('level')==='1'):?>
                <div class="col-md-12">
                    <div class="card"> 
                        <div class="p-3-4">
                            <div class="d-flex">
                                <div>
                                    <div>Data absent :</div><small class="text-muted">users</small>
                                </div><span class="flex"></span>
                            </div>
                        </div>
                        <table class="table table-theme v-middle m-0">
                            <thead>
                                <tr>
                                    <th><span class="text-muted">NO</span></th>
                                    <th><span class="text-muted">Profile</span></th>
                                    <th><span class="text-muted">Name</span></th>
                                    <th><span class="text-muted d-none d-sm-block"><i data-feather="settings"></i></span></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1 ; ?> <?php foreach($absen as $absen) : ?>
                                <tr class="" data-id="16">
                                    <td style="min-width:30px"><?= $i ?></td>
                                    <td>
                                        <div class="avatar-group"><a href="#" class="avatar ajax w-32" data-toggle="tooltip"
                                            title="Urna"><img src="<?php echo base_url('assets/image/profile/') . $absen->photo ?>" alt="."> </a></div>
                                        </td>
                                        <td class="flex"><a href="<?php echo site_url('Admin/AbsensiController/detail/').$absen->id_user ?>" class="item-company ajax h-1x"><?php echo $absen->firstname ?> <?php echo $absen->lastname ?></a>
                                        </td>
                                        <td>
                                         <a
                                         class="btn btn-info btn-sm  btn-bg-gradient-x-purple-blue box-shadow-2 gaji-lihat"
                                         href="<?php echo base_url('Admin/AbsensiController/detail/'. $absen->id_user)?>"
                                         title="Look absent"><i data-feather="eye"></i></a>    
                                     </td>
                                 </tr>
                                 <?php $i++ ?> <?php endforeach ;?>
                             </tbody>
                         </table>
                     </div>
                 </div>
             <?php elseif($this->session->userdata('level')==='2'|$this->session->userdata('level')==='3'):?>
           <div class="container">
		     <div class="card mb-12">
                <?= $this->session->flashdata('absen'); ?>
				<div class="card-header" style="text-align: center;">
                    <h2 style="font-family: Montserrat;">Detail Absensi</h2>
                </div>
                <div class="col-md">
                    <div class="card-body">
                        <div>
                            <form action="<?php echo base_url('Admin/AbsensiController/show_by_date')?>" method="post">
                                <div class="row">
                                    <div class="col-3">
                                        <label for="laporan-tahun">Years</label>
                                        <?php foreach($profile as $grom):?>
                                            <input type="hidden" name="id_user" id="id_user" value="<?php echo $grom->id_user?>">
                                        <?php endforeach ;?>
                                        <input type="text" id="tahun" name="tahun" class="form-control" value="<?=date('Y')?>">
                                    </div>
                                    <div class="col-7">
                                        <label for="laporan-bulan">Month</label>
                                        <select name="bulan" id="bulan" class="form-control">
                                            <!-- <option value="<?=date('M')?>"><?=date('M')?></option> -->
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Maret</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Juni</option>
                                            <option value="07">Juli</option>
                                            <option value="08">Agustus</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                    <div class="col-2">
                                        <label for="laporan-btn-lihat" style="color: white">as</label>
                                        <button class="btn btn-info btn-block btn-bg-gradient-x-blue-cyan" id="laporan-btn-lihat">Lihat</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="laporan" class="kotak">

                        </div>
                    </div>
                </div> <br>
                <div class="card-header border-bottom">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <table class="table border-0">
                                <?php foreach($profile as $prfl) :?>
                                    <tr>
                                        <th class="border-0 py-0" style="font-family: Montserrat;">Nama</th>
                                        <th class="border-0 py-0">:</th>
                                        <th class="border-0 py-0" style="font-family: Montserrat;"><?php echo $prfl->firstname?> <?php echo $prfl->lastname?>
                                    </th>
                                </tr>
                                <tr>
                                    <th class="border-0 py-0" style="font-family: Montserrat;">Divisi</th>
                                    <th class="border-0 py-0">:</th>
                                    <th class="border-0 py-0" style="font-family: Montserrat;"><?php echo $prfl->name?>
                                </th>
                            </tr>
							<tr>
                                    <th class="border-0 py-0" style="font-family: Montserrat;">NIP</th>
                                    <th class="border-0 py-0">:</th>
                                    <th class="border-0 py-0" style="font-family: Montserrat;"><?php echo $prfl->nip?>
                                </th>
                            </tr>
                        <?php endforeach ;?>
                    </table>
                </div>
            </div>
        </div> <br>
        <!-- <h4 class="card-title mb-4">Absen Bulan : </h4> -->
        <table id="datatable" class="table table-theme table-row v-middle" data-plugin="dataTable">
            <thead>
                <tr>
                    <th><span class="text-muted" style="font-family: Montserrat;">NO</span></th>
                    <th><span class="text-muted" style="font-family: Montserrat;">Jam Masuk</span></th>
                    <th><span class="text-muted" style="font-family: Montserrat;">Jam Keluar</span></th>
                </tr>
            </thead>
            <tbody>
               <?php $i = 1 ; ?> <?php foreach($absensi as $u) : ?> 
               <?php if($absensi == NULL) :?>
                   <div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <strong>Tidak ada data absen bulan ini!</strong>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php else:?>
                   <tr class="" data-id="16">
                    <td style="min-width:30px;"><small
                        class="text-muted"><?= $i++ ?></small></td>
                        <td class="flex"><a href="#" class="item-title text-color" style="font-family: Montserrat;"><?php echo $u->check_in ?></a>
                        </td>
                        <td class="flex"><a href="#" class="item-title text-color" style="font-family: Montserrat;"><?php echo $u->check_out ?></a></td>
                    </tr>
                    <?php endif ;?>
                    <?php $i++ ?> <?php endforeach ;?>
                </tbody>
            </table>
        </div>
		   </div>
        <?php else:?>
        <?php endif;?>  
    </div>
</div>

    <script type="text/javascript">
        $('.alert').alert()
    </script>
