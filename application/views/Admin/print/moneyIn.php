<style type="text/css">
	.kotak {
		padding: 5px;
	}

	@page {
		size: A4;
		margin: 0;
	}

	@media print {
		body * {
			visibility: hidden;
		}

		.kotak,
		.kotak * {
			visibility: visible;
		}

		.kotak {
			z-index: 2;
			position: absolute;
			width: 100%;
			top: 0;
			left: 0;
		}
	}

</style>
<!-- ############ Content START-->
<div id="content" class="flex">
	<!-- ############ Main START-->
	<div>

		<!-- <div class="page-hero page-container" id="page-hero">
			<div class="padding d-flex">

			</div>
		</div> -->
		<div class="col-md-12">
			<div class="card">
				<div id="laporan" class="kotak">
					<div class="card-header" style="text-align: center;">
						<h2 style="font-family: Montserrat;">
							<img src="<?php echo base_url('assets/image/logo/coffee-cup.svg')?>" alt="" height="50">
							LAPORAN UANG MASUK</h2>
						<p style="font-family: Montserrat;">Dari Tanggal :
							<?= $fromdate.'  '.' Sampai Tanggal :'. $todate ?></p>
					</div>
					<table class="table table-theme v-middle m-0">
						<thead>
							<tr>
								<th data-sortable="true" data-field="id">NO</th>
								<th data-field="finish"><span class="d-none d-sm-block">Date</span></th>
								<th data-field="finish"><span class="d-none d-sm-block">Money In</span>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1 ; ?>
							<?php foreach($reports as $report) :?>
							<tr class="" data-id="16">
								<td style="min-width:30px;"><?= $i++ ?></td>
								</td>
								<td class="flex"><?php echo $report->date ?></td>
								<td class="flex">Rp <?php echo rupiah($report->grand_tot)?></td>
								<input type="hidden" name="" value="<?= $count += $report->grand_tot ?>" id="">
							</tr>
							<?php endforeach;?>
						</tbody>
						<tfoot>
							<tr>
								<th data-sortable="true" data-field="owner">Total</th>
								<th data-sortable="true" data-field="owner"></th>
								<th data-field="finish"><span class="d-none d-sm-block">Rp.
										<?php echo rupiah($count) ?></span></th>
							</tr>
						</tfoot>
					</table>
					<br><br>
					<div class="row">
						<div class="col-8"></div>
						<div class="col-4 text-center">
							<p>Jakarta, <?php echo tanggal($today) ?></p>
							<br>
							<br>
							<br>
							<p>Fikih Alan</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	window.print();

</script>
