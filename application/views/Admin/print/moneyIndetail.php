<!-- ############ Content START-->
<div id="content" class="flex">
	<!-- ############ Main START-->
	<div>

		<!-- <div class="page-hero page-container" id="page-hero">
			<div class="padding d-flex">

			</div>
		</div> -->
		<div class="col-md-12">
			<div class="card">
           <div class="card-header">
					<h2 class="text-md text-highlight" style="text-align: center;">
                    <img src="<?php echo base_url('assets/image/logo/coffee-cup.svg')?>" alt="" height="100">
					LAPORAN UANG MASUK TANGGAL : <?= $date ?></h2><small
						class="text-muted"></small>
				</div>
				<table class="table table-bordered table-hover" id="table-id" style="font-size:13px;">
					<thead>
						<th>Number Order</th>
						<th>Cashier</th>
						<th>Name Menu</th>
						<th>Qty</th>
						<th>Total</th>
						<th>Grand Total</th>
						<th>Money Cash</th>
						<th>Money Change</th>
					</thead>
					<tbody>
						<?php $i = 1; ?>
						<?php foreach ($order as $order) :?>
						<tr>
							<td class="flex"><?php echo $order->numberorder ?></td>
							<td class="flex"><?php echo $order->firstname.' '. $order->lastname ?></td>
							<td class="flex">
							<?php foreach(json_decode($order->id_menu) as $menu) :?>
									<ul><li><?php echo $this->menu->show($menu); ?></li></ul>
									<?php endforeach ;?>
							</td>
							<td class="flex">
							<?php foreach (json_decode($order->qty) as $jumlah) : ?>
									<ul><li><?php echo $jumlah ?></li></ul>
									<?php endforeach ;?>	
							</td>
							<td class="flex">
							<?php foreach (json_decode($order->total) as $total) :?>
										<ul>
										<li>Rp <?php echo rupiah($total) ?></li>
										</ul>
									<?php endforeach ;?>
							</td>
							<td class="flex">Rp <?php echo rupiah($order->grand_total) ?></td>
							<td class="flex">Rp <?php echo rupiah($order->cash) ?></td>
							<td class="flex">Rp <?php echo str_replace('-', ' ', rupiah($order->change)) ?></td>
						</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
	window.print();

</script>
