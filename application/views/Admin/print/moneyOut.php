<style type="text/css">
	.kotak {
		padding: 5px;
	}

	@page {
		size: A4;
		margin: 0;
	}

	@media print {
		body * {
			visibility: hidden;
		}

		.kotak,
		.kotak * {
			visibility: visible;
		}

		.kotak {
			z-index: 2;
			position: absolute;
			width: 100%;
			top: 0;
			left: 0;
		}
	}

</style>

<!-- ############ Content START-->
<div id="content" class="flex">
	<!-- ############ Main START-->
	<div>

		<div class="container">
			<div class="card mb-12">
				<div class="col-md">
					<div class="col-md">
						<div class="card-body">
							<div id="laporan" class="kotak">
								<div class="card-header" style="text-align: center;">
									<h2 style="font-family: Montserrat;">
									<img src="<?php echo base_url('assets/image/logo/coffee-cup.svg')?>" alt=""
										height="50">
									LAPORAN GAJI KARYAWAN ATAU LAPORAN UANG KELUAR</h2>
									<p style="font-family: Montserrat;">Laporan Bulan : <?= $month ?></p>
								</div>
								<table class="table table-theme v-middle m-0">
									<thead>
										<tr>
											<th data-sortable="true" data-field="id">NO</th>
											<th data-sortable="true" data-field="owner">Full name</th>
											<th data-field="task"><span class="d-none d-sm-block">Phone</span></th>
											<th data-field="finish"><span class="d-none d-sm-block">Email</span></th>
											<th data-field="finish"><span class="d-none d-sm-block"></span></th>
											<th data-field="finish"><span class="d-none d-sm-block">Salarys</span></th>
										</tr>
									</thead>
									<tbody>
										<?php if($reports == NULL):?>
										<div class="alert alert-info alert-dismissible fade show" role="alert">
											<strong>Report</strong> this month null!.
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<?php else:?>
										<?php $i = 1 ; ?>
										<?php foreach($reports as $report) :?>
										<tr class="" data-id="16">
											<td style="min-width:30px;"><?= $i++ ?></td>
											<td class="flex"><?php echo $report->firstname. ' '. $report->lastname ?>
											</td>
											<td class="flex"><?php echo $report->phone ?>
											</td>
											<td><span
													class="item-amount d-none d-sm-block text-sm"><?php echo $report->email ?></span>
											</td>
											<td class="flex"></td>
											<td class="flex">Rp <?php echo rupiah($report->total_salary)?></td>
										</tr>
										<?php endforeach;?>
										<?php endif;?>
									</tbody>
									<tfoot>
										<?php foreach($count as $con) :?>
										<tr>
											<th data-sortable="true" data-field="id"></th>
											<th data-sortable="true" data-field="owner">Total</th>
											<th data-field="task"><span class="d-none d-sm-block"></span></th>
											<th data-field="finish"><span class="d-none d-sm-block"></span></th>
											<th data-field="finish"><span class="d-none d-sm-block"></span></th>
											<th data-field="finish"><span class="d-none d-sm-block">Rp.
													<?php echo rupiah($con->total) ?> </span></th>
										</tr>
										<?php endforeach;?>
									</tfoot>
								</table>
								<br><br>
								<div class="row">
									<div class="col-8"></div>
									<div class="col-4 text-center">
										<p>Jakarta, <?php echo tanggal($today) ?></p>
										<br>
										<br>
										<br>
										<p>Fikih Alan</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


	</div>
</div>

<script>
	window.print()

</script>
