    <!-- ############ Content START-->
    <div id="content" class="flex">
    	<!-- ############ Main START-->
    	<div>
    		<div class="page-hero page-container" id="page-hero">
    			<div class="padding d-flex">
    				<div class="page-title">
    					<h2 class="text-md text-highlight">Coffe shop</h2><small class="text-muted">Data
    						Comments</small>
    				</div>
    			</div>
    		</div>
    		<div class="row row-sm">
    			<div class="col-md-12">
    				<div class="card">
    					<div class="p-3-4">
    						<div class="d-flex">
    							<div>
    								<div>Comments</div><small class="text-muted"></small>
    							</div><span class="flex"></span>
    						</div>
    					</div>
    					<table class="table table-theme v-middle m-0">
    						<thead>
    							<tr>
    								<th>no</th>
    								<th>name</th>
    								<th>email</th>
    								<th>subject</th>
    								<th>comment</th>
    								<th>action</th>
    							</tr>
    						</thead>
    						<tbody>
    							<?php if($comments == NULL) :?>
									<div class="alert alert-info alert-dismissible fade show" role="alert">
                                            <strong>Data</strong> komentar saat ini masih kosong!.
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
    							<?php else :?>
    							<?php $i = 1; ?>
    							<?php foreach($comments as $coment) : ?>
    							<tr id="<?php echo $coment->id ?>">
    								<td style="min-width:30px;text-align:center"><?= $i ?></td>

    								<td class="flex"><a href="" data-toggle="modal"
    										data-target="#detail<?php echo $coment->id ?>">
    										<?php echo $coment->name?></a></td>

    								<td class="flex"><?php echo $coment->email ?></td>
    								<td class="flex"><?php echo $coment->subject ?></td>
    								<td class="flex"><?php echo substr($coment->comment,0,50) ?>...</td>
									<td class="flex"><a href="" data-toggle="modal" 
									data-target="#detail<?php echo $coment->id ?>" class="btn btn-rounded btn-info"><i data-feather="search"></i></a></td>
    							</tr>
    							<?php $i++ ; ?>
    							<?php endforeach; ?>
    							<?php endif ;?>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>

    <?php $no = 0;
        foreach ($comments as $coment) : $no++; ?>
    <div class="modal fade" id="detail<?php echo $coment->id?>" tabindex="-1" role="dialog"
    	aria-labelledby="exampleModalLabel" aria-hidden="true">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<h5 class="modal-title" id="exampleModalLabel">Details Comments <?php echo $coment->name ?></h5>
    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    					<span aria-hidden="true">&times;</span>
    				</button>
    			</div>
    			<div class="modal-body">

    				<div class="form-group">
    					<label>Name</label>
    					<input class="form-control" type="text" readonly value="<?php echo $coment->name ?>">
    				</div>

    				<div class="form-group">
    					<label>Email</label>
    					<input class="form-control" type="email" readonly value="<?php echo $coment->email ?>">
    				</div>

    				<div class="form-group">
    					<label>Subjects</label>
    					<input class="form-control" type="text" readonly value="<?php echo $coment->subject ?>">
    				</div>

    				<div class="form-group">
    					<label>Comments</label>
    					<textarea readonly class="form-control" id="" cols="30"
    						rows="10"><?php echo $coment->comment ?></textarea>
    				</div>
    			</div>

    			<div class="modal-footer">
    				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    			</div>
    		</div>
    	</div>
    </div>
    <?php endforeach;?>
