    <!-- ############ Content START-->
    <div id="content" class="flex">
    	<!-- ############ Main START-->
    	<div>
    		<div class="page-hero page-container" id="page-hero">
    			<div class="padding d-flex">
    				<div class="page-title">
    					<h2 class="text-md text-highlight">Slide </h2><small class="text-muted"></small>
    				</div>
    			</div>
    		</div>
    		<div class="col-md-12">
    			<div class="card">
    				<div class="p-3-4">
    					<div class="d-flex">
    						<div>
    							<div>Slides</div><small class="text-muted"></small>
    						</div><span class="flex"></span>
    						<div><a href="#" class="btn btn-sm btn-white" data-toggle="modal" data-target="#addModal"><i data-feather="plus"></i></a></div>
    					</div>
    				</div>
    				<table class="table table-theme v-middle m-0">
    					<tbody>
    						<?php $i = 1; ?>
    						<?php foreach ($slide as $slid) :?>
    							<tr id="<?php echo $slid->id ?>">
    								<td style="min-width:30px;text-align:center"><?= $i++ ?></td>
    								<td>
    									<div class="avatar-group"><a href="#" class="avatar ajax w-32" data-toggle="tooltip"
    										title="Urna"><img src="<?php echo base_url() . 'assets/image/slide/' . $slid->image ?>" alt="."> </a></div>
    									</td>
    									<td class="flex"><a class="item-company ajax h-1x"><?php echo $slid->name ?></a>
    									</td>
    									<td><span class="item-amount d-none d-sm-block text-sm"><?php echo $slid->created_at ?></span></td>
    									<td>
    										<div class="item-action dropdown"><a href="#" data-toggle="dropdown" class="text-muted"><i
    											data-feather="more-vertical"></i></a>
    											<div class="dropdown-menu dropdown-menu-right bg-black" role="menu"><a data-toggle="modal" data-target="#editmodal<?php echo $slid->id ?>"
                           class="dropdown-item edit">Edit</a>
                           <div class="dropdown-divider"></div><a href="#!" class="dropdown-item remove">Delete item</a>
                         </div>
                       </div>
                     </td>
                   </tr>
                 <?php endforeach;?>
               </tbody>
             </table>
           </div>
         </div>
       </div>
     </div>
   </div>


   <!-- Add Modal -->
   <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
   aria-hidden="true">
   <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Add Slides</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
       <span aria-hidden="true">&times;</span>
     </button>
   </div>
   <div class="modal-body">
    <form action="<?php echo base_url(). 'Admin/SlidesController/store'; ?>" enctype="multipart/form-data" method="post">
     <div class="form-group col-md-12">
      <label for="inputPassword4">Content</label>
      <input type="text" class="form-control" name="name" id="inputStartTime4" value="">
    </div>
    <div class="form-group col-md-12">
      <label for="inputPassword4">Images</label>
      <input type="file" class="form-control" name="image" id="inputStartTime4">
    </div>
  </div>
  <div class="modal-footer">
   <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
   <button type="submit" class="btn btn-primary">Save</button>
 </div>
</div>
</form>
</div>
</div>
</div>

<!-- End Add Modal -->

<!-- Edit Modal-->

<?php $no = 0;
foreach ($slide as $slid) : $no++; ?>
	<div class="modal fade" id="editmodal<?php echo $slid->id?>" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form action="<?php echo base_url(). 'Admin/SlidesController/update'; ?>" enctype="multipart/form-data" method="post">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Edit</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
           <div class="form-group">
             <label>Content</label>
             <input class="form-control" name="id" type="hidden" value="<?php echo $slid->id ?>">
             <input class="form-control" name="name" type="text" value="<?php echo $slid->name ?>">
           </div>
           <div class="form-group">
             <label>Images</label>
             <input class="form-control" name="image" type="file" value="<?php echo $slid->image ?>">
           </div>
         </div>

         <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" value="upload" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>


<?php endforeach;?>

<!-- End Edit Modal-->

<!-- Sweet Alert Delete -->
<script>
  $(".remove").click(function(){
    var id = $(this).parents("tr").attr("id");

    swal({
      title: "Apa kamu yakin?",
      text: "Data yang dihapus tidak dapat dikembalikan!",
      type: "warning",
      showConfirmButton: true,
      confirmButtonClass: "btn-danger btn-sm",
      showCancelButton: true,
      cancelButtonClass: "btn-primary btn-sm",
      buttons: true,
      closeOnConfirm: false,
      closeOnCancel: false,
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          url: "<?php echo site_url('Admin/SlidesController/delete')?>/"+id,
         type: 'DELETE',
         error: function() {
          alert('Something is wrong');
        },
        success: function(data) {
          $("#"+id).remove();
          swal("Dihapus!", "Data berhasil dihapus.", "success");
        }
      });
      } else {
        swal({
          type: "error",
          icon: "error",
          title: "Gagal!",
          text: " Data gagal dihapus! ",
          timer: 2500,
          showConfirmButton: false,
          showCancelButton: false,
          buttons: false,
        });
      }
    });

  });
</script>
<!-- Sweet Alert Delete -->
