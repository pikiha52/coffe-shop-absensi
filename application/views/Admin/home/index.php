   <!-- ############ Content START-->
   <div id="content" class="flex">
   	<!-- ############ Main START-->
   	<div>
   		<div class="page-hero page-container" id="page-hero">
   			<div class="padding">
   				<div class="page-title">
   					<?php foreach ($profile as $prof): ?>
   					<h2 class="text-md text-highlight">Coffe shop</h2><small class="text-muted">Welcome to
   						<?php echo $prof->firstname ?></small>
   					<?php endforeach ;?>
   				</div>
   			</div>
   		</div>
   		<?php if($this->session->userdata('level')==='1'):?>
   		<div class="page-content page-container" id="page-content">
   			<div class="padding">
   				<div class="row row-sm">
   					<div class="col-12">
   						<div class="card">
   							<div class="card-body">
   								<div class="row row-sm">
   									<div class="col-4"><small class="text-muted">Your position</small>
   										<div class="mt-2 font-weight-500"><span class="text-info">Admin</span></div>
   									</div>
   									<div class="col-4"><small class="text-muted">Absent status</small>
   										<div class="text-highlight mt-2 font-weight-500"><?= $status; ?></div>
   									</div>
   								</div>
   							</div>
   						</div>
   					</div>

   					<div class="col-4">
   						<div class="card">
   							<div class="card-header">
   								<small class="text-muted">Employee Absent Today</small>
   							</div>
   							<div class="card-body">
   								<div style="text-align: center;">
   									<a href="<?php echo base_url('Admin/AbsensiController/listToday') ?>"><small
   											class="text-muted"><?= $emp_absen ?></small></a>
   								</div>
   							</div>
   						</div>
   					</div>

   					<div class="col-4">
   						<div class="card">
   							<div class="card-header">
   								<small class="text-muted">Total Employee Active</small>
   							</div>
   							<div class="card-body">
   								<div style="text-align: center;">
   									<small class="text-muted"><?= $active_emp ?></small>
   								</div>
   							</div>
   						</div>
   					</div>

   					<div class="col-4">
   						<div class="card">
   							<div class="card-header">
   								<small class="text-muted">Total Money in to Month</small>
   							</div>
   							<div class="card-body">
   								<?php foreach ($moneyIn as $money) :?>
   								<small class="text-muted">Rp <?php echo rupiah($money->grand_tot) ?></small>
   								<?php endforeach ;?>
   							</div>
   						</div>
   					</div>

   					<!-- <div class="col-md-12">
   						<div class="card">
   							<div class="card-header">
   								<small class="text-muted">MONEY OUT</small>
   							</div>
   							<div class="card-body">
   								<div id="chart"></div>
   							</div>
   						</div>
   					</div> -->

   					<div class="col-md-12">
   						<div class="card">
   							<div class="p-3-4">
   								<div class="d-flex">
   									<div>
   										<div>
   											List of job applications</div><small class="text-muted"></small>
   									</div><span class="flex"></span>
   								</div>
   							</div>
   							<div class="table-responsive">
   								<table class="table table-theme v-middle m-0">
   									<thead>
   										<tr>
   											<th data-sortable="true" data-field="id">NO</th>
   											<th data-field="finish"><span class="d-none d-sm-block">Position
   													applied</span></th>
   											<th data-sortable="true" data-field="owner">Full name</th>
   											<th data-field="task"><span class="d-none d-sm-block">Phone</span></th>
   											<th data-field="finish"><span class="d-none d-sm-block">Email</span></th>
   											<th data-field="finish"><span class="d-none d-sm-block">Actions</span>
   											</th>
   											<th></th>
   										</tr>
   									</thead>
   									<tbody>
   										<?php $i = 1; ?>
   										<?php foreach($apply as $app): ?>
   										<tr class="" data-id="16">
   											<td style="min-width:30px;text-align:center"><?= $i ?></td>
   											<td class="flex"><a
   													href="<?php echo base_url('Admin/CareersAdminController/showApply/'.$app->careers_id)?>"
   													class="item-company ajax h-1x"><?php echo $app->name?></a>
   											</td>
   											<td class="flex"><a 
   													class="item-company ajax h-1x"><?php echo $app->firstname ?>
   													<?php echo $app->lastname ?></a>
   											</td>
   											<td><span
   													class="item-amount d-none d-sm-block text-sm"><?php echo $app->phone ?></span>
   											</td>
   											<td class="flex"><?php echo $app->email ?>
   											</td>
   											<td>
   												<a href="<?php echo base_url('Admin/CareersAdminController/detailapply/'. $app->id)?>"
   													type="button" class="btn btn-info btn-sm"><i
   														data-feather="eye"></i></a>
   												<a href="<?php echo base_url('Admin/CareersAdminController/download/'. $app->resume)?>"
   													type="button" class="btn btn-info btn-sm"><i
   														data-feather="save"></i>
   											</td>
   										</tr>
   										<?php endforeach ;?>
   									</tbody>
   								</table>
   							</div>
   						</div>
   					</div>
   					<div class="col-md-6">
   						<div class="card">
   							<div class="p-3-4">
   								<div class="d-flex">
   									<div>
   										<div>Innactive Employee</div>
   									</div><span class="flex"></span>
   								</div>
   							</div>
   							<?php if ($users == NULL) :?>

   							<div class="alert alert-info alert-dismissible fade show" role="alert">
   								<strong>Employee</strong> innactive is null!.
   								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
   									<span aria-hidden="true">&times;</span>
   								</button>
   							</div>


   							<?php else:?>
   							<table class="table table-theme v-middle m-0">
   								<tbody>
   									<?php $i = 1; ?>
   									<?php foreach($users as $user) :?>
   									<tr class="" data-id="16">
   										<td style="min-width:30px;text-align:center"><?= $i ?></td>
   										<td>
   											<div class="avatar-group"><a
   													href="<?php echo base_url('Admin/UsersController/')?>"
   													class="avatar ajax w-32" data-toggle="tooltip" title="Urna"><img
   														src="<?php echo base_url('assets/image/profile/') . $user->photo ?>"
   														alt="."> </a></div>
   										</td>
   										<td class="flex"><a href="<?php echo base_url('Admin/UsersController/')?>"
   												class="item-company ajax h-1x"><?php echo $user->firstname.' '.$user->lastname ?></a>
   											<div class="item-mail text-muted h-1x d-none d-sm-block">
   												<?php echo $user->name ?></div>
   										</td>
   										<td><span class="item-amount d-none d-sm-block text-sm"><a
   													href="<?php echo base_url('Admin/UsersController/updateStatus/'. $user->id_user)?>"
   													class="btn btn-info badge">Non active</a></span></td>
   									</tr>
   									<?php endforeach ;?>
   								</tbody>
   								<?php endif;?>
   							</table>
   						</div>
   					</div>
   				</div>
   			</div>
   		</div>
   		<?php elseif($this->session->userdata('level')==='2'):?>
   		<div class="page-content page-container" id="page-content">
   			<div class="padding">
   				<div class="row row-sm">
   					<div class="col-12">
   						<div class="card">
   							<div class="card-body">
   								<div class="row row-sm">
   									<div class="col-4"><small class="text-muted">Your position</small>
   										<?php foreach ($profile as $prof) :?>
   										<div class="mt-2 font-weight-500"><span
   												class="text-info"><?php echo $prof->name ?></span></div>
   										<?php endforeach ;?>
   									</div>
   									<div class="col-4"><small class="text-muted">Absent status</small>
   										<div class="text-highlight mt-2 font-weight-500"><?= $status; ?></div>
   									</div>
   									<div class="col-4"><small class="text-muted">Click for Absent</small>
   										<div class="text-highlight mt-2 font-weight-500">
   											<button type="button" class="btn btn-rounded btn-info"
   												onclick="showPosition();">Absen</button>
   										</div>
   									</div>
   								</div>
   							</div>
   						</div>
   					</div>
   					<div class="col-md-12">
   						<div class="card">
   							<div class="p-3-4">
   								<div class="d-flex">
   									<div>
   										<div>
   											List of job applications</div><small class="text-muted"></small>
   									</div><span class="flex"></span>
   								</div>
   							</div>
   							<table class="table table-theme v-middle m-0">
   								<thead>
   									<tr>
   										<th data-sortable="true" data-field="id">NO</th>
   										<th data-field="finish"><span class="d-none d-sm-block">Position
   												applied</span></th>
   										<th data-sortable="true" data-field="owner">Full name</th>
   										<th data-field="task"><span class="d-none d-sm-block">Phone</span></th>
   										<th data-field="finish"><span class="d-none d-sm-block">Email</span></th>
   										<th data-field="finish"><span class="d-none d-sm-block">Actions</span></th>
   										<th></th>
   									</tr>
   								</thead>
   								<tbody>
   									<?php $i = 1; ?>
   									<?php foreach($apply as $app): ?>
   									<tr class="" data-id="16">
   										<td style="min-width:30px;text-align:center"><?= $i ?></td>
   										<td class="flex"><a
   												href="<?php echo base_url('Admin/CareersAdminController/showApply/'.$app->careers_id)?>"
   												class="item-company ajax h-1x"><?php echo $app->name?></a>
   										</td>
   										<td class="flex"><a href="page.invoice.detail.html"
   												class="item-company ajax h-1x"><?php echo $app->firstname ?>
   												<?php echo $app->lastname ?></a>
   										</td>
   										<td><span
   												class="item-amount d-none d-sm-block text-sm"><?php echo $app->phone ?></span>
   										</td>
   										<td class="flex"><?php echo $app->email ?>
   										</td>
   										<td>
   											<a href="<?php echo base_url('Admin/CareersAdminController/detailapply/'. $app->id)?>"
   												type="button" class="btn btn-info btn-sm"><i
   													data-feather="eye"></i></a>
   											<a href="<?php echo base_url('Admin/CareersAdminController/download/'. $app->resume)?>"
   												type="button" class="btn btn-info btn-sm"><i data-feather="save"></i>
   										</td>
   									</tr>
   									<?php endforeach ;?>
   								</tbody>
   							</table>
   						</div>
   					</div>
   					<div class="col-md-6">
   						<div class="card">
   							<div class="p-3-4">
   								<div class="d-flex">
   									<div>
   										<div>Innactive Employee</div>
   									</div><span class="flex"></span>
   								</div>
   							</div>
   							<?php if ($users == NULL) :?>

   							<div class="alert alert-info alert-dismissible fade show" role="alert">
   								<strong>Employee</strong> innactive is null!.
   								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
   									<span aria-hidden="true">&times;</span>
   								</button>
   							</div>


   							<?php else:?>
   							<table class="table table-theme v-middle m-0">
   								<tbody>
   									<?php $i = 1; ?>
   									<?php foreach($users as $user) :?>
   									<tr class="" data-id="16">
   										<td style="min-width:30px;text-align:center"><?= $i ?></td>
   										<td>
   											<div class="avatar-group"><a
   													href="<?php echo base_url('Admin/UsersController/')?>"
   													class="avatar ajax w-32" data-toggle="tooltip" title="Urna"><img
   														src="<?php echo base_url('assets/image/profile/') . $user->photo ?>"
   														alt="."> </a></div>
   										</td>
   										<td class="flex"><a href="<?php echo base_url('Admin/UsersController/')?>"
   												class="item-company ajax h-1x"><?php echo $user->firstname.' '.$user->lastname ?></a>
   											<div class="item-mail text-muted h-1x d-none d-sm-block">
   												<?php echo $user->name ?></div>
   										</td>
   										<td><span class="item-amount d-none d-sm-block text-sm"><a
   													href="<?php echo base_url('Admin/UsersController/updateStatus/'. $user->id_user)?>"
   													class="btn btn-info badge">Non active</a></span></td>
   									</tr>
   									<?php endforeach ;?>
   								</tbody>
   								<?php endif;?>
   							</table>
   						</div>
   					</div>

   					<div class="card col-12">
   						<div class="card-body">
   							<center>
   								<h2 class="text-md text-highlight">Rekap absen bulan ini</h2>

   							</center>
   							<div class="page-content page-container" id="page-content">
   								<div class="padding">
   									<div class="table-responsive">
   										<table id="datatable" class="table table-theme table-row v-middle"
   											data-plugin="dataTable">
   											<thead>
   												<tr>
   													<th><span class="text-muted">No</span></th>
   													<th><span class="text-muted">Profile</span></th>
   													<th><span class="text-muted">Name</span></th>
   													<th><span class="text-muted d-none d-sm-block">Absent
   															entry</span></th>
   													<th><span class="text-muted d-none d-sm-block">Absent out</span>
   													</th>
   													<th></th>
   												</tr>
   											</thead>
   											<tbody>
   												<?php if($rekap == NULL) :?>

   												<tr>
   													<div class="alert alert-info alert-dismissible fade show"
   														role="alert">
   														<strong>Absent</strong> is null!.
   														<button type="button" class="close" data-dismiss="alert"
   															aria-label="Close">
   															<span aria-hidden="true">&times;</span>
   														</button>
   													</div>
   												</tr>

   												<?php else :?>

   												<?php foreach($rekap as $rek): ?>
   												<tr class="" data-id="17">
   													<td style="min-width:30px;text-align:center"><small
   															class="text-muted"><?= ++$start; ?></small></td>
   													<td>
   														<div class="avatar-group"><a
   																href="<?php echo site_url('Admin/groomer/detail/').$rek->id_user ?>"
   																class="avatar ajax w-32"><img
   																	src="<?php echo base_url('assets/image/profile/') . $rek->photo ?>"
   																	alt="."> </a></div>
   													</td>
   													<td class="flex"><a href="#"
   															class="item-title text-color"><?php echo $rek->firstname ?>
   															<?php echo $rek->lastname ?></a>
   													</td>
   													<td><span
   															class="item-amount d-none d-sm-block text-sm"><?php echo $rek->check_in ?></span>
   													</td>
   													<td><span
   															class="item-amount d-none d-sm-block text-sm [object Object]"><?php echo $rek->check_out ?></span>
   													</td>
   												</tr>
   												<?php endforeach ;?>

   												<?php endif ;?>

   											</tbody>
   										</table>
   									</div>
   								</div>
   							</div>
   							<?= $this->pagination->create_links(); ?>
   						</div>

   					</div>
   				</div>

   			</div>
   		</div>
   		<?php elseif($this->session->userdata('level')==='3'):?>
   		<div class="page-content page-container" id="page-content">
   			<div class="padding">
   				<div class="row row-sm">
   					<div class="col-12">
   						<div class="card">
   							<div class="card-body">
   								<div class="row row-sm">
   									<div class="col-4"><small class="text-muted">Your position</small>
   										<?php foreach ($profile as $prof) :?>
   										<?php if ($kasir ==  3) :?>

   										<div class="mt-2 font-weight-500"><span
   												class="text-info"><?php echo $prof->name ?></span></div>
   									</div>
   									<div class="col-4"><small class="text-muted">Absent status</small>
   										<div class="text-highlight mt-2 font-weight-500"><?= $status; ?></div>
   									</div>
   									<?php if($access == NULL) :?>
										<div class="col-4"><small class="text-muted">Cashier App</small>
   										<div class="text-highlight mt-2 font-weight-500"> <button
   												type="button" class="btn btn-rounded btn-warning" onclick="showPositionAccess();"><i data-feather="x-circle"> </i> Cashier</button><br>
											<small class="badge" style="text-align: center;">lakukan absen terlebih dahulu untuk melakukan transaksi !</small>		   
										</div>
   									</div>
   									<?php else :?>
										<div class="col-4"><small class="text-muted">Cashier App</small>
   										<div class="text-highlight mt-2 font-weight-500"> <button
   												type="button" onclick="showPositionAccess();" class="btn btn-rounded btn-success">Cashier</button></div>
   									</div>
   									<?php endif ;?>
									   <div id="getLocations" hidden="true">
   										<!--Position information will be inserted here-->
   										</div>

   									<?php elseif ($kasir == 4) :?>

   									<div class="mt-2 font-weight-500"><span
   											class="text-info"><?php echo $prof->name ?></span>
   									</div>
   								</div>
   								<div class="col-4"><small class="text-muted">Absent status</small>
   									<div class="text-highlight mt-2 font-weight-500"><?= $status; ?></div>
   								</div>
								   <?php if($access == NULL) :?>
   									<div class="col-4"><small class="text-muted">Cashier App</small>
   										<div class="text-highlight mt-2 font-weight-500"> <button
   												type="button" class="btn btn-rounded btn-warning" onclick="showPositionAccess();"><i data-feather="x-circle"> </i> Cashier</button><br>
											<small class="badge" style="text-align: center;">lakukan absen terlebih dahulu untuk melakukan transaksi !</small>		   
										</div>
   									</div>
   									<?php else :?>
   									<div class="col-4"><small class="text-muted">Cashier App</small>
   										<div class="text-highlight mt-2 font-weight-500"> <button
   												type="button" onclick="showPositionAccess();" class="btn btn-rounded btn-success">Cashier</button></div>
   									</div>
   									<?php endif ;?>

									   <div id="getLocations" hidden="true">
   										<!--Position information will be inserted here-->
   										</div>

   								<?php else :?>

   								<div class="mt-2 font-weight-500"><span
   										class="text-info"><?php echo $prof->name ?></span></div>
   							</div>
   							<div class="col-4"><small class="text-muted">Absent status</small>
   								<div class="text-highlight mt-2 font-weight-500"><?= $status; ?></div>
   							</div>

   							<?php endif;?>
   							<?php endforeach ;?>
   						</div>
   					</div>
   				</div>
   				<div class="card">
   					<div class="card-body">
   						<center>
   							<p style="color: black">Click disini untuk absen</p>

   							<div id="result" hidden="true">
   								<!--Position information will be inserted here-->
   							</div>
   							<button type="button" class="btn btn-rounded btn-info"
   								onclick="showPosition();">Absen</button>

   						</center>
   					</div>
   				</div>
   			</div>
   			<div class="card col-12">
   				<div class="card-body">
   					<center>
   						<h2 class="text-md text-highlight">Rekap absen bulan ini</h2>
   					</center>
   					<div class="page-content page-container" id="page-content">
   						<div class="padding">
   							<div class="table-responsive">
   								<table id="datatable" class="table table-theme table-row v-middle"
   									data-plugin="dataTable">
   									<thead>
   										<tr>
   											<th><span class="text-muted">No</span></th>
   											<th><span class="text-muted">Profile</span></th>
   											<th><span class="text-muted">Name</span></th>
   											<th><span class="text-muted d-none d-sm-block">Absent entry</span></th>
   											<th><span class="text-muted d-none d-sm-block">Absent out</span></th>
   											<th></th>
   										</tr>
   									</thead>
   									<tbody>
   										<?php if($rekap == NULL) :?>

   										<tr>
   											<div class="alert alert-info alert-dismissible fade show" role="alert">
   												<strong>Absent</strong> is null!.
   												<button type="button" class="close" data-dismiss="alert"
   													aria-label="Close">
   													<span aria-hidden="true">&times;</span>
   												</button>
   											</div>
   										</tr>

   										<?php else :?>
   										<?php foreach($rekap as $rek): ?>
   										<tr class="" data-id="17">
   											<td style="min-width:30px;text-align:center"><small
   													class="text-muted"><?= ++$start; ?></small></td>
   											<td>
   												<div class="avatar-group"><a
   														href="<?php echo site_url('Admin/groomer/detail/').$rek->id_user ?>"
   														class="avatar ajax w-32"><img
   															src="<?php echo base_url('assets/image/profile/') . $rek->photo ?>"
   															alt="."> </a></div>
   											</td>
   											<td class="flex"><a href="#"
   													class="item-title text-color"><?php echo $rek->firstname ?>
   													<?php echo $rek->lastname ?></a>
   											</td>
   											<td><span
   													class="item-amount d-none d-sm-block text-sm"><?php echo $rek->check_in ?></span>
   											</td>
   											<td><span
   													class="item-amount d-none d-sm-block text-sm [object Object]"><?php echo $rek->check_out ?></span>
   											</td>
   										</tr>
   										<?php endforeach ;?>

   										<?php endif ;?>
   									</tbody>
   								</table>
   							</div>
   						</div>
   					</div>
   					<?= $this->pagination->create_links(); ?>
   				</div>
   			</div>
   		</div>
   	</div>
   </div>
   </div>
   <?php else:?>
   <?php endif;?>
   </div>
   </div><!-- ############ Main END-->

   <script type="text/javascript"> 
   	var result;

   	function showPosition() {
   		// Store the element where the page displays the result
   		result = document.getElementById("result");
		   console.log(result);

   		// If geolocation is available, try to get the visitor's position
   		if (navigator.geolocation) {
   			navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
   			result.innerHTML = "Getting the position information...";
   		} else {
   			alert("Sorry, your browser does not support HTML5 geolocation.");
   		}
   	};

   	// Define callback function for successful attempt
   	function successCallback(position) {
   		result.innerHTML = "Your current position is (" + "Latitude: " + position.coords.latitude + ", " +
   			"Longitude: " + position.coords.longitude + ")";

   		var lat = position.coords.latitude;
   		var lng = position.coords.longitude;

   		var data = {
   			'lat': lat,
   			'lng': lng
   		};
   		// console.log(response);
   		window.location.href = "welcome/absen/" + data['lat'] + '/' + data['lng'];
   	}

   	// Define callback function for failed attempt
   	function errorCallback(error) {
   		if (error.code == 1) {
   			result.innerHTML = "You've decided not to share your position, but it's OK. We won't ask you again.";
   		} else if (error.code == 2) {
   			result.innerHTML = "The network is down or the positioning service can't be reached.";
   		} else if (error.code == 3) {
   			result.innerHTML = "The attempt timed out before it could get the location data.";
   		} else {
   			result.innerHTML = "Geolocation failed due to unknown error.";
   		}
   	}

   </script>


<script type="text/javascript"> 
   	var cashier;

   	function showPositionAccess() {
   		// Store the element where the page displays the result
   		cashier = document.getElementById("getLocations");
		   console.log(cashier);

   		// If geolocation is available, try to get the visitor's position
   		if (navigator.geolocation) {
   			navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
   			cashier.innerHTML = "Getting the position information...";
   		} else {
   			alert("Sorry, your browser does not support HTML5 geolocation.");
   		}
   	};

   	// Define callback function for successful attempt
   	function successCallback(position) {
		cashier.innerHTML = "Your current position is (" + "Latitude: " + position.coords.latitude + ", " +
   			"Longitude: " + position.coords.longitude + ")";

   		var lat = position.coords.latitude;
   		var lng = position.coords.longitude;

   		var data = {
   			'lat': lat,
   			'lng': lng
   		};
   		// console.log(response);
   		window.location.href = "kasircontroller/kasirApps/" + data['lat'] + '/' + data['lng'];
   	}

   	// Define callback function for failed attempt
   	function errorCallback(error) {
   		if (error.code == 1) {
			cashier.innerHTML = "You've decided not to share your position, but it's OK. We won't ask you again.";
   		} else if (error.code == 2) {
			cashier.innerHTML = "The network is down or the positioning service can't be reached.";
   		} else if (error.code == 3) {
			cashier.innerHTML = "The attempt timed out before it could get the location data.";
   		} else {
			cashier.innerHTML = "Geolocation failed due to unknown error.";
   		}
   	}

   </script>




            <script type="text/javascript">
                $(".alert").alert('close')
            </script>

