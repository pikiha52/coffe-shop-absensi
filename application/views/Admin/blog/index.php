    <!-- ############ Content START-->
    <div id="content" class="flex"> 
    	<!-- ############ Main START-->
    	<div>
    		<div class="page-hero page-container" id="page-hero">
    			<div class="padding d-flex">
    				<div class="page-title">
    					<h2 class="text-md text-highlight">Blogs </h2><small class="text-muted"></small>
    				</div>
    			</div>
    		</div>
    		<div class="col-md-12">
    			<div class="card">
    				<div class="p-3-4">
    					<div class="d-flex">
    						<div>
    							<div>Top blogs</div><small class="text-muted">Total: <?= $count ?></small>
    						</div><span class="flex"></span>
    						<div><a href="#" class="btn btn-sm btn-white" data-toggle="modal" data-target="#addModal"><i data-feather="plus"></i></a></div>
    					</div>
    				</div>
    				<table class="table table-theme v-middle m-0">
    					<tbody>
    						<?php $i = 1; ?>
    						<?php foreach ($blog as $blg) :?>
    							<tr id="<?php echo $blg->id ?>">
    								<td style="min-width:30px;text-align:center"><?= $i++ ?></td>
    								<td>
    									<div class="avatar-group"><a href="#" class="avatar ajax w-32" data-toggle="tooltip"
    										title="Urna"><img src="<?php echo base_url() . 'assets/image/blogs/' . $blg->image ?>" alt="."> </a></div>
    									</td>
    									<td class="flex"><a href="#" class="item-company ajax h-1x"><?php echo $blg->title ?></a>
    									</td>
    									<td><span class="item-amount d-none d-sm-block text-sm"><?php echo $blg->date ?></span></td>
    									<td>
    										<div class="item-action dropdown"><a href="#" data-toggle="dropdown" class="text-muted"><i
    											data-feather="more-vertical"></i></a>
    											<div class="dropdown-menu dropdown-menu-right bg-black" role="menu"><a data-toggle="modal" data-target="#editmodal<?php echo $blg->id ?>"
                           class="dropdown-item edit">Edit</a>
                           <div class="dropdown-divider"></div><a class="dropdown-item remove">Delete item</a>
                         </div>
                       </div>
                     </td>
                   </tr>
                 <?php endforeach;?>
               </tbody>
             </table>
           </div>
         </div>
       </div>
     </div>
   </div>


   <!-- Add Modal -->
   <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
   aria-hidden="true">
   <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Add</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
       <span aria-hidden="true">&times;</span>
     </button>
   </div>
   <div class="modal-body">
    <form action="<?php echo base_url(). 'Admin/BlogsController/add'; ?>" enctype="multipart/form-data" method="post">
     <div class="form-group col-md-12">
      <label for="inputPassword4">Title</label>
      <input type="text" class="form-control" name="title" id="inputStartTime4" value="">
    </div> 
    <div class="form-group col-md-12">
      <label for="inputDivisi4">Category</label>
      <select class="form-control mb-2" name="category_id">
        <option class="hidden" selected disabled>Select your Category
        </option>
        <?php foreach($category as $row) : ?>
         <option value="<?php echo $row->id;?>"><?php echo $row->name;?></option>
       <?php endforeach ;?>
     </select>
   </div>
   <div class="form-group col-md-12">
    <label for="inputPassword4">Images</label>
    <input type="file" class="form-control" name="image" id="inputStartTime4" value="">
  </div>
  <div class="form-group col-md-12">
    <label for="inputType9">Blogs</label>
    <textarea class="form-control" id="ckeditor" cols="12" rows="5" name="isi"></textarea>
  </div>
</div>
<div class="modal-footer">
 <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
 <button type="submit" class="btn btn-primary">Save</button>
</div>
</div>
</form> 
</div>
</div>
</div>

<!-- End Add Modal -->

<!-- Edit Modal-->

<?php $no = 0;
foreach ($blog as $blg) : $no++; ?>
	<div class="modal fade" id="editmodal<?php echo $blg->id?>" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form action="<?php echo base_url(). 'Admin/BlogsController/update'; ?>" enctype="multipart/form-data" method="post">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Edit</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
            <div class="form-group">
              <label for="inputDivisi4">Category</label>
              <select class="form-control mb-2" name="category_id" >
                <?php foreach($category as $cat):?>
                  <option value="<?= $cat->id?>"
                    <?php if ($cat->id == $blg->category_id) : ?> selected<?php endif; ?>>
                    <?= $cat->name?>
                  </option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="form-group">
             <label>Title</label>
             <input class="form-control" name="id" type="hidden" value="<?php echo $blg->id ?>">
             <input class="form-control" name="title" type="text" value="<?php echo $blg->title ?>">
           </div>
           <div class="form-group">
             <label>Images</label>
             <input class="form-control" name="images" type="file" value="<?php echo $blg->image ?>">
           </div>
           <div class="form-group">
             <label for="inputType9">Blogs</label>
             <textarea class="form-control" id="ckeditoredit" cols="12" rows="5" id="edit" name="isi"><?php echo $blg->isi?></textarea>
           </div>
         </div>

         <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" value="upload" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>


<?php endforeach;?>
<!-- End Edit Modal-->

<script src="<?php echo base_url('assets/jquery/jquery-3.3.1.js');?>"></script>
<script src="<?php echo base_url('assets/ckeditor/ckeditor.js');?>"></script>
<script type="text/javascript">
  $(function () {
    CKEDITOR.replace('ckeditor',{
      filebrowserImageBrowseUrl : '<?php echo base_url('assets/kcfinder/browse.php');?>',
      height: '400px'             
    });
    CKEDITOR.replace('ckeditoredit',{
      filebrowserImageBrowseUrl : '<?php echo base_url('assets/kcfinder/browse.php');?>',
      height: '400px'             
    });
  });
</script>

<!-- Sweet Alert Delete -->
<script>
$(".remove").click(function(){
    var id = $(this).parents("tr").attr("id");

    swal({
        title: "Apa kamu yakin?",
        text: "Data yang dihapus tidak dapat dikembalikan!",
        type: "warning",
        showConfirmButton: true,
        confirmButtonClass: "btn-danger btn-sm",
        showCancelButton: true,
        cancelButtonClass: "btn-primary btn-sm",
        buttons: true,
        closeOnConfirm: false,
        closeOnCancel: false,
    },
    function(isConfirm) {
        if (isConfirm) {
          $.ajax({
           url: "<?php echo site_url('Admin/BlogsController/delete')?>/"+id,
           type: 'DELETE',
           error: function() {
            alert('Something is wrong');
        },
        success: function(data) {
          $("#"+id).remove();
          swal("Dihapus!", "Data berhasil dihapus.", "success");
      }
  });
      } else {
          swal({
                type: "error",
                icon: "error",
                title: "Gagal!",
                text: " Data gagal dihapus! ",
                timer: 2500,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
      }
  });

});
</script>
<!-- Sweet Alert Delete -->



