    <!-- ############ Content START-->
    <div id="content" class="flex">
    	<!-- ############ Main START-->
    	<div>
    		<div class="page-hero page-container" id="page-hero">
    			<div class="padding d-flex">
    				<div class="page-title">
    					<h2 class="text-md text-highlight">Coffe shop</h2><small class="text-muted">Our menu</small>
    				</div>
    			</div>
    		</div>
    		<div class="row row-sm">
    			<div class="col-md-12">
    				<div class="card">
    					<div class="p-3-4">
    						<div class="d-flex">
    							<div>
    								<div>Menu</div><small class="text-muted"></small>
    							</div><span class="flex"></span>
    							<div><a class="btn btn-sm btn-white" data-toggle="modal" data-target="#addModal"><i
    										data-feather="plus"></i></a></div>
    						</div>
    					</div>
    					<table class="table table-theme v-middle m-0">
    						<tbody>
    							<?php $i = 1; ?>
    							<?php foreach($ourmenu as $menu) : ?>
    							<tr id="<?php echo $menu->id_menu ?>">
    								<td style="min-width:30px;text-align:center"><?= $i ?></td>
    								<td>
    									<div class="avatar-group"><a class="avatar ajax w-32" data-toggle="tooltip"
    											title="Urna"><img
    												src="<?php echo base_url().'assets/image/menu/'. $menu->image ?>"
    												alt="."> </a></div>
    								</td>
    								<td class="flex"><a href="#"
    										class="item-company ajax h-1x"><?php echo $menu->title?></a>
    								</td>
    								<td class="flex"><a class="item-company ajax h-1x">Rp
    										<?php echo rupiah($menu->price)?></a>
    								</td>
    								<td>
    									<div class="item-action dropdown"><a href="#" data-toggle="dropdown"
    											class="text-muted"><i data-feather="more-vertical"></i></a>
    										<div class="dropdown-menu dropdown-menu-right bg-black" role="menu"><a
    												class="dropdown-item edit" data-toggle="modal"
    												data-target="#editmodal<?php echo $menu->id_menu?>">Edit</a>
    											<div class="dropdown-divider"></div><a class="dropdown-item remove"
    												href="#!">Delete item</a>
    										</div>
    									</div>
    								</td>
    							</tr>
    							<?php $i++ ; ?>
    							<?php endforeach; ?>
    						</tbody>
    					</table>
    				</div>
    			</div>
    			<!-- <div class="card col-md-6">
    				<div class="p-3-4">
    					<div class="d-flex">
    						<div>
    							<div>Topping Menu</div>
    						</div><span class="flex"></span>
    						<div><a class="btn btn-sm btn-white" data-toggle="modal" data-target="#addTopping"><i
    									data-feather="plus"></i></a></div>
    					</div>
    				</div>
    				<table class="table table-theme v-middle m-0">
    					<tbody>
    						<?php $i = 1; ?>
    						<tr class="" data-id="16">
    							<td style="min-width:30px;text-align:center"><?= $i ?></td>
    							<td>
    								<div class="avatar-group"><a href="#" class="avatar ajax w-32" data-toggle="tooltip"
    										title="Urna"><img src="../assets/img/a15.jpg" alt="."> </a></div>
    							</td>
    							<td class="flex"><a href="page.invoice.detail.html"
    									class="item-company ajax h-1x">Microsoft</a>
    								<div class="item-mail text-muted h-1x d-none d-sm-block">
    									frances-stewart@microsoft.com</div>
    							</td>
    							<td><span class="item-amount d-none d-sm-block text-sm">200</span></td>
    							<td>
    								<div class="item-action dropdown"><a href="#" data-toggle="dropdown"
    										class="text-muted"><i data-feather="more-vertical"></i></a>
    									<div class="dropdown-menu dropdown-menu-right bg-black" role="menu">
    										<a class="dropdown-item" href="#">See detail </a><a
    											class="dropdown-item download">Download </a><a
    											class="dropdown-item edit">Edit</a>
    										<div class="dropdown-divider"></div><a class="dropdown-item trash ">Delete
    											item</a>
    									</div>
    								</div>
    							</td>
    						</tr>
    					</tbody>
    				</table>
    			</div> -->
    		</div>
    	</div>
    </div>
    </div>

    <!-- Add Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    	aria-hidden="true">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<form action="<?php echo base_url(). 'Admin/MenuController/add'; ?>" enctype="multipart/form-data"
    				method="post">
    				<div class="modal-header">
    					<h5 class="modal-title" id="exampleModalLabel">Add Menu</h5>
    					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    						<span aria-hidden="true">&times;</span>
    					</button>
    				</div>
    				<div class="modal-body">
    					<div class="form-group">
    						<label>Images</label>
    						<input class="form-control" name="image" type="file">
    					</div>
    					<div class="form-group">
    						<label>Name Menu</label>
    						<input class="form-control" name="title" type="text">
    					</div>
    					<div class="form-group">
    						<label>Price Menu</label>
    						<input class="form-control" name="price" type="text">
    					</div>
    				</div>

    				<div class="modal-footer">
    					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    					<button type="submit" value="upload" class="btn btn-primary">Save</button>
    				</div>
    			</form>
    		</div>
    	</div>
    </div>

    <!-- Edit Modal-->

    <?php $no = 0;
        foreach ($ourmenu as $menu) : $no++; ?>
    <div class="modal fade" id="editmodal<?php echo $menu->id_menu?>" tabindex="-1" role="dialog"
    	aria-labelledby="exampleModalLabel" aria-hidden="true">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<form action="<?php echo base_url(). 'Admin/MenuController/update'; ?>" method="post"
    				enctype="multipart/form-data">
    				<div class="modal-header">
    					<h5 class="modal-title" id="exampleModalLabel">Edit</h5>
    					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    						<span aria-hidden="true">&times;</span>
    					</button>
    				</div>
    				<div class="modal-body">
    					<div class="form-group">
    						<label>Images</label>
    						<input class="form-control" name="id_menu" type="hidden"
    							value="<?php echo $menu->id_menu ?>">
    						<input class="form-control" name="images" type="file" value="<?php echo $menu->image ?>">
    					</div>
    					<div class="form-group">
    						<label>Name Menu</label>
    						<input class="form-control" name="title" type="text" value="<?php echo $menu->title ?>">
    					</div>
    					<div class="form-group">
    						<label>Price Menu</label>
    						<input class="form-control" name="price" type="text" value="<?php echo $menu->price ?>">
    					</div>
    				</div>

    				<div class="modal-footer">
    					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    					<button type="submit" value="upload" class="btn btn-primary">Save</button>
    				</div>
    			</form>
    		</div>
    	</div>
    </div>


    <?php endforeach;?>

    <!-- End Edit Modal-->


    <!-- Add Modal Tpping-->
    <!-- <div class="modal fade" id="addTopping" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    	aria-hidden="true">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<form action="<?php echo base_url(). 'Admin/MenuController/addTopping'; ?>"
    				enctype="multipart/form-data" method="post">
    				<div class="modal-header">
    					<h5 class="modal-title" id="exampleModalLabel">Add Topping</h5>
    					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    						<span aria-hidden="true">&times;</span>
    					</button>
    				</div>
    				<div class="modal-body">
    					<div class="form-group">
    						<label>Images</label>
    						<input class="form-control" name="images" type="file">
    					</div>
    					<div class="form-group">
    						<label>Name Topping</label>
    						<input class="form-control" name="name_topping" type="text">
    					</div>
    					<div class="form-group">
    						<label>Price Topping</label>
    						<input class="form-control" name="price_topping" type="text">
    					</div>
    				</div>

    				<div class="modal-footer">
    					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    					<button type="submit" value="upload" class="btn btn-primary">Save</button>
    				</div>
    			</form>
    		</div>
    	</div>
    </div> -->

    <!-- Edit Modal-->

    <!-- Sweet Alert Delete -->
    <script>
    	$(".remove").click(function () {
    		var id = $(this).parents("tr").attr("id");

    		swal({
    				title: "Apa kamu yakin?",
    				text: "Data yang dihapus tidak dapat dikembalikan!",
    				type: "warning",
    				showConfirmButton: true,
    				confirmButtonClass: "btn-danger btn-sm",
    				showCancelButton: true,
    				cancelButtonClass: "btn-primary btn-sm",
    				buttons: true,
    				closeOnConfirm: false,
    				closeOnCancel: false,
    			},
    			function (isConfirm) {
    				if (isConfirm) {
    					$.ajax({
    						url: "<?php echo site_url('Admin/MenuController/delete')?>/" + id,
    						type: 'DELETE',
    						error: function () {
    							alert('Something is wrong');
    						},
    						success: function (data) {
    							$("#" + id).remove();
    							swal("Dihapus!", "Data berhasil dihapus.",
    								"success");
    						}
    					});
    				} else {
    					swal({
    						type: "error",
    						icon: "error",
    						title: "Gagal!",
    						text: " Data gagal dihapus! ",
    						timer: 2500,
    						showConfirmButton: false,
    						showCancelButton: false,
    						buttons: false,
    					});
    				}
    			});

    	});

    </script>
    <!-- Sweet Alert Delete -->
