<body class="layout-row"> 
  <div id="aside" class="page-sidenav no-shrink bg-light nav-dropdown fade" aria-hidden="true">
  	<div class="sidenav h-100 modal-dialog bg-light">
  		<!-- sidenav top -->
  		<div class="navbar">
  			<!-- brand --> <a href="" class="navbar-brand"><svg width="32" height="32" viewBox="0 0 512 512"
  					xmlns="http://www.w3.org/2000/svg" fill="currentColor">
  					<g class="loading-spin" style="transform-origin: 256px 256px">
  						<img src="<?php echo base_url('assets/image/logo/coffee-cup.svg')?>">
  					</g>
  				</svg><!-- <img src="../assets/img/logo.png" alt="..."> --> <span style="color: #964B00"
  					class="hidden-folded d-inline l-s-n-1x">Coffee</span> </a><!-- / brand -->
  		</div><!-- Flex nav content -->
  		<div class="flex scrollable hover">
  			<div class="nav-active-text-primary" data-nav>
  				<ul class="nav bg">
  					<li class="nav-header hidden-folded"><span class="text-muted">Website</span></li>
  					<li><a href="<?php echo base_url('welcome') ?>"><span class="nav-icon text-primary"><i
  									data-feather="globe"></i></span> <span class="nav-text">View Website</span></a>
  					</li>
  					<?php 
         			   $level = $this->session->userdata('level');
         			   $queryMenu = "SELECT `menu`.`id`, `menu`
         			   FROM `menu` JOIN `access_menu`
         			   ON `menu`.`id` = `access_menu`.`menu_id`
         			   WHERE `access_menu`.`level` = $level
         			   ORDER BY `access_menu`.`menu_id` ASC
         			   ";
         			   $menu = $this->db->query($queryMenu)->result_array();
         			   ?>
  								<?php foreach ($menu as $m) : ?>
  								<li class="nav-header hidden-folded"><span class="text-muted"><?= $m['menu']; ?></span></li>
  								<?php 
         			     $menuId = $m['id'];
         			     $querySubMenu = "SELECT *
         			     FROM `sub_menu` JOIN `menu` 
         			     ON `sub_menu`.`menu_id` = `menu`.`id`
         			     WHERE `sub_menu`.`menu_id` = $menuId
         			     AND `sub_menu`.`is_active` = 1
         			     ";
         			     $subMenu = $this->db->query($querySubMenu)->result_array();
         			     ?>
  					<?php foreach ($subMenu as $sm) : ?>
  					<li><a href="<?=base_url($sm['url']);?>"><span class="nav-icon text-primary"><i
  									data-feather="<?=$sm['icon'];?>"></i></span> <span
  								class="nav-text"><?=$sm['title'];?></span></a>
  					</li>
  					<?php endforeach ;?>
  					<?php endforeach; ?>
  					<?php if($this->session->userdata('level') == 1) :?>
					<li class="nav-header hidden-folded"><span class="text-muted">Print</span></li>
  					<li><a href="#" class=""><span class="nav-icon text-primary"><i
  									data-feather="file-text"></i></span> <span class="nav-text">Print Report</span>
  							<span class="nav-caret"></span></a>
  						<ul class="nav-sub nav-mega">
  							<!-- <li><a href="<?php echo base_url('Admin/SalaryReport') ?>" class=""><span
  										class="nav-text">Salary</span></a></li> -->
  							<li><a href="<?php echo base_url('Admin/MoneyInReport') ?>" class=""><span
  										class="nav-text">Money
  										in</span></a></li>
  							<li><a href="<?php echo base_url('Admin/SalaryReport') ?>" class=""><span
  										class="nav-text">Money
  										out</span></a></li>
  						</ul>
  					</li>
  					<li class="nav-header hidden-folded"><span class="text-muted">Logouts</span></li>
  					<li><a  href="#" data-target=".bs-example-modal-sm" data-toggle="modal"><span
  								class="nav-icon text-primary"><i data-feather="log-out"></i></span> <span
  								class="nav-text">Sign Out</span></a></li>
  					<?php else:?>
  					<li class="nav-header hidden-folded"><span class="text-muted">Logouts</span></li>
  					<li><a  href="#" data-target=".bs-example-modal-sm" data-toggle="modal"><span
  								class="nav-icon text-primary"><i data-feather="log-out"></i></span> <span
  								class="nav-text">Sign Out</span></a></li>
  					<?php endif;?>
  				</ul>
  			</div>
  		</div><!-- sidenav bottom -->
  	</div>
  </div><!-- ############ Aside END-->
  <div id="main" class="layout-column flex">
  	<!-- ############ Header START-->
  	<div id="header" class="page-header">
  		<div class="navbar navbar-expand-lg">
  			<!-- brand --> <a href="index.html" class="navbar-brand d-lg-none"><svg width="32" height="32"
  					viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" fill="currentColor">
  					<g class="loading-spin" style="transform-origin: 256px 256px">
  						<img src="<?php echo base_url('assets/image/logo/coffee-cup.svg')?>">
  					</g>
  				</svg><!-- <img src="../assets/img/logo.png" alt="..."> --> <span style="color: #964B00"
  					class="hidden-folded d-inline l-s-n-1x">Coffee</span></a><!-- / brand -->
  			<!-- Navbar collapse -->
  			<div class="collapse navbar-collapse order-2 order-lg-1" id="navbarToggler">
  				<form class="input-group m-2 my-lg-0">
  					<div class="input-group-prepend"><button type="button"
  							class="btn no-shadow no-bg px-0 text-inherit"><i data-feather="search"></i></button></div>
  					<input type="text" class="form-control no-border no-shadow no-bg typeahead"
  						placeholder="Search components..." data-plugin="typeahead" data-api="../assets/api/menu.json">
  				</form>
  			</div>
  			<ul class="nav navbar-menu order-1 order-lg-2">
  				<?php foreach ($profile as $prof) : ?>
  				<li class="nav-item dropdown"><a href="#" data-toggle="dropdown"
  						class="nav-link d-flex align-items-center px-2 text-color"><span class="avatar w-24"
  							style="margin: -2px"><img
  								src="<?php echo base_url('assets/image/profile/') . $prof->photo ?>"
  								alt="..."></span></a>
  					<div class="dropdown-menu dropdown-menu-right w mt-3 animate fadeIn"><a class="dropdown-item"
  							href="page.profile.html"><span><?php echo $prof->firstname.' '.$prof->lastname ?></span></a>
  						<div class="dropdown-divider"></div><a class="dropdown-item"
  							href="<?php echo base_url('Admin/SettingController')?>"><span>Settings</span>
  						</a>
  						<div class="dropdown-divider"></div><a class="dropdown-item" href="#" data-target=".bs-example-modal-sm" data-toggle="modal">Sign
  							out</a>
  					</div>
  				</li><!-- Navarbar toggle btn -->
  				<li class="nav-item d-lg-none"><a href="#" class="nav-link px-2" data-toggle="collapse"
  						data-toggle-class data-target="#navbarToggler"><i data-feather="search"></i></a></li>
  				<li class="nav-item d-lg-none"><a class="nav-link px-1" data-toggle="modal" data-target="#aside"><i
  							data-feather="menu"></i></a></li>
  				<?php endforeach ;?>
  			</ul>
  		</div>
  	</div>

<div tabindex="-1" class="modal bs-example-modal-sm" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header"><h4>Logout <i class="fa fa-lock"></i></h4></div>
      <div class="modal-body"><i class="fa fa-question-circle"></i> Are you sure you want to log-off?</div>
      <div class="modal-footer"><a class="btn btn-primary btn-block" href="<?php echo base_url('LoginController/logout')?>">Logout</a></div>
    </div>
  </div>
</div>


<script>
$("a.move").on("click", function(){
	window.open('<?php echo base_url('welcome')?>');
})
</script>