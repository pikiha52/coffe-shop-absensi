	<footer>
		<div class="section bg-dark">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-12 col-md-6 text-center text-md-left">
						<h3>coffe</h3>
						<ul class="list-inline-dash margin-top-20">
							<li><a href="<?php echo base_url('CareersController') ?>">Careers</a></li>
						</ul>
						<p class="margin-top-10">&copy; 2021.</p>
					</div>
				</div><!-- end row -->
			</div><!-- end container -->
		</div>
	</footer>

	<!-- Scroll to top button -->
	<div class="scrolltotop">
		<a class="button-circle button-circle-sm button-circle-dark" href="#"><i class="fa fa-sort-up"></i></a>
	</div>
	<!-- end Scroll to top button -->

	<script>
    //sweetalert for success or error message
    <?php if($this->session->flashdata('success') == 'careers_success'):?>
        swal({
            type: "success",
            icon: "success",
            title: "Berhasil",
            text: "Lamaran akan segera diproses admin",
            timer: 1800,
            showConfirmButton: false,
            showCancelButton: false,
            buttons: false,
        });
        <?php elseif($this->session->flashdata('error') == 'careers_failed'):?>
            swal({
                type: "error",
                icon: "error",
                title: "Gagal!",
                text: "Lamaran gagal dikirim, terjadi kesalahan",
                timer: 1800,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
		<?php elseif($this->session->flashdata('success') == 'contact_success'):?>
            swal({
                type: "success",
                icon: "success",
                title: "Berhasil",
                text: "Komentar anda berhasil dikirim, dan akan segera diproses oleh admin.",
                timer: 1800,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
		<?php elseif($this->session->flashdata('error') == 'contact_failed'):?>
            swal({
                type: "error",
                icon: "error",
                title: "Gagal!",
                text: "Komentar anda gagal dikirim, terjadi kesalahan.",
                timer: 1800,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
        <?php endif; ?>
    </script>

	<!-- ***** JAVASCRIPTS ***** -->
	<script src="<?php echo base_url('assets/plugins/jquery.min.js')?>"></script>
	<script src="<?php echo base_url('assets/plugins/plugins.js')?>"></script>
	<script src="<?php echo base_url('assets/js/functions.min.js')?>"></script>
</body>

<!-- Mirrored from mono.flatheme.net/home/creative-agency-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Feb 2021 06:03:46 GMT -->
</html>
