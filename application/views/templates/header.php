<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from mono.flatheme.net/home/creative-agency-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Feb 2021 06:00:59 GMT -->
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title><?= $title ?></title>
	<!-- Favicon -->
	<link rel="icon" href="<?php echo base_url('assets/image/logo/coffee-cup.svg')?>">
	<!-- CSS -->
	<link href="<?php echo base_url('assets/plugins/bootstrap/bootstrap.min.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/plugins/owl-carousel/owl.carousel.min.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/plugins/owl-carousel/owl.theme.default.min.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/plugins/magnific-popup/magnific-popup.min.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/plugins/sal/sal.min.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/theme.min.css')?>" rel="stylesheet">
	<!-- Fonts/Icons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="<?php echo base_url('assets/plugins/themify/themify-icons.min.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/plugins/simple-line-icons/css/simple-line-icons.css')?>" rel="stylesheet">
	<!-- sweetallert -->
	<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
	<link rel="" href="<?php echo base_url('assets/css/sweetalert.css') ?>" />
	<script src="<?php echo base_url('assets/js/sweetalert.min.js')?>"></script>
</head>
<body data-preloader="1">

	<!-- Header -->
	<div class="header light">
		<div class="container">
			<!-- Logo -->
			<div class="header-logo">
				<h3><a  href="<?php echo base_url('welcome')?>"><img src="<?php echo base_url('assets/image/logo/coffee-cup.svg')?>"> coffee</a></h3>
			</div>
			<!-- Menu -->
			<?php if($this->session->userdata('level')==='1'||$this->session->userdata('level')==='2'):?>
			<div class="header-menu">
				<ul class="nav">
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('Welcome')?>">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('BlogsController')?>">Blogs</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('ContactsController')?>">Comments</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('LocationsController')?>">Locations</a>
					</li>
					<!-- <li class="nav-item">
						<a class="nav-link" href="project.html">Menu</a>
					</li> -->
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('CareersController')?>">Careers</a>
					</li>
				</ul>
			</div>
			<?php elseif ($this->session->userdata('level')==='3') :?>
				<div class="header-menu">
					<ul class="nav">
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url('Welcome')?>">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url('BlogsController')?>">Blogs</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url('ContactsController')?>">Comments</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url('LocationsController')?>">Locations</a>
						</li>
						<!-- <li class="nav-item">
							<a class="nav-link" href="project.html">Menu</a>
						</li> -->
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url('CareersController')?>">Careers</a>
						</li>
					</ul>
				</div>
				<?php else:?>
					<div class="header-menu">
						<ul class="nav">
							<li class="nav-item">
								<a class="nav-link" href="<?php echo base_url('Welcome')?>">Home</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo base_url('BlogsController')?>">Blogs</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo base_url('ContactsController')?>">Comments</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo base_url('LocationsController')?>">Locations</a>
							</li>
							<!-- <li class="nav-item">
								<a class="nav-link" href="project.html">Menu</a>
							</li> -->
							<li class="nav-item">
								<a class="nav-link" href="<?php echo base_url('CareersController')?>">Careers</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo base_url('LoginController')?>">Logins</a>
							</li>
						</ul>
					</div>
				<?php endif;?>

				<!-- Menu Toggle -->
				<button class="header-toggle">
					<span></span>
				</button>
			</div><!-- end container -->
		</div>
		<!-- end Header -->
