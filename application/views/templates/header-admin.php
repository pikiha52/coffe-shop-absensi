	<!DOCTYPE html>
	<html lang="en">

	<head>
		<meta charset="utf-8">
		<title><?= $title ?></title>
		<meta name="description" content="Responsive, Bootstrap, BS4">
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
		<!-- logo -->
		<link rel="icon" href="<?php echo base_url('assets/image/logo/coffee-cup.svg')?>">

		<!-- style online -->
		<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'> 
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" type="text/javascript"></script> 
		<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script> 
		<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.css" />
		<script src="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.js"></script> 

		<!-- style offile -->
		<link rel="stylesheet" href="<?php echo base_url('assets/css/site.min.css') ?>" />
		<!-- <link rel="stylesheet" href="<?php echo base_url('assets/fonts/fonts-monserrat.css') ?>" />
		<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/gijgo.min.js') ?>" type="text/javascript"></script>
		<link href="<?php echo base_url('assets/css/gijgo.min.css') ?>" rel="stylesheet" type="text/css" />
		<script src="<?php echo base_url('assets/js/sweetalert.min.js')?>"></script>
		<link rel="" href="<?php echo base_url('assets/css/sweetalert.css') ?>" />
		<script src="<?php echo base_url('assets/js/sweetalert.js') ?>" type="text/javascript"></script>  -->

	</head>
		<!-- ############ Aside START-->

		<!-- ############ Header START-->
