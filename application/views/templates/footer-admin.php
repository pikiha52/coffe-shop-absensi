<div tabindex="-1" class="modal bs-example-modal-sm" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header"><h4>Logout <i class="fa fa-lock"></i></h4></div>
      <div class="modal-body"><i class="fa fa-question-circle"></i> Are you sure you want to log-off?</div>
      <div class="modal-footer"><a class="btn btn-primary btn-block" href="<?php echo base_url('LoginController/logout')?>">Logout</a></div>
    </div>
  </div>
</div>


<!-- ############ Footer START-->
<div id="footer" class="page-footer hide">
	<div class="d-flex p-3"><span class="text-sm text-muted flex">&copy; Copyright. flatfull.com</span>
		<div class="text-sm text-muted">Version 1.1.2</div>
	</div>
</div><!-- ############ Footer END-->
</div>
<script src="<?php echo base_url('assets/js/site.min.js')?>"></script>
<!-- JS HIGHTCHART ONLINE -->
<!-- <script src="https://code.highcharts.com/stock/highstock.js"></script>
<script src="https://code.highcharts.com/stock/modules/data.js"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
<script src="https://code.highcharts.com/stock/modules/export-data.js"></script> -->

<!-- JS HIGHTCHART ONLINE -->
<!-- <script src="<?php echo base_url('assets/js/highstock.js') ?>"></script>
<script src="<?php echo base_url('assets/js/data.js') ?>"></script>
<script src="<?php echo base_url('assets/js/exporting.js') ?>"></script>
<script src="<?php echo base_url('assets/js/export-data.js') ?>"></script> -->

<script>
    //sweetalert for success or error message
    <?php if($this->session->flashdata('success') == 'message_success'):?>
        swal({
            type: "success",
            icon: "success",
            title: "Berhasil",
            text: "Selamat, data berhasil diupdate ",
            timer: 1800, 
            showConfirmButton: false,
            showCancelButton: false,
            buttons: false,
        });
        <?php elseif($this->session->flashdata('error') == 'message_failed'):?>
            swal({
                type: "error",
                icon: "error",
                title: "Gagal!",
                text: " Gagal melakukan update",
                timer: 1800,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
        <?php elseif($this->session->flashdata('success') == 'absen_berhasil'):?>
        swal({
            type: "success",
            icon: "success",
            title: "Berhasil",
            text: "Selamat, anda telah berhasil absen.",
            timer: 1800,
            showConfirmButton: false,
            showCancelButton: false,
            buttons: false,
        });
        <?php elseif($this->session->flashdata('error') == 'absen_gagal'):?>
            swal({
                type: "error",
                icon: "error",
                title: "Gagal!",
                text: "Gagal melakukan absen, harap hubungi admin.",
                timer: 1800,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
            <?php elseif($this->session->flashdata('success') == 'sudah_absen'):?>
                swal({
                    type: "info",
                    icon: "info",
                    title: "Perhatian!",
                    text: "Anda telah melakukan absen hari ini!",
                    timer: 2000,
                    showConfirmButton: false,
                    showCancelButton: false,
                    buttons: false,
                });
                <?php elseif($this->session->flashdata('error') == 'jam_absen'):?>
                swal({
                    type: "error",
                    icon: "error",
                    title: "Baca!",
                    text: "anda tidak bisa melakukan absen, karna sekarang belum waktunya untuk absen!",
                    timer: 2500,
                    showConfirmButton: false,
                    showCancelButton: false,
                    buttons: false,
                });
                <?php elseif($this->session->flashdata('error') == 'absen_jarak'):?>
                    swal({
                        type: "error",
                        icon: "error",
                        title: "Baca!",
                        text: "anda tidak bisa melakukan absen, karna jarak anda terlalu jauh dari coffeshop, harap lebih mendekat",
                        timer: 2000,
                        showConfirmButton: false,
                        showCancelButton: false,
                        buttons: false,
                    });
                    <?php elseif($this->session->flashdata('error') == 'kasir_failed'):?>
                    swal({
                        type: "error",
                        icon: "error",
                        title: "Baca!",
                        text: "anda tidak bisa melakukan transaksi, karna anda belum melakukan absen",
                        timer: 2000,
                        showConfirmButton: false,
                        showCancelButton: false,
                        buttons: false,
                    });
        <?php endif; ?>
    </script>

</body>

</html>>
