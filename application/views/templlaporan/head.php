	<!DOCTYPE html>
	<html lang="en">

	<head>
		<meta charset="utf-8">
		<title><?= $title ?></title>
		<meta name="description" content="Responsive, Bootstrap, BS4">
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1"><!-- style -->
		<link rel="icon" href="<?php echo base_url('assets/image/logo/coffee-cup.svg')?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/css/site.min.css') ?>">
		<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
		<!-- datatables -->
		<script src="https://code.jquery.com/jquery-3.5.1.js" ></script>
		<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js" ></script>
		<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap5.min.js" ></script>
		<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.bootstrap5.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js
		"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.colVis.min.js"></script>
		<!-- datatables -->
	</head>

	<body class="layout-row">
		<!-- ############ Aside START-->

		<!-- ############ Header START-->
