<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
  <meta name="generator" content="Hugo 0.79.0">
  <title><?= $title ?></title>
  <link rel="icon" href="<?php echo base_url('assets/image/logo/coffee-cup.svg')?>">
  <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sign-in/">

  

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">

  <style>
    .bd-placeholder-img {
      font-size: 1.125rem;
      text-anchor: middle;
      -webkit-user-select: none;
      -moz-user-select: none;
      user-select: none;
    }

    @media (min-width: 768px) {
      .bd-placeholder-img-lg {
        font-size: 3.5rem;
      }
    }
  </style>

  
  <!-- Custom styles for this template -->
  <link href="<?php echo base_url('assets/signin.css')?>" rel="stylesheet">
</head>
<body class="text-center">

  <main class="form-signin">
    <form action="<?php echo base_url('Kasir/KasirController/loginCashier'); ?>" method="post">
      <img class="mb-4" src="<?php echo base_url('assets/image/logo/coffee-cup.svg')?>" alt="" width="72" height="57">
      <h1 class="h3 mb-3 fw-normal">Please sign in</h1>
      <label for="inputEmail" class="visually-hidden">Email address</label>
      <input type="text" id="inputEmail" class="form-control" name="nip" placeholder="Email address" required autofocus>
      <!-- <input type="text" id="inputEmail" class="form-control" name="id_position" value="<?= $kasir ?>"> -->
      <label for="inputPassword" class="visually-hidden">Password</label>
      <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me" name="remember"> Remember me
        </label>
      </div>
      <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
      <!-- <p class="mt-5 mb-3 text-muted">&copy; 2017-2020</p>  -->
    </form>
  </main>


  
</body>
</html>
