<body>
	<div class="contentPOS">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-7 order-xl-first order-last">
					<div class="card card-custom gutter-b bg-white border-0">
						<div class="card-body">
						</div>
						<div class="product-items">
							<div class="row">
								<?php foreach($menu as $m) :?>
								<div class="col-xl-4 col-lg-2 col-md-3 col-sm-4 col-6">
									<div class="productCard">
										<div class="productThumb"><a
												href="<?php echo base_url('admin/KasirController/storeCarts/'. $lat .'/'.$lng .'/'.$m->id_menu) ?>">
												<img class="img-fluid"
													src="<?php echo base_url('assets/image/menu/'. $m->image) ?>"
													alt="ix"></a>
										</div>
										<div class="productContent">
											<input type="hidden" name="id_menu" value="<?php echo $m->id_menu ?>">
											<a
												href="<?php echo base_url('admin/KasirController/storeCarts/'. $lat .$lng . $m->id_menu) ?>">
												<?php echo $m->title ?>
												<p>Porsi : <?php echo $m->porsi ?></p>
											</a>
											<a href="#">
												Rp <?php echo rupiah($m->price)?>
											</a>
										</div>
									</div>
								</div>
								<?php endforeach ;?>
							</div>
						</div>


					</div>
				</div>
				<div class="col-xl-5 col-lg-8 col-md-8">
					<div class="">
						<form action="<?php echo base_url('admin/KasirController/storeOrder') ?>" method="post">
							<div class="card card-custom gutter-b bg-white border-0 table-contentpos">
								<div class="table-datapos">
									<div class="table-responsive" id="printableTable">
										<table id="orderTable" class="display" style="width:100%">
											<?php if($carts == NULL) :?>
											<br>
											<div class="alert alert-warning alert-dismissible fade show" role="alert">
												Carts null.
												<button type="button" class="btn-close" data-bs-dismiss="alert"
													aria-label="Close"></button>
											</div>
											<br>
											<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
												<symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
													<path
														d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
												</symbol>
												<symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
													<path
														d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z" />
												</symbol>
												<symbol id="exclamation-triangle-fill" fill="currentColor"
													viewBox="0 0 16 16">
													<path
														d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
												</symbol>
											</svg>
											<div class="alert alert-success alert-dismissible fade show" role="alert">
												<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img"
													aria-label="Success:">
													<use xlink:href="#check-circle-fill" /></svg><a href="#"
													data-bs-toggle="modal" data-bs-target="#struk">
													The order is successful, please click to print
													<i class="fas fa-print mr-2"></i></a>
												<button type="button" class="btn-close" data-bs-dismiss="alert"
													aria-label="Close"></button>
											</div>
											<?php else:?>
											<thead>
												<tr>
													<th>Name</th>
													<th>Quantity</th>
													<th>Price</th>
													<th>Total</th>
													<th class=" text-right no-sort"></th>
												</tr>
											</thead>
											<tbody>
												<?php foreach($carts as $cart) :?>
												<tr class="line" id="<?php echo $cart->id_carts ?>">
													<input type="hidden" class="form-control" name="id_menu[]"
														value="<?php echo $cart->id_menu ?>">
													<input type='hidden' value="<?php echo $numberorder ?>"
														name="number">
													<td><?php echo $cart->title ?></td>
													<td>
														<input type="number" value="1" max="10" min="1" name="qty[]"
															id="qty" class="form-control border-dark w-100px qty"
															placeholder="">
													</td>
													<td>Rp <?php echo rupiah($cart->price) ?>
														<input type="hidden" name="price" id="harga" class="value"
															value="<?php echo $cart->price ?>">
														<input type="hidden" name="id_carts[]" id="id_carts"
															value="<?php echo $cart->id_carts ?>">
													</td>
													<td><input type="number" name="total[]"
															class="form-control border-dark w-100px tot_price"
															id="total" value="<?php echo $cart->price ?>" readonly></td>
													<td>
														<div class="card-toolbar text-right">
															<a class="confirm-delete remove" title="Delete"><i
																	class="fa fa-trash remove"></i></a>
														</div>
													</td>
												</tr>
												<input type="text" hidden value="<?php echo $cart->porsi ?>" name="porsi[]">

												<?php endforeach ;?>
											</tbody>
											<tfoot>
												<tr>
													<td><span style="color: white;">akaka</span></td>
													<td>Total Payment</td>
													<td><input type='number'
															class="form-control border-dark w-100px grand_total"
															name="grand_total" id="grand_total">
													</td>
												</tr>
												<tr>
													<td>Cash</td>
													<td>
														<input type='number' class="form-control border-dark cash"
															name="cash" id="cash">
													</td>
												</tr>
												<tr>
													<td>Change</td>
													<td>
														<input type='number' class="form-control border-dark change"
															name="change" id="change">
														<input type="text" hidden value="<?php echo $lat ?>" name="lat">
														<input type="text" hidden value="<?php echo $lng ?>" name="lng">
													</td>
												</tr>
											</tfoot>
											<?php endif;?>
										</table>
									</div>
								</div>
								<div class="card-body">
									<div class="form-group row mb-0">
										<div class="col-md-12 btn-submit d-flex justify-content-end">

											<button type="submit" class="btn btn-info mr-2" title="Apply">
												<i class="fa fa-money mr-2"></i>
												Pay
											</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- cetak -->
	<div class="modal fade" id="struk" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="col-xl-12 col-lg-12 col-md-12">
						<div class="card card-custom gutter-b bg-white border-0">
							<div class="card-body">
								<div class="shop-profile">
									<div class="media">
										<div
											class="w-100px h-100px d-flex justify-content-center align-items-center">
											<img src="<?php echo base_url('assets/image/logo/coffee-cup.svg')?>" alt="">
										</div>
										<div class="media-body ml-3">
											<h3 class="title font-weight-bold">Coffe</h3>
											<p class="phoonenumber">
												+62 895-226-xxx-xx
											</p>
											<p class="adddress">
												Depok, Xxxxxxx
											</p>
											<p class="countryname">Indonesia</p>
										</div>
									</div>
								</div>

								<div class="resulttable-pos">
									<table class="table right-table">
										<tbody>
										<tr class="d-flex align-items-center justify-content-between">
												<th class="border-0 font-size-h5 mb-0 font-size-bold text-dark">
													Number Orders
												</th>
												<td
													class="border-0 justify-content-end d-flex text-dark font-size-base">
													<?php echo $lastIdOrder?></td>

											</tr>
											<tr class="d-flex align-items-center justify-content-between">
												<th class="border-0 font-size-h5 mb-0 font-size-bold text-dark">
													<?php echo $today ?>
												</th>
												<td
													class="border-0 justify-content-end d-flex text-dark font-size-base">
													<?php echo $time?></td>

											</tr>
											<?php foreach($orders as $order):?>

											<tr class="d-flex align-items-center justify-content-between">
												<th class="border-0 font-size-h5 mb-0 font-size-bold text-dark">
												<?php foreach(json_decode($order->id_menu) as $orders) :?>
												<li><?php echo $this->menu->show($orders);?></li>
												<?php endforeach ;?>
												</th>
												<th>
												<?php foreach (json_decode($order->qty) as $qty) :?>
												<li><?php echo $qty ?></li>
												<?php endforeach ;?>
												</th> 
												<td
													class="border-0 justify-content-end d-flex text-dark font-size-base">
													<?php foreach (json_decode($order->total) as $total) :?>
													<li>Rp <?php echo rupiah($total) ?></li>
													<?php endforeach ;?>

											</tr>
											<?php endforeach;?>

											<tr class="d-flex align-items-center justify-content-between item-price">
												<th class="border-0 font-size-h5 mb-0 font-size-bold text-primary">

													TOTAL
												</th>
												<td
													class="border-0 justify-content-end d-flex text-primary font-size-base">
													Rp <?php echo $order->grand_total ?></td>

											</tr>
										</tbody>
									</table>
								</div>
								<div class="resulttable-pos">
									<table class="table right-table">
										<tbody>
										<tr class="d-flex align-items-center justify-content-between">
												<th class="border-0 font-size-h5 mb-0 font-size-bold text-dark">
													Cashier
												</th>
												<td
													class="border-0 justify-content-end d-flex text-dark font-size-base">
													<?php echo $order->firstname.' '.$order->lastname ?></td>
											</tr>
											<tr class="d-flex align-items-center justify-content-between">
												<th class="border-0 font-size-h5 mb-0 font-size-bold text-dark">
													Cash
												</th>
												<td
													class="border-0 justify-content-end d-flex text-dark font-size-base">
													Rp <?php echo rupiah($order->cash) ?></td>
											</tr>
											<tr class="d-flex align-items-center justify-content-between">
												<th class="border-0 font-size-h5 mb-0 font-size-bold text-dark">
													Change
												</th>
												<td
													class="border-0 justify-content-end d-flex text-dark font-size-base">
													Rp <?php echo str_replace('-', '', rupiah($order->change)) ?></td>
											</tr>
										</tbody>
									</table>
								</div>

								<div class="d-flex justify-content-end align-items-center flex-column buttons-cash">
									<div>
										<button onclick="window.print()" class="btn btn-primary white mb-2" data-toggle="modal"
											data-target="#payment-popup">
											<i class="fas fa-print"></i>
											Print
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- all order -->
	<div class="modal fade" id="allorder" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-body">
					<div class="box-body">
						<div class="table-responsive">
							<table class=" table table-bordered table-hover" id="table-id" style="font-size:13px;">
								<thead>
									<th>Cashier</th>
									<th>Number Order</th>
									<th>Name Menu</th>
									<th>Qty</th>
									<th>Total</th>
									<th>Grand Total</th>
									<th>Money Cash</th>
									<th>Money Change</th>
									<th>Date Order</th>
								</thead>
								<tbody>
								<?php foreach($getAllOrder as $allOrder) :?>
								<tr>
								<td><?php echo $allOrder->firstname.' '.$allOrder->lastname ?></td>
								<td><?php echo $allOrder->numberorder ?></td>
								<td>
									<?php foreach(json_decode($allOrder->id_menu) as $menu) :?>
									<ul><li><?php echo $this->menu->show($menu); ?></li></ul>
									<?php endforeach ;?>
								</td>
								<td>
									<?php foreach (json_decode($allOrder->qty) as $jumlah) : ?>
									<ul><li><?php echo $jumlah ?></li></ul>
									<?php endforeach ;?>
								</td>
								<td>
									<?php foreach (json_decode($allOrder->total) as $total) :?>
									<ul><li>Rp <?php echo rupiah($total); ?></li></ul>
									<?php endforeach ;?>
								</td>
								<td>Rp <?php echo rupiah($allOrder->grand_total) ?></td>
								<td>Rp <?php echo rupiah($allOrder->cash) ?></td>
								<td>Rp <?php echo str_replace('-', '', rupiah($allOrder->change)) ?></td>
								<td><?php echo $allOrder->created_at ?></td>
								</tr>
								<?php endforeach ;?>
								</tbody>
							</table>
						</div>
					</div>

					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	</div>


<!-- Sweet Alert Delete -->
<script>
$(".remove").click(function(){
	var lat = document.getElementById("lat");
	var lng = document.getElementById("lng");
    var id = $(this).parents("tr").attr("id");

    swal({
        title: "Apa kamu yakin?",
        text: "Data yang dihapus tidak dapat dikembalikan!",
        type: "warning",
        showConfirmButton: true,
        confirmButtonClass: "btn-danger btn-sm",
        showCancelButton: true,
        cancelButtonClass: "btn-primary btn-sm",
        buttons: true,
        closeOnConfirm: false,
        closeOnCancel: false,
    },
    function(isConfirm) {
        if (isConfirm) {
          $.ajax({
           url: "<?php echo site_url('admin/KasirController/destroy')?>/"+id,
           type: 'DELETE',
           error: function() {
            alert('Something is wrong');
        },
        success: function(data) {
          $("#"+id).remove();
          swal("Dihapus!", "Data berhasil dihapus.", "success");
      }
  });
      } else {
          swal({
                type: "error",
                icon: "error",
                title: "Gagal!",
                text: " Data gagal dihapus! ",
                timer: 2500,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
      }
  });

});
</script>
