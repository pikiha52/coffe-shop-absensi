

	<!-- Optional JavaScript; choose one of the two! -->


	<script>
		jQuery(document).ready(function () {
			jQuery('#orderTable').DataTable({

				"info": false,
				"paging": false,
				"searching": false,

				"columnDefs": [{
					"targets": 'no-sort',
					"orderable": false,
				}]
			});
		});

	</script>

<!-- count harga -->
	<script>
		$( document ).ready(function() {
		  $("body").on("keyup", "input", function(event){
			  $(this).closest(".line").find(".tot_price").val( $(this).closest(".line").find(".qty").val()*$(this).closest(".line").find(".value").val() );
		    $(this).closest(".line").val( $(this).closest(".line").find(".tot_price"));
			$(this).closest(".line").find(".change").val( $(this).closest(".line").find(".grand_total").val()-$(this).closest(".line").find(".cash").val() );
		//   
			var sum = 0;
		    $('.tot_price').each(function() {
		        sum += Number($(this).val());
		    });
		    $(".grand_total").val(sum);
		// 

		
		$('.cash').each(function(){
			sum -= Number($(this).val());
		});
		$(".change").val(sum);
		  });
		});
	</script>

<script>
$(document).ready(function() {
    $('#table-id').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ]
    });
});
$('#id-table').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": false,
    "autoWidth": true,
});
</script>

<script>
    //sweetalert for success or error message
    <?php if($this->session->flashdata('success') == 'message_success'):?>
        swal({
            type: "success",
            icon: "success",
            title: "Berhasil",
            text: "Selamat, data berhasil ditambah ",
            timer: 2000,
            showConfirmButton: false,
            showCancelButton: false,
            buttons: false,
        });
        <?php elseif($this->session->flashdata('error') == 'message_failed'):?>
            swal({
                type: "error",
                icon: "error",
                title: "Gagal!",
                text: " Uang cash tidak boleh kurang",
                timer: 4000,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
		<?php elseif($this->session->flashdata('success') == 'message_berhasil'):?>
            swal({
                type: "success",
                icon: "success",
                title: "Berhasil!",
                text: "Pesanan berhasil ditambah, silahkan cetak struk order",
                timer: 2000,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
		<?php elseif($this->session->flashdata('error') == 'message_menu_kosong'):?>
            swal({
                type: "error",
                icon: "error",
                title: "Gagal!",
                text: " Menu tidak boleh kosong",
                timer: 4000,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
		<?php elseif($this->session->flashdata('error') == 'message_cash_kosong'):?>
            swal({
                type: "error",
                icon: "error",
                title: "Gagal!",
                text: " Periksa kembali uang cash",
                timer: 4000,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
        <?php endif; ?>
    </script>

	<!-- Option 1: Bootstrap Bundle with Popper -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
	</script>

	<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"> </script> -->


<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"> </script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>

	<!-- Option 2: Separate Popper and Bootstrap JS -->
	
    <!-- <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script> -->
   
</body>

</html>
 