<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Bootstrap CSS -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
	<link rel="icon" href="<?php echo base_url('assets/image/logo/coffee-cup.svg')?>">
	<link href="<?php echo base_url('assets/css/stylec619.css') ?>" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- SweetAllert CSS -->
	<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
	<link rel="stylesheet" href="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.css" />
		<script src="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.js"></script> 
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" />
	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css" />
	<title><?= $title ?></title>
</head>

<header class="p-3 mb-3 border-bottom">
	<div class="container">
		<div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
			<ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
				<li><a href="#" class="nav-link px-2 link-secondary" data-bs-toggle="modal"
						data-bs-target="#allorder">Overall Orders</a></li>
				<!-- <li><a href="#" class="nav-link px-2 link-secondary">Input Error</a></li> -->
				<li><a href="" class="nav-link px-2 link-secondary">
						<?php echo $today ?> <span class="badge bg-info"><?php echo $time ?></span></a>
				</li>
			</ul>

			<form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
				<input type="search" class="form-control" placeholder="Search menu..." aria-label="Search">
			</form>

			<div class="dropdown text-end">
				<?php foreach($profile as $prof) :?>
				<a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1"
					data-bs-toggle="dropdown" aria-expanded="false">
					<img src="<?php echo base_url('assets/image/profile/'). $prof->photo ?>" alt="mdo" width="32"
						height="32" class="rounded-circle">
				</a>
				<center>
					<span
						class="nav-link px-2 link-secondary"><?php echo $prof->firstname .' '.$prof->lastname ?></span>
				</center>
				<ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
					<li><a class="dropdown-item" href="<?php echo base_url('Admin/SettingController')?>">Settings
							Account</a></li>
					<li>
						<hr class="dropdown-divider">
					</li>
					<li><a class="dropdown-item" href="<?php echo base_url('LoginController/logout')?>">Sign out</a>
					</li>
				</ul>
				<?php endforeach ;?>
			</div>
		</div>
	</div>
</header>
