	<!-- Blog Post section -->
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 px-6">
					<div class="margin-bottom-30">
						<img src="<?php echo base_url('assets/image/blogs/'. $blogs->image)?>" alt="">
					</div>
					<h5><a><?php echo $blogs->title ?></a></h5>

					<p><?php echo $blogs->isi ?></p>
				</div>
			</div><!-- end row -->
			<br>
			<center>
				<div class="border-top"></div>
				<h5>Recent Blogs</h5>
				<div class="row">
					<?php foreach($recentblogs as $recent) :?>
					<div class="col-md-4">
						<div class="text-center">
							<img src="<?php echo base_url('assets/image/blogs/'. $recent->image) ?>" class="rounded"
								alt="...">
							<h6><a
									href="<?php echo base_url('blogscontroller/show/'. $recent->id) ?>"><?php echo $recent->title ?></a>
							</h6>
						</div>
					</div>
					<?php endforeach ;?>
				</div>
			</center>
		</div><!-- end container -->
	</div>
	<!-- end Blog Post section -->
