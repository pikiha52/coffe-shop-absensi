		<div class="section">
			<div class="container">
				<div class="row col-spacing-50">
					<div class="col-12 col-lg-8">
						<!-- Blog Post box 1 -->
						<?php foreach ($blogs as $blog) : ?>
							<div class="margin-bottom-50">
								<div class="hoverbox-8">
									<a href="#">
										<img src="<?php echo base_url('assets/image/blogs/'.$blog->image)?>" alt="">
									</a>
								</div>
								<div class="margin-top-30">
									<div class="d-flex justify-content-between margin-bottom-10">
										<div class="d-inline-flex">
											<a class="font-family-tertiary font-small font-weight-normal uppercase"><?php echo $blog->name ?></a>
										</div>

										<div class="d-inline-flex">
											<span class="font-small"><?php echo $blog->date ?></span>
										</div>
									</div>
									<h5><a href="<?php echo base_url('BlogsController/show/'. $blog->id)?>"><?php echo $blog->title ?></a></h5>
									<p><?php echo substr($blog->isi,0, 300 ) ?>.....</p>
									<div class="margin-top-20">
										<a class="button-text-1" href="<?php echo base_url('BlogsController/show/'. $blog->id)?>">Read More</a>
									</div>
								</div>
							</div>
						<?php endforeach ; ?>
						<!-- Pagination -->
						<nav>
							<ul class="pagination justify-content-center margin-top-70">
								<li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
								<li class="page-item active"><a class="page-link" href="#">1</a></li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
							</ul>
						</nav>
					</div>
					<!-- end Blog Posts -->
					
					<!-- Blog Sidebar -->
					<div class="col-12 col-lg-4 sidebar-wrapper">
						<!-- Sidebar box 2 - Categories -->
						<div class="sidebar-box">
							<h6 class="font-small font-weight-normal uppercase">Kategori</h6>
							<?php foreach($category as $cat) :?>
								<ul class="list-category">
									<li><a href="<?php echo base_url('BlogsController/byCategory/'. $cat->id)?>"><?php echo $cat->name ?></a></li>
								</ul>
							<?php endforeach;?>
						</div>
						<!-- Sidebar box 4 - Banner Image -->
						<div class="margin-bottom-20">
							<a href="#"><img src="../assets/images/blog-banner.jpg" alt=""></a>
						</div>
						<!-- Sidebar box 6 - Facebook Like box -->
						<!-- <div class="sidebar-box text-center">
							<h6 class="font-small font-weight-normal uppercase">Follow on</h6>
							<ul class="list-inline">
								<li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-pinterest"></i></a></li>
								<li><a href="#"><i class="fa fa-behance"></i></a></li>
								<li><a href="#"><i class="fa fa-instagram"></i></a></li>
							</ul>
						</div> -->
					</div>
					<!-- end Blog Sidebar -->
				</div><!-- end row -->
			</div><!-- end container -->
		</div>
