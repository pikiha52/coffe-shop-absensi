		<div class="section">
			<div class="container">
				<div class="row col-spacing-50">
					<div class="col-12 col-lg-8">
						<!-- Blog Post box 1 -->
						<div class="margin-bottom-50">
							<div class="hoverbox-8">
								<a href="#">
									<img src="../assets/images/blog-big-post.jpg" alt="">
								</a>
							</div>
							<div class="margin-top-30">
								<?php if($category == NULL) :?>
								<div class="alert alert-dark alert-dismissible fade show" role="alert">
									Maaf category ini masih kosong.
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<?php else: ?>
									<div class="d-flex justify-content-between margin-bottom-10">
									<div class="d-inline-flex">
										<h1><a class="font-family-tertiary font-small font-weight-normal uppercase"
												href="#">BLOGS BY CATEGORY</a></h1>
									</div>
								</div>
								<?php foreach ($category as $blog) : ?>

								<h5><a
										href="<?php echo base_url('BlogsController/show/'.$blog->id_blogs)?>"><?php echo $blog->title ?></a>
								</h5>
								<!-- <p><?php echo $blog->isi ?></p> -->
								<div class="d-inline-flex">
									<span class="font-small"><?php echo $blog->date ?></span>
								</div>
								<div class="margin-top-20 mb-4">
									<a class="button-text-1"
										href="<?php echo base_url('BlogsController/show/'. $blog->id_blogs)?>">Read More</a>
								</div>
								<?php endforeach ; ?>
								<?php endif;?>
							</div>
						</div>

					</div>
					<!-- end Blog Posts -->

					<!-- Blog Sidebar -->

					<!-- end Blog Sidebar -->

				</div><!-- end row -->
			</div><!-- end container -->
		</div>

		<script>
			$('.alert').alert()
		</script>
