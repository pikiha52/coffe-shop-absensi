		<div class="section">
			<div class="container">
				<div class="row col-spacing-50">
					<div class="col-12 col-lg-12">

						<div class="margin-bottom-50">
							<div class="margin-top-30">
								<div class="d-inline-flex">
									<a class="font-family-tertiary font-small font-weight-normal uppercase">All Careers</a>
								</div>
								<?php foreach ($careers as $car) : ?>
								<div class="d-flex justify-content-between margin-bottom-10">
									<div class="d-inline-flex">
									</div>
									<div class="d-inline-flex">
										<span class="font-small"><?php echo $car->created_at ?></span>
									</div>
								</div>
								<h5><a
										href="<?php echo base_url('CareersController/show/'. $car->id)?>"><?php echo $car->name ?></a>
								</h5>
								<div class="margin-top-20">
									<a class="button-text-1"
										href="<?php echo base_url('CareersController/show/'. $car->id)?>">Read More</a>
								</div>
								<?php endforeach ; ?>
							</div>
						</div>
						<!-- Pagination -->
						<nav>
							<ul class="pagination justify-content-center margin-top-70">
								<li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
								<li class="page-item active"><a class="page-link" href="#">1</a></li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
							</ul>
						</nav>
					</div>

				</div>
			</div>
		</div>
		</div>
