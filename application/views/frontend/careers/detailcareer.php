	<!-- Blog Post section -->
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<!-- <div class="margin-bottom-30">
						<img src="<?php echo base_url('assets/image/blogs/'. $blogs->image)?>" alt="">
					</div> -->
					<h5><a href=""><?php echo $careers->name ?></a></h5>

					<p><?php echo $careers->detail ?></p>
					<p>Limit apply : <?php echo $careers->limit_aply ?></p>
					<?php if($careers->limit_aply <= $today) :?>
					<div class="col-6 text-right">
						<a href="#" type="button"
							class="btn btn-danger">Has passed</a>
					</div>
					<?php else:?>
					<div class="col-6 text-right">
						<a href="<?php echo base_url('CareersController/apply/'. $careers->id)?>" type="button"
							class="btn btn-info">Apply</a>
					</div>
					<?php endif ;?>
					<!-- end Post Tags / Share -->
				</div>
			</div><!-- end row -->
		</div><!-- end container -->
	</div>
	<!-- end Blog Post section -->
