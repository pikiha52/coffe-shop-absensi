	<!-- Blog Post section -->
	<div class="section">
		<div class="container">
			<div class="mb-2">
				<h5><a href="<?php echo base_url('CareersController/show/'. $careers->id)?>"><?php echo $careers->name ?></a></h5>
			</div>
				<form action="<?php echo base_url(). 'CareersController/store'; ?>" enctype="multipart/form-data" method="post">
			<div class="margin-top-50 margin-lg-top-70">
					<div class="form-row">
						<div class="col-12 col-sm-6 col-lg-4">
							<label class="required">First name</label>
							<input type="hidden" name="careers_id" value="<?php echo $careers->id ?>">
							<input type="text" name="firstname" required>
						</div>
						<div class="col-12 col-sm-6 col-lg-4">
							<label class="required">Last name</label>
							<input type="text" name="lastname" required>
						</div>
						<div class="col-12 col-sm-6 col-lg-4">
							<label class="required">Phone</label>
							<input type="number" name="phone" required>
						</div>
						<div class="col-12 col-sm-6 col-lg-4">
							<label class="required">Email address</label>
							<input type="email" name="email" required>
						</div>
						<div class="col-12 col-sm-6 col-lg-4">
							<label class="required">Last education</label>
							<input type="text" name="education" required>
						</div>
						<div class="col-12 col-sm-6 col-lg-4">
							<label class="required">Address</label>
							<input type="text" name="address" placeholder="" required>
						</div>
						<div class="col-12 col-lg-4">
							<label>How were you referred to us?</label>
							<select class="custom-select w-100" name="findme">
								<option selected></option>
								<option value="Ads">Ads</option>
								<option value="Friend">Friend</option>
								<option value="Social Media">Social Media</option>
								<option value="Other">Other</option>
							</select>
						</div>
						<div class="col-12">
							<form>
								<div class="form-group">
									<label for="exampleFormControlFile1" class="required">Resume file</label>
									<input type="file" class="form-control-file" name="resume" id="exampleFormControlFile1">
								</div>
							</form>
						</div>
						<div class="col-12">
							<label>Additional Information(optional)</label>
							<textarea name="additional"></textarea>
						</div>
					</div>
					<button class="button button-lg button-dark button-radius" type="submit">Submit CV</button>
			</div>
				</form>

		</div>
	</div>