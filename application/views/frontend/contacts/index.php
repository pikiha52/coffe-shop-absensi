        <br>
		<br>
		<br>
		<br>
		<!-- Contact Info section -->
		<!-- <div class="section">
			<div class="container">
				<div class="row icon-4xl">
					<div class="col-12 col-sm-6 col-md-3">
						<h6 class="font-weight-normal margin-0">Email:</h6>
						<p>contact@email.com</p>
					</div>
					<div class="col-12 col-sm-6 col-md-3">
						<h6 class="font-weight-normal margin-0">Phone:</h6>
						<p>+(987) 654 321 01</p>
					</div>
					<div class="col-12 col-sm-6 col-md-3">
						<h6 class="font-weight-normal margin-0">Skype:</h6>
						<p>contact.skype</p>
					</div>
					<div class="col-12 col-sm-6 col-md-3">
						<h6 class="font-weight-normal margin-0">Address:</h6>
						<p>121 King St, Melbourne VIC 3000</p>
					</div>
				</div>
			</div>
		</div> -->
		<!-- end Contact Info section -->

		<!-- Contact Form section -->
		<div class="section padding-top-0">
			<div class="container">
				<div class="contact-form">
					<form action="<?php echo base_url('ContactsController/store') ?>" method="post">
						<div class="form-row">
							<div class="col-12 col-sm-6">
								<input type="text" id="name" name="name" placeholder="Name" required>
							</div>
							<div class="col-12 col-sm-6">
								<input type="email" id="email" name="email" placeholder="E-Mail" required>
							</div>
						</div>
						<input type="text" id="subject" name="subject" placeholder="Subject" required>
						<textarea name="comment" id="message" placeholder="Message"></textarea>
						<button class="button button-lg button-outline-dark-2" type="submit">Send Comments</button>
					</form>
				</div><!-- end contact-form -->
			</div><!-- end container -->
		</div>
		<!-- end Contact Form section -->
