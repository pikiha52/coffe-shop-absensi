		<!-- Scroll to top button -->
		<div class="scrolltotop">
			<a class="button-circle button-circle-sm button-circle-dark" href="#"><i class="ti-arrow-up"></i></a>
		</div>
		<!-- end Scroll to top button -->

		<!-- Hero section -->

		<?php foreach ($slide as $slid) :?>
		<div class="owl-carousel owl-nav-overlay owl-dots-overlay" data-owl-nav="false" data-owl-dots="true"
			data-owl-items="1" data-owl-autoPlay="true">
			<!-- Slider box 1 -->
			
			<div class="section-2xl bg-image parallax"
			data-bg-src="<?php echo base_url('assets/image/slide/'. $slid->image)?>">
				<div class="bg-black-03">
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-10 col-lg-8 col-xl-6">
								<h1><?php echo $slid->name ?></h1>
							</div>
						</div><!-- end row -->
					</div><!-- end container -->
				</div>
			</div>
			
		</div><!-- end owl-carousel -->
		<?php endforeach ;?>
		<!-- end Hero section -->

		<!-- Services section -->
		<div class="section">
			<div class="container">
					<center>
						<h6 class="text-muted">DAFTAR MENU</h6>
					</center>
				<div class="col-12">
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-muted" style="text-align: center;">No</th>
									<th class="text-muted">Nama Menu</th>
									<th class="text-muted">Harga</th>
								</tr>
							</thead>
							<tbody>
								<?php $i = 1 ;?>
								<?php foreach ($allMenu as $allMen) :?>
								<tr>
									<th class="text-muted" style="text-align: center;"><?= $i++ ?></th>
									<th><a href="<?php echo base_url('welcome/showMenu/'. $allMen->id_menu) ?>"><?php echo $allMen->title ?></a></th>
									<th class="text-muted">Rp <?php echo rupiah($allMen->price) ?></th>
								</tr>
								<?php endforeach ;?>
							</tbody>
						</table>
					</div>
				</div>

					<center>
						<h6 class="text-muted">COMMENT</h6>
					</center>

				<div class="owl-carousel owl-nav-overlay owl-dots-overlay" data-owl-nav="false" data-owl-dots="true"
			data-owl-items="1" data-owl-autoPlay="true">
			<!-- Slider box 1 -->
			<?php foreach ($comment as $coment) :?>
				<div class="bg-black-03">
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-10 col-lg-8 col-xl-6">
								<h4><?php echo $coment->name ?></h4>
								<p style="color: white;"><?php echo $coment->comment ?></p>
							</div>
						</div><!-- end row -->
					</div><!-- end container -->
				</div>
			<!-- </div> -->
			<?php endforeach ;?>
		</div><!-- end owl-carousel -->

			</div><!-- end container -->
		</div>
		<!-- end Services section -->

		<!-- About section -->
		<div class="section padding-top-0 border-top">
			<div class="container">
				<?php foreach($blogs as $blog) :?>
				<div class="row align-items-center col-spacing-50">
					<div class="col-12 col-lg-6">
						<img src="<?php echo base_url('assets/image/blogs/'. $blog->image)?>" alt="">
					</div>
					<div class="col-12 col-lg-6">
						<h3 class="font-weight-light"><?php echo $blog->title ?></h3>
						<p><?php echo substr($blog->isi, 0, 300) ?>....</p>
						<a class="button button-radius button-lg button-dark margin-top-40"
							href="<?php echo base_url('BlogsController/show/'. $blog->id) ?>">Baca</a>
					</div>
				</div><!-- end row -->
				<?php endforeach ;?>
			</div><!-- end container -->
		</div>
		<!-- end About section -->
		<!-- Team section -->
		<div class="section border-top">
			<div class="container">
				<div class="margin-bottom-70">
					<div class="row text-center">
						<div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2">
							<h6 class="font-small font-weight-normal uppercase">Team</h6>
						</div>
					</div>
				</div>
				<div class="row col-spacing-20 team-wrapper team-box-hover-3">
					<!-- Team box 1 -->
					<?php foreach($profile as $prof) :?>
					<div class="col-12 col-sm-6 col-lg-3 team-box">
						<div class="team-img">
							<img style="height: 280px" src="<?php echo base_url('assets/image/profile/'. $prof->photo)?>"
								alt="">
							<div>
								<ul>
									<li><a href="<?php echo $prof->facebook?>"><i class="fa fa-facebook-f"></i></a></li>
									<li><a href="<?php echo $prof->twitter?>"><i class="fa fa-twitter"></i></a></li>
									<li><a href="<?php echo $prof->instagram?>"><i class="fa fa-instagram"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="margin-bottom-10">
							<h6 class="font-weight-normal margin-0"><?php echo $prof->firstname ?>
								<?php echo $prof->lastname ?></h6>
							<span><?php echo $prof->name ?></span>
						</div>
					</div>
					<?php endforeach ;?>
				</div><!-- end row -->

			</div><!-- end container -->
		</div>
		<!-- end Team section -->
