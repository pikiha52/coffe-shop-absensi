	<!-- Blog Post section -->
	<div class="section">
		<div class="container">
			<div class="row mb-4">
				<div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
					<div class="margin-bottom-30">
						<img src="<?php echo base_url('assets/image/menu/'. $menu->image)?>" style="height: 300px;" alt="">
					</div>
					<h5><a ><?php echo $menu->title ?></a></h5>
					
					<p>Rp <?php echo rupiah($menu->price) ?></p>
					
				</div>
			</div><!-- end row -->
		</div><!-- end container -->
		<!-- <div class="section"> -->
			<div class="container"> 
			<div class="col-md-12">
			<div class="">
					<center>
						<h6 class="text-muted">RECENT MENU</h6>
					</center>
				</div>
				<div class="row icon-5xl">
					<?php foreach($recent as $men) :?>
					<div class="col-12 col-md-4">
						<!-- <i class="ti-briefcase text-dark"></i> -->
						<img src="<?php echo base_url('assets/image/menu/'. $men->image) ?>" style="height: 200px;" alt="">
						<h5 class="font-weight-normal margin-top-20">
						<a href="<?php echo base_url('welcome/showMenu/'. $men->id_menu) ?>"><?php echo $men->title ?></a></h5>
					</div>
					<?php endforeach ;?>
				</div><!-- end row -->
			</div>
	</div>
	<!-- end Blog Post section -->
	
