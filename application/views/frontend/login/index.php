<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
  <meta name="generator" content="Hugo 0.79.0">
  <title><?= $title ?></title>
  <link rel="icon" href="<?php echo base_url('assets/image/logo/coffee-cup.svg')?>">
  <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sign-in/">

  

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">

  <style>
    .bd-placeholder-img {
      font-size: 1.125rem;
      text-anchor: middle;
      -webkit-user-select: none;
      -moz-user-select: none;
      user-select: none;
    }

    @media (min-width: 768px) {
      .bd-placeholder-img-lg {
        font-size: 3.5rem;
      }
    }
  </style>

  
  <!-- Custom styles for this template -->
  <link href="<?php echo base_url('assets/signin.css')?>" rel="stylesheet">
</head>
<body class="text-center">

  <main class="form-signin">
    <form action="<?php echo base_url('LoginController/proses_login'); ?>" method="post">
    <div class="" style="text-align: center;">
			<img class="mb-4" src="<?php echo base_url('assets/image/logo/coffee-cup.svg')?>" alt="" width="72" height="57">
				<h1 class="h3 mb-3 fw-normal">Please sign in</h1>
		</div> 
			<label for="inputEmail" class="visually-hidden">NIP</label>
      <input type="text" id="inputEmail" class="form-control" name="nip" placeholder="Nip" required autofocus>
      <label for="inputPassword" class="visually-hidden">Password</label>
      <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required>
      <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
    </form>
  </main>
 
	<script src="<?php echo base_url('assets/js/sweetalert.min.js')?>"></script>
<script>

    //sweetalert for success or error message
    <?php if($this->session->flashdata('error') == 'login_failed_nip'):?>
        swal({
            type: "error",
            icon: "error",
            title: "GAGAL!",
            text: "NIP tidak terdaftar !",
            timer: 3000,
            showConfirmButton: false,
            showCancelButton: false,
            buttons: false,
        });
        <?php elseif($this->session->flashdata('error') == 'login_failed'):?>
            swal({
                type: "error",
                icon: "error",
                title: "GAGAL!",
                text: " Tolong cek kembali nip dan password anda! ",
                timer: 3000,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
				<?php elseif($this->session->flashdata('error') == 'login_failed_status'):?>
            swal({
                type: "error",
                icon: "error",
                title: "GAGAL!",
                text: "Maaf Anda tidak bisa login, karena status anda nonactive! ",
                timer: 5000,
                showConfirmButton: false,
                showCancelButton: false,
                buttons: false,
            });
        <?php endif; ?>

</script>
  
</body>
</html>
