    <!-- ======= Services Section ======= -->
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="content col-xl-5 d-flex flex-column justify-content-center">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.137334639196!2d106.88495011433429!3d-6.3762663953861045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69ec9f0a5680b7%3A0x3495bd225772d4a9!2sJl.%20Putri%20Tunggal%2C%20Harjamukti%2C%20Kec.%20Cimanggis%2C%20Kota%20Depok%2C%20Jawa%20Barat!5e0!3m2!1sid!2sid!4v1615454858286!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
          </div>
          <div class="col-xl-7">
            <div class="icon-boxes d-flex flex-column justify-content-center">
              <div class="row">
                <div class="col-lg-12 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                  <div class="icon-box iconbox-blue">
                    <div class="icon">
                      <i class="bx bxl-dribbble"></i>
                    </div>
                    <h4><a href="">Depok</a></h4>
                    <p>Jl. xxxxx, Kota Depok, Jawa Barat</p>
                  </div>
                </div>
              </div>
            </div><!-- End .content-->
          </div>
        </div>
      </div>
    </div>
